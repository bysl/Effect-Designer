﻿#-------------------------------------------------------
# Copyright (c) ED
# All rights reserved.
# History:
# 		<Author>	<Time>		<Version>	  <Des>
#      lzlong		2019-1-10	1.0		
#-------------------------------------------------------
cmake_minimum_required(VERSION 2.8)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# 工程名
PROJECT(ED)  

set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS true)

set(PROJDIR ${CMAKE_CURRENT_SOURCE_DIR})

set(EXECUTABLE_OUTPUT_PATH  ${ED_BINARY_DIR}/bin)
set(LIBRARY_OUTPUT_PATH  	${ED_BINARY_DIR}/bin)

set(LIBS ${LIBRARY_OUTPUT_PATH};${ED_SOURCE_DIR}/3rdParty/lib)
LINK_DIRECTORIES(${LIBS})

OPTION(USE_DMDLL_ "Compile DmMain as dll"   ON) 
if(USE_DMDLL_)

ADD_DEFINITIONS(-DDLL_DMMAIN)
endif()

OPTION(USE_DMLOG_  "DM open log"    ON)
if(USE_DMLOG_)
ADD_DEFINITIONS(-DDMLOG_ON)
endif()

# 是否使用Skia引擎
OPTION(USE_DMSKIA_  "DM user skia render draw"    OFF)

LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/Tools/CMake)
INCLUDE(PrecompiledHeader)

ADD_SUBDIRECTORY(${PROJDIR}/3rdParty/json)
ADD_SUBDIRECTORY(${PROJDIR}/3rdParty/zlib)

ADD_SUBDIRECTORY(${PROJDIR}/Modules/DMMain)
#ADD_SUBDIRECTORY(${PROJDIR}/Modules/HttpReq)
#add_subdirectory(${PROJDIR}/Modules/EDCore)

ADD_SUBDIRECTORY(${PROJDIR}/Modules/EDBase)
ADD_SUBDIRECTORY(${PROJDIR}/EDesigner)

if(USE_DMSKIA_)
ADD_SUBDIRECTORY(${PROJDIR}/3rdParty/skia)
endif()

