#include "StdAfx.h"
#include "EDResourceParser.h"
#include "FileHelper.h"
#include "DMDIBHelper.h"
#include "Modules/Render/DMBitmapImpl.h"
#include <regex>

EDResourceParser* EDResourceParser::sta_pResourceParser = NULL;
EDResourceParser::EDResourceParser()
{
	sta_pResourceParser = this;
}

EDResourceParser::~EDResourceParser()
{
	sta_pResourceParser = NULL;
	if (!m_strTempDir.IsEmpty())
		RemoveDir(m_strTempDir);
}

EDResourceParser* EDResourceParser::GetResourceParserIns(EDJSonParser* pParent)
{
	do
	{
		if (sta_pResourceParser || !pParent)
		{
			return sta_pResourceParser;
			break;
		}

		EDJSonParserPtr pNewNode = pParent->CreateChildParser(EDResourceParser::GetClassNameW(), JSPARSER_FIRST); DMASSERT(pNewNode);
		if (pNewNode)
		{
			return dynamic_cast<EDResourceParser*>(pNewNode);
		}
	} while (false);
	return NULL;
}

bool EDResourceParser::IsResourceNameCharacterValid(const CStringW& strNewName, const CStringW& strTag, bool bMutiFiles, CStringW& strOutTip)
{
	bool bRet = true;
	do
	{
		if (bMutiFiles || strTag == TAG2DSTICKER) //多文件 或者 2d贴纸 必须放文件夹   这种情况必须只能包含字母
		{
			if (bMutiFiles)
				strOutTip = L"包含多张图片的美妆/2D贴纸素材，";
			else
				strOutTip = L"2D贴纸素材，";

			strOutTip += L"名称只能包含英文字母";
			std::regex rx("^[A-Za-z]+$");
			bRet = std::regex_match(std::string(EDBASE::w2a(strNewName)).c_str(), rx);
			break;
		}

		strOutTip = L"包含单张图片的美妆素材，名称不能包含下列任何字符:\\:/*?\"<>|";
		std::regex rx("[^~\\\\:*\\ ? \"<>|/]+");
		bRet = std::regex_match(std::string(EDBASE::w2a(strNewName)).c_str(), rx);		//std::regex rx("^[\u4E00-\u9FA5A-Za-z0-9|]+$");		
	} while (false);
	return bRet;
}

bool EDResourceParser::IsNameOnlyContainNumLettersUnderline(const CStringW& strName)
{
	std::regex rx("^[A-Za-z0-9_|]+$");
	return std::regex_match(std::string(EDBASE::w2a(strName)).c_str(), rx);
}

bool EDResourceParser::IsResourceNameSameConflict(EDResourceNodeParser* pParser, CStringW strNewName)
{
	bool bRet = false;
	do 
	{
		if (!pParser)
			break;

		EDResourceNodeParser* pChild = dynamic_cast<EDResourceNodeParser*>(GetParser(GPS_FIRSTCHILD));
		while (pChild)
		{
			if (pParser != pChild && pChild->m_strName == strNewName && pChild->m_strResTag == pParser->m_strResTag)
			{
				bRet = true;
				break;
			}
			pChild = dynamic_cast<EDResourceNodeParser*>(pChild->GetParser(GPS_NEXTSIBLING));
		}
	} while (false);
	return bRet; 
}

bool EDResourceParser::IsResourceNameSameConflict(const CStringW& strTag, const CStringW& strNewName)
{
	bool bRet = false;
	do
	{
		EDResourceNodeParser* pChild = dynamic_cast<EDResourceNodeParser*>(GetParser(GPS_FIRSTCHILD));
		while (pChild)
		{
			if (pChild->m_strName == strNewName && pChild->m_strResTag == strTag)
			{
				bRet = true;
				break;
			}
			pChild = dynamic_cast<EDResourceNodeParser*>(pChild->GetParser(GPS_NEXTSIBLING));
		}
	} while (false);
	return bRet;
}

CStringW EDResourceParser::GetAppdataTmpDir()
{
	if (m_strTempDir.IsEmpty())
	{
		CStringW strTmpFolder = GetAppdataPath() + L"\\EDesigner\\Temp_" + CStringW(std::to_wstring(GetCurrentProcessId()).c_str()) + CStringW(L"\\");
		FileHelper::CreateMultipleDirectory((LPCWSTR)strTmpFolder);
		m_strTempDir = strTmpFolder;
	}
	return m_strTempDir;
}

EDResourceNodeParser* EDResourceParser::IsResourceNodeExist(CStringW strimagePath, CStringW strName, CStringW strResTag)
{
	EDResourceNodeParser* pRetResNode = NULL;
	EDResourceNodeParser* pChild = dynamic_cast<EDResourceNodeParser*>(GetParser(GPS_FIRSTCHILD));
	while (pChild)
	{
		if (/*pChild->m_strImagePath == strimagePath &&*/ pChild->m_strName == strName && pChild->m_strResTag == strResTag /*&& pChild->m_bDir == bDir*/)
		{
			pRetResNode = pChild;
			break; 
		}
		pChild = dynamic_cast<EDResourceNodeParser*>(pChild->GetParser(GPS_NEXTSIBLING));
	}
	return pRetResNode;
}

EDResourceNodeParser* EDResourceParser::InsertNewResourceNode(CStringW strimagePath, CStringW strName, CStringW strResTag)
{
	EDResourceNodeParser* pNewResourceNode = NULL;
	do 
	{
		pNewResourceNode = IsResourceNodeExist(strimagePath, strName, strResTag);
		if (pNewResourceNode)
		{
			break;
		}

		EDJSonParserPtr pNewNode = CreateChildParser(EDResourceNodeParser::GetClassNameW()); DMASSERT(pNewNode);
		if (!pNewNode)
		{
			break;
		}

		pNewResourceNode = dynamic_cast<EDResourceNodeParser*>(pNewNode); DMASSERT(pNewResourceNode);
		if (!pNewResourceNode)
			break;

		pNewResourceNode->m_strImagePath = strimagePath;
		pNewResourceNode->m_strResTag = strResTag;
		pNewResourceNode->m_strName = strName;
		pNewResourceNode->LoadImgFromPath();
		IEDJSonParserOwner* pParserOwner = GetParserOwner();
		if (pParserOwner)
			pParserOwner->NewJsonParserCreatedNotify(pNewResourceNode);
	} while (false);
	return pNewResourceNode;
}

DMCode EDResourceParser::InitJSonData(JSHandle &JSonHandler)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (false == JSonHandler.isValid())
		{
			iErr = 200;
			break;
		}

		int cnt = JSonHandler.arraySize();
		for (int i = 0; i < cnt; ++i)
		{
			JSHandle& ItemHandle = JSonHandler[i];
			if (!ItemHandle.isValid())
			{
				continue;
			}

			iErr = ParseMemberJSObj(std::string(EDBASE::w2a(EDResourceNodeParser::GetClassNameW())).c_str(), ItemHandle, true);//解析resourcesnode节点
		}
 	} while (false);
	return iErr;
}

DMCode EDResourceParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

EDResourceNodeParser::EDResourceNodeParser() :m_iFrameCount(0), m_pResourceParserOwner(NULL), m_iCurResPreviewPngIndex(0)
{
}

EDResourceNodeParser::~EDResourceNodeParser()
{
	m_pResourceParserOwner = NULL;
	m_iFrameCount = 0;
	m_iCurResPreviewPngIndex = 0;
}

DMCode EDResourceNodeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDResourceNodeParserAttr::STRING_imagePath))
		{
			if (JsHandleValue.isString())
			{
 				m_strImagePath = std::wstring(EDBASE::a2w(JsHandleValue.toStringA())).c_str();
				LoadImgFromPath();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDResourceNodeParserAttr::STRING_name))
		{
			if (JsHandleValue.isString())
			{
				m_strName = std::wstring(EDBASE::a2w(JsHandleValue.toStringA())).c_str();
				//m_strName = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDResourceNodeParserAttr::STRING_tag))
		{
			if (JsHandleValue.isString())
			{
				m_strResTag = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDResourceNodeParserAttr::INT_PreviewPngIndex))
		{
			if (JsHandleValue.isInt())
			{
				m_iCurResPreviewPngIndex = JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else
		{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
			DMASSERT_EXPR(false, L"resource 有没有解析的节点");
			iErr = DM_ECODE_NOLOOP;
		}

		if (!bLoadJSon)
			MarkDataDirty();
	} while (false);
	return iErr;
}

VOID EDResourceNodeParser::LoadImgFromPath()
{
	m_pBmp.Attach(new DM::DMBitmapImpl());
	//m_pBmp.reset(new DM::DMBitmapImpl());	//这样导致引用计数变成了2 导致无法释放内存
	m_ArrImageList.RemoveAll();

	int nCount = (int)SplitStringT(m_strImagePath, L';', m_ArrImageList);
	for (int i = 0; i < nCount; i++)
	{
		if (!IsFileExist((LPCWSTR)m_ArrImageList[i]))
		{
			CStringW strPath = GetParserOwner()->JSonParserGetJSonFilePath() + CStringW(L"\\") + m_ArrImageList[i]; ///__facemorph/eyeE.png 类型路径
			if (IsFileExist((LPCWSTR)strPath) || IsDirectoryExist(strPath))
			{
				m_ArrImageList[i] = strPath;
			}
		}

		if (IsFileExist((LPCWSTR)m_ArrImageList[i]))
		{
			if (m_pBmp && NULL == m_pBmp->GetBitmap())//一般只取第一张
				m_pBmp->LoadFromFile(m_ArrImageList[i], L"PNG");
		}
		else if (IsDirectoryExist(m_ArrImageList[i]))//是文件夹
		{
			CStringW strDir = m_ArrImageList[i];
			m_ArrImageList.RemoveAll();
			for (int i = 0; i < 200; i++)
			{
				if (m_strName.IsEmpty())
				{
					int iSlash = strDir.ReverseFind(L'\\');
					int iSlash2 = strDir.ReverseFind(L'/');
					iSlash = iSlash > iSlash2 ? iSlash : iSlash2;
					if (-1 != iSlash)
					{
						m_strName = strDir.Right(strDir.GetLength() - iSlash - 1);
					}
				}

				CStringW strPng;
				strPng.Format(L"%s\\%s_%03d.png", (LPCWSTR)strDir, (LPCWSTR)m_strName, i);
				if (IsFileExist((LPCWSTR)strPng))
				{
					m_ArrImageList.Add(strPng);
					if (m_pBmp && NULL == m_pBmp->GetBitmap())
						m_pBmp->LoadFromFile(strPng, L"PNG");
				}
				else //not exist
					break;
			}
			DMASSERT(m_ArrImageList.GetCount() > 0);
			break;
		}
		else
		{
			DMASSERT_EXPR(FALSE, m_ArrImageList[i]);
			m_ArrImageList.RemoveAt(i);
		}
	}
	m_iFrameCount = m_ArrImageList.GetCount();
	if (m_iCurResPreviewPngIndex >= m_iFrameCount)
		m_iCurResPreviewPngIndex = 0;
}

DMCode EDResourceNodeParser::SetCurResPreviewPngIndex(int iIndex)
{
	if (iIndex < 0 || iIndex >= (int)m_ArrImageList.GetCount())
		iIndex = 0;
	SetJSonAttributeInt(EDAttr::EDResourceNodeParserAttr::INT_PreviewPngIndex, iIndex);
	if (m_pResourceParserOwner)
	{
		m_pResourceParserOwner->RelativeResourceNodeParser(this);
	}
	return DM_ECODE_OK;
}

DMCode EDResourceNodeParser::RenameResource(const CStringW& strNewName)
{
	SetJSonAttributeString(EDAttr::EDResourceNodeParserAttr::STRING_name, strNewName);
	if (m_pResourceParserOwner)
	{
		m_pResourceParserOwner->RelativeResourceNodeParser(this);
	}
	return DM_ECODE_OK;
}

DMCode EDResourceNodeParser::RemoveProjectUnuseImgRes()
{
	DMCode iErr = DM_ECODE_OK;
	do 
	{
		if (!GetParserOwner())
		{
			iErr = DM_ECODE_FAIL;
			break;
		}

		//工程目录内的资源需要删除
		CStringW strProjectPath = GetParserOwner()->JSonParserGetJSonFilePath();
		CStringW strCombine = strProjectPath + L"\\" + m_strImagePath;
		CStringW strTmpFolder = EDResourceParser::GetResourceParserIns(NULL)->GetAppdataTmpDir();
		if (IsDirectoryExist(strCombine))//删除文件夹 在工程目录下
		{
			CStringW strDestFolder = strTmpFolder + m_strImagePath;
			//RemoveDir((LPCWSTR)strDestFolder);
			FileHelper::CreateMultipleDirectory((LPCWSTR)strDestFolder);
			CopyDir(strCombine, strDestFolder);
			RemoveDir(strCombine);
			m_strImagePath = strDestFolder;
			LoadImgFromPath();
		}
		else if (IsFileExist(strCombine))
		{
			CStringW strDestFile = strTmpFolder + m_strImagePath;
			int iSlash = strDestFile.ReverseFind(L'\\');
			int iSlash2 = strDestFile.ReverseFind(L'/');
			iSlash = iSlash > iSlash2 ? iSlash : iSlash2;
			if (-1 != iSlash)
			{
				CStringW szFileDir = strDestFile.Left(iSlash);
				FileHelper::CreateMultipleDirectory((LPCWSTR)szFileDir);
			}
			CopyFile(strCombine, strDestFile, FALSE);
			DeleteFileW(strCombine);
			m_strImagePath = strDestFile;
			LoadImgFromPath();
		}
		else
			iErr = DM_ECODE_FAIL;
	} while (false);
	return iErr;
}

DMCode EDResourceNodeParser::MoveProjectDirImgRes(CStringW strNewDestName)
{
	DMCode iErr = DM_ECODE_OK;
	do
	{
		if (!GetParserOwner())
		{
			iErr = DM_ECODE_FAIL;
			break;
		}
		//工程目录内的资源需要调整位置
		CStringW strProjectPath = GetParserOwner()->JSonParserGetJSonFilePath();
		CStringW strCombineOld = strProjectPath + L"\\" + m_strImagePath;
		CStringW strCombineNew = strProjectPath + L"\\" + strNewDestName;
		if (IsDirectoryExist(strCombineOld))//删除文件夹
		{
			_wrename((LPCWSTR)strCombineOld, (LPCWSTR)strCombineNew);
		}
		else if (IsFileExist(strCombineOld))
		{
			_wrename((LPCWSTR)strCombineOld, (LPCWSTR)strCombineNew);
		}
		else
			iErr = DM_ECODE_FAIL;
	} while (false);
	return iErr;
}

DMCode EDResourceNodeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	if (!m_pResourceParserOwner) //没有owner 暂时丢弃  因为没找到存放的位置  lzlong
	{
		RemoveProjectUnuseImgRes();//删除工程目录下没用的资源
		return DM_ECODE_FAIL;
	}
	DMCode iErr = MoveBuildResourceFiles();
	int iSize = JSonHandler.arraySize();
	do
	{
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_imagePath].putString(std::string(EDBASE::w2a(m_strImagePath)).c_str());
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_tag].putStringW(m_strResTag);
	//	JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_name].putStringW(m_strName);
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::STRING_name].putString(std::string(EDBASE::w2a(m_strName)).c_str());
		JSonHandler[iSize][EDAttr::EDResourceNodeParserAttr::INT_PreviewPngIndex].putInt(m_iCurResPreviewPngIndex);
	} while (false);
	return iErr;
}

void EDResourceNodeParser::FreeParser(EDJSonParser* pParser)
{
	if (m_pResourceParserOwner)
	{
		m_pResourceParserOwner->RelativeResourceNodeParser(NULL);
	}
	delete pParser;
}

DMCode EDResourceNodeParser::MoveBuildResourceFiles()
{
	DMASSERT(m_ArrImageList.GetCount() > 0);
	if (m_ArrImageList.GetCount() < 1)
		return DM_ECODE_FAIL;

	bool b2DStickerRes = false;
	bool bFolderSave = false;//是否是用文件夹保存   只有单张的美妆图片可以不用文件夹保存
	if (m_strResTag == TAG2DSTICKER)//2d贴纸
	{
		bFolderSave = true;
		b2DStickerRes = true;
	}

	if (m_ArrImageList.GetCount() > 1)//多个文件 必须用文件夹保存
	{
		bFolderSave = true;
	}

	IEDJSonParserOwner* pParserOwner = GetParserOwner(); DMASSERT(pParserOwner);
	if (!pParserOwner)
		return DM_ECODE_FAIL;

	CStringW strProjectPath = pParserOwner->JSonParserGetJSonFilePath();
	CStringW strResourceDir;
	CStringW strRelativeDir;//相对目录
	if (b2DStickerRes)
	{
		strResourceDir = strProjectPath + CStringW(L"/") + m_strName;
		strRelativeDir = m_strName;
	}
	else
	{
		if (bFolderSave)
		{
			strResourceDir = strProjectPath + L"/" FACEMORPH L"/" + m_strName;
			strRelativeDir = FACEMORPH L"/" + m_strName;
		}
		else
		{
			strResourceDir = strProjectPath + L"/" FACEMORPH;
			strRelativeDir = FACEMORPH;
		}
	}
	FileHelper::CreateMultipleDirectory((LPCWSTR)strResourceDir);
	auto IsSameFilePath = [](CStringW strSrc1, CStringW strSrc2){
		int iSlash = strSrc1.Find(L"/");
		while (iSlash >= 0)
		{
			strSrc1.SetAt(iSlash, L'\\');
			iSlash = strSrc1.Find(L"/");
		}
		int iSlash2 = strSrc2.Find(L"/");
		while (iSlash2 >= 0)
		{
			strSrc2.SetAt(iSlash2, L'\\');
			iSlash2 = strSrc2.Find(L"/");
		}
		return _wcsicmp(strSrc1, strSrc2) == 0;
	};

	bool bImgPathChanged = false;
	if (bFolderSave)
	{
		for (size_t index = 0; index < m_ArrImageList.GetCount(); index++)
		{
			CStringW strDesFilePath, strTmpPath;
			strDesFilePath.Format(L"%s/%s_%03d.png", (LPCWSTR)strResourceDir, (LPCWSTR)m_strName, index);
			if (!IsSameFilePath(m_ArrImageList[index], strDesFilePath))
			{
				CopyFile(m_ArrImageList[index], strDesFilePath, FALSE);
				if (m_ArrImageList[index].Find(strProjectPath) >= 0) //工程目录 删除原来的文件
				{
					DeleteFileW(m_ArrImageList[index]);
					int iSlash = m_ArrImageList[index].ReverseFind(L'\\');
					int iSlash2 = m_ArrImageList[index].ReverseFind(L'/');
					iSlash = iSlash > iSlash2 ? iSlash : iSlash2;
					if (-1 != iSlash)
					{
						CStringW szFileDir = m_ArrImageList[index].Left(iSlash);
						if (IsDirectoryExist(szFileDir))
						{
							RemoveDirectory(szFileDir);//删除空文件夹
						}
					}
				}
			}
		}
		
		if (m_strImagePath != strRelativeDir)
		{
			m_strImagePath = strRelativeDir;
			LoadImgFromPath();
			bImgPathChanged = true;
		}
	}
	else//单个文件 美妆图片
	{
		CStringW strDesFilePath, strTmpPath;
		strDesFilePath.Format(L"%s/%s.png", (LPCWSTR)strResourceDir, (LPCWSTR)m_strName);
		if (!IsSameFilePath(m_ArrImageList[0], strDesFilePath))
		{
			CopyFile(m_ArrImageList[0], strDesFilePath, FALSE);
			if (m_ArrImageList[0].Find((LPCWSTR)strResourceDir) >= 0)//在__facemorph文件夹 需要删除文件夹的png文件
			{
				DeleteFileW(m_ArrImageList[0]);
			}

			strTmpPath.Format(L"/%s.png", (LPCWSTR)m_strName);
			m_strImagePath = strRelativeDir + strTmpPath;
			LoadImgFromPath();
			bImgPathChanged = true;
		}		
	}

	if (m_pResourceParserOwner)
	{
		if (bImgPathChanged && 0 == _wcsicmp(m_pResourceParserOwner->V_GetClassName(), EDMakeupsNodeParser::GetClassName()))
			m_pResourceParserOwner->RelativeResourceNodeParser(this, true);//更新makeup m_strImagePath 不置脏
	}
	return DM_ECODE_OK;
}

VOID EDResourceNodeParser::BindCurResourceParserOwner(EDJSonParser* pParser)
{
	m_pResourceParserOwner = pParser;
	if (GetParserOwner())
	{
		GetParserOwner()->ResourceNodeOwnerParserChgNotify(this);
	}
}

VOID EDResourceNodeParser::GetImgSize(INT& uiWidth, INT& uiHeight)
{
	if (m_pBmp)
	{
		uiWidth = m_pBmp->GetWidth();
		uiHeight = m_pBmp->GetHeight();
	}
	else
	{
		uiWidth = DEFRESPNGWIDTH;
		uiHeight = DEFRESPNGHEIGHT;
	}
}