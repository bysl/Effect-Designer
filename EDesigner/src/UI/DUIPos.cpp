#include "stdAfx.h"
#include "DUIPos.h"

namespace DM
{
	// PosEdit-----------------------------------------------------
	PosEdit::PosEdit() : m_pPosItem(NULL), m_editPostype(EDITPOS_NULL)
	{
	}

	void PosEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		if (0 == isdigit(nChar) && nChar != '-')
		{
			if (0 == GetWindowTextLength())
			{// 至少有一位字符
				SetWindowText(L"0");
				SetSel(MAKELONG(-1, -1));
			}
			return;
		}
		__super::OnChar(nChar, nRepCnt, nFlags);
	}

	LRESULT PosEdit::OnImeChar(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		//DM_MessageBox(L"不要使用输入法输入!,请切换到英文模式");
		return 0;
	}

	DMCode PosEdit::DV_CreateChildWnds(DMXmlNode &XmlNode)
	{
		DMCode iErr = __super::DV_CreateChildWnds(XmlNode);

		SetEventMask(ENM_CHANGE | GetEventMask());
		m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&PosEdit::OnEditChange, this));
		return iErr;
	}

	DMCode PosEdit::OnEditChange(DMEventArgs *pEvt)
	{
		DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
		if (EN_CHANGE == pEvent->m_iNotify)
		{
			if (m_pPosItem)
			{
				m_pPosItem->OnItemChange();
			}
		}
		return DM_ECODE_OK;
	}

	BOOL PosEdit::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
	{
		CStringW strValue = GetWindowText();
		int iValue = StringToInt(strValue);
		bool bInit = false;
		if (0 == iValue)
		{
			if (strValue.Left(1) == L"-" && zDelta > 0)// 原来是-0
			{
				strValue = L"0";
				bInit = true;
			}
			else if (strValue.Left(1) != L"-" && zDelta < 0)// 原来是0
			{
				strValue = L"-0";
				bInit = true;
			}
		}
		if (false == bInit)
		{
			iValue += (zDelta > 0 ? 1 : -1);
			strValue.Format(L"%d", iValue);
		}
		SetWindowText(strValue);
		SetSel(MAKELONG(-1, -1));
		return TRUE;
	}

	//PosItem---------------------------------------------------------------
	PosItem::PosItem()
 	{
 		m_pItem = NULL;
		m_pObjTreeCtrl = g_pMainWnd->FindChildByNameT<ED::DUIEffectTreeCtrl>(EFFECTTREENAME);  DMASSERT(m_pObjTreeCtrl);
 	}

	DMCode PosItem::SetEditItem(PosEdit* pEdit)
	{
		if (pEdit)
		{
			if (pEdit != m_pEdit)
			{
				if (m_pEdit)
					m_pEdit->m_pPosItem = NULL;
				m_pEdit = pEdit;
				m_pEdit->m_pPosItem = this;
			}
		}
		else
		{
			if (m_pEdit)
			{
				m_pEdit->m_pPosItem = NULL;
			}
			m_pEdit = pEdit;
		}
		return DM_ECODE_OK;
	}

	DMCode PosItem::InitPosItem(POS_ITEM* pItem, Layout* pLayout, PosEdit* pEdit)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do 
		{
			if (NULL == pItem||NULL == pLayout)
			{
				break;
			}
		
			m_pItem   = pItem;
			m_pLayout = pLayout;
			SetEditItem(pEdit);
			if (pEdit && m_pItem)
			{
				int iValue = DMABS((int)m_pItem->nPos);
				if (EDITPOS_LEFT == pEdit->m_editPostype || EDITPOS_RIGHT == pEdit->m_editPostype)
				{
					iValue -= ROOTPNGXOFFSET;
				}
				else if (EDITPOS_TOP == m_pEdit->m_editPostype || EDITPOS_BOTTOM == m_pEdit->m_editPostype)
				{
					iValue -= ROOTPNGYOFFSET;
				}
				
				CStringW strValue = IntToString(iValue);
				DUIWndAutoMuteGuard guard(pEdit);
				pEdit->SetWindowText(strValue);
				
				if (g_pMainWnd->m_pObjEditor && g_pMainWnd->m_pObjEditor->m_pDragFrame && g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData && g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData->m_pJsonParser)
				{				
					double dbScale = g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData->m_pJsonParser->GetParserImageScale();
					strValue.Format(L"%.2f", dbScale);//strValue = DoubleToString(dbScale);	
					DUIWndAutoMuteGuard guard1(g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[0]);
					g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[0]->SetWindowText(strValue);//scale
				}

				strValue = IntToString(m_pLayout->m_pOwner->m_rcWindow.Width());//width
				DUIWndAutoMuteGuard guard2(g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[1]);
				g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[1]->SetWindowText(strValue);
				//MuteDUIWnd(g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[1], false);// 窗口切换时，不影响消息
				strValue = IntToString(m_pLayout->m_pOwner->m_rcWindow.Height());//height
				DUIWndAutoMuteGuard guard3(g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[2]);
				g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pElementSWHEditItem[2]->SetWindowText(strValue);
			}
			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}

	DMCode PosItem::UnInitPosItem()
	{
		m_pItem = NULL;
		m_pLayout = NULL;
		SetEditItem(NULL);
		return DM_ECODE_OK;
	}
	
	DMCode PosItem::OnItemChange(POS_ITEM* pItem)
	{
		if (NULL == m_pLayout||NULL == m_pItem)
		{
			return DM_ECODE_FAIL;
		}

		if (!m_pObjTreeCtrl || !m_pObjTreeCtrl->GetSelectedItem())
		{
			return DM_ECODE_FAIL;
		}

		// 1.临时备份
		POS_ITEM Pos_Copy;
		memcpy(&Pos_Copy,m_pItem,sizeof(POS_ITEM));
		if (!pItem)// 传NULL表示使用UI初始化,pItem指向m_pItem自身,不为NULL表示为dragframe传入
		{
			CRect rcWnd;
			m_pLayout->m_pOwner->DV_GetClientRect(rcWnd);
			POS_ITEM PosLeft, PosTop, PosRight, PosBtm;
			memcpy(&PosLeft, &m_pLayout->m_Left, sizeof(POS_ITEM));
			memcpy(&PosTop, &m_pLayout->m_Top, sizeof(POS_ITEM));
			memcpy(&PosRight, &m_pLayout->m_Right, sizeof(POS_ITEM));
			memcpy(&PosBtm, &m_pLayout->m_Bottom, sizeof(POS_ITEM));
			m_pLayout->ParseItemByRect(rcWnd, PosLeft, PosTop, PosRight, PosBtm);

			INT iWidth = 0, iHeight = 0;
			g_pMainWnd->m_pObjEditor->m_pDragFrame->m_pData->m_pJsonParser->GetRelativeResourceImgSize(iWidth, iHeight);

			CStringW strValue	= m_pEdit->GetWindowText();
			int iValue			= StringToInt(strValue);
			if (EDITPOS_LEFT == m_pEdit->m_editPostype || EDITPOS_RIGHT == m_pEdit->m_editPostype)
			{
				iValue += ROOTPNGXOFFSET;			

				//下面进行等比变换
				float nInCrease = 0.0f;
				if (EDITPOS_LEFT == m_pEdit->m_editPostype)
				{
					nInCrease = iValue - m_pLayout->m_Left.nPos;
					PosLeft.nPos += nInCrease;
					m_pLayout->m_Left.nPos = static_cast<float>(iValue); //update
				}
				else
				{
					nInCrease = iValue - m_pLayout->m_Right.nPos;
					PosRight.nPos += nInCrease;
					m_pLayout->m_Right.nPos = static_cast<float>(iValue); //update
				}

				INT iNewHeight = iHeight; INT iMultiplyRet = 0;
				if (FAILED(Multiply(&iMultiplyRet, iHeight, (INT)(PosRight.nPos - PosLeft.nPos))))//溢出
				{
					iNewHeight = (INT)(iHeight * ((PosRight.nPos - PosLeft.nPos) / (float)iWidth));
				}
				else
				{
					iNewHeight = iMultiplyRet / iWidth;
				}
				PosBtm.nPos = PosTop.nPos + iNewHeight;
				g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[3]->OnItemChange(&PosBtm);//底部的pos改变
			}
			else if (EDITPOS_TOP == m_pEdit->m_editPostype || EDITPOS_BOTTOM == m_pEdit->m_editPostype)
			{
				iValue += ROOTPNGYOFFSET;

				//下面进行等比变换
				float nInCrease = 0.0f;
				if (EDITPOS_TOP == m_pEdit->m_editPostype)
				{
					nInCrease = iValue - m_pLayout->m_Top.nPos;
					PosTop.nPos += nInCrease;
					m_pLayout->m_Top.nPos = static_cast<float>(iValue); //update
				}
				else
				{
					nInCrease = iValue - m_pLayout->m_Bottom.nPos;
					PosBtm.nPos += nInCrease;
					m_pLayout->m_Bottom.nPos = static_cast<float>(iValue);
				}

				INT iNewWidth = iWidth; INT iMultiplyRet = 0;
				if (FAILED(Multiply(&iMultiplyRet, iWidth, (INT)(PosBtm.nPos - PosTop.nPos))))//溢出
				{
					iNewWidth = (INT)(iWidth * ((PosBtm.nPos - PosTop.nPos) / (float)iHeight));
				}
				else
				{
					iNewWidth = iMultiplyRet / iHeight;
				}
				PosRight.nPos = PosLeft.nPos + iNewWidth;
				g_pMainWnd->m_pObjEditor->m_pDuiPos->m_pItem[2]->OnItemChange(&PosRight);//右边的pos改变
			}
			m_pItem->bMinus	= false;
			m_pItem->nPos	= (float)DMABS(iValue);
			pItem           = m_pItem;

			g_pMainWnd->m_pObjEditor->m_pDragFrame->SetParserItemRect(PosLeft, PosTop, PosRight, PosBtm);
		}

		if (pItem->pit != Pos_Copy.pit// 不相等
			||pItem->nPos != Pos_Copy.nPos
			||(PIT_PERCENT!=pItem->pit&&PIT_OFFSET!=pItem->pit&&pItem->bMinus != Pos_Copy.bMinus))//%或@时无视-号
		{
			if (pItem != m_pItem)//不为NULL表示为dragframe传入
			{
				memcpy(m_pItem,pItem,sizeof(POS_ITEM));
			}
			
			if (m_pLayout->m_pOwner && m_pLayout->m_pOwner->m_Node.m_pParent)
			{
				//2.设置Pos
				if (DMSUCCEEDED(m_pLayout->m_pOwner->m_Node.m_pParent->DV_UpdateChildLayout()))
				{
					g_pMainWnd->m_pObjEditor->DragFrameInSelMode();
					g_pMainWnd->m_pObjEditor->ReferenceLineFrameInSelMode();

					//3.更新
					//CStringW strPos = m_pLayout->GetPosString();
					ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTreeCtrl->GetItemData(m_pObjTreeCtrl->GetSelectedItem());			
					if (pItem != m_pItem)
					{
						InitPosItem(m_pItem, m_pLayout, m_pEdit);// 不能直接传pItem，m_pItem总是要和Layout关联
					}
				}
				else
				{
					DMASSERT(FALSE);
					memcpy(m_pItem,&Pos_Copy,sizeof(POS_ITEM));
					if (pItem == m_pItem)// 传NULL表示使用UI初始化
					{
						InitPosItem(m_pItem, m_pLayout, m_pEdit);
					}
				}
			}
		}

		return DM_ECODE_OK;
	}
	
	DUIPos::DUIPos()
	{
		for (int i = 0; i < 4; i++)
		{
			g_pDMApp->CreateRegObj((void**)&m_pItem[i], PosItem::GetClassName(), DMREG_Window);
		}
	}

	//DUIPos---------------------------------------------------------------
	DMCode DUIPos::InitLayout(IDMLayout* pLayout, PosEdit** ppPosEdit)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do 
		{
			if (NULL == pLayout)
			{
				break;
			}
			
			m_pLayout = dynamic_cast<Layout*>(pLayout);
			if (ppPosEdit[0])	ppPosEdit[0]->m_editPostype = EDITPOS_LEFT;
			if (ppPosEdit[1])	ppPosEdit[1]->m_editPostype = EDITPOS_TOP;
			if (ppPosEdit[2])	ppPosEdit[2]->m_editPostype = EDITPOS_RIGHT;
			if (ppPosEdit[3])	ppPosEdit[3]->m_editPostype = EDITPOS_BOTTOM;
			if (ppPosEdit[4])	m_pElementSWHEditItem[0] = ppPosEdit[4];
			if (ppPosEdit[5])	m_pElementSWHEditItem[1] = ppPosEdit[5];
			if (ppPosEdit[6])	m_pElementSWHEditItem[2] = ppPosEdit[6];
			//1.解析到四个边框中
			m_pItem[0]->InitPosItem(&m_pLayout->m_Left, m_pLayout, ppPosEdit[0]);
			m_pItem[1]->InitPosItem(&m_pLayout->m_Top, m_pLayout, ppPosEdit[1]);
			m_pItem[2]->InitPosItem(&m_pLayout->m_Right, m_pLayout, ppPosEdit[2]);
			m_pItem[3]->InitPosItem(&m_pLayout->m_Bottom, m_pLayout, ppPosEdit[3]);
						
			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}

	DMCode DUIPos::UnInitLayout()
	{
		DMCode iErr = DM_ECODE_FAIL;
		do 
		{
			if (m_pItem[0].isNull())
			{
				break;
			}
			for (int i=0; i<4; i++)
			{
				m_pItem[i]->UnInitPosItem();
			}
			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}
}