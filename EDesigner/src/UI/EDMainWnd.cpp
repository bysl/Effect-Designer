﻿#include "stdafx.h"
#include "EDMainWnd.h"

BEGIN_MSG_MAP(EDMainWnd)
	//CHAIN_MSG_STATIC_MAP(DUITransitionTreeCtrl, s_ProcessWindowMessage)
	MSG_WM_CREATE(OnCreate)
	MSG_WM_INITDIALOG(OnInitDialog)
	MSG_WM_DESTROY(OnDestroy)
	MSG_WM_SIZE(OnSize)
	MSG_WM_CLOSE(OnClose)
	MSG_WM_LBUTTONDBLCLK(OnLButtonDbClick)
	MSG_WM_COMMAND(OnCommand)
	MSG_WM_DROPFILES(OnDropFiles)
	CHAIN_MSG_MAP(DMTrayIconImpl)
	CHAIN_MSG_MAP(DMHWnd)// 将未处理的消息交由DMHWnd处理
	REFLECT_NOTIFICATIONS_EX()
END_MSG_MAP()

 BEGIN_EVENT_MAP(EDMainWnd)
	CHAIN_EVENT_STATIC_MAP(DUITransitionTreeCtrl, s_DMHandleEvent)
 	EVENT_NAME_COMMAND(L"minbutton", OnMinimize)
 	EVENT_NAME_COMMAND(L"closebutton",OnBtnClose)
	EVENT_NAME_COMMAND(L"maxmizeButton", OnMaximize)
	EVENT_NAME_COMMAND(L"restoreButton", OnRestore)
	EVENT_NAME_COMMAND(L"ImportResButton", OnImportReslib)
	EVENT_NAME_COMMAND(L"AddEffectButton", OnAddTempletEffect)
	EVENT_NAME_HANDLER(L"shrinkpreviewbutton", DM::DMEVT_CMD, OnShrinkPreview)
	EVENT_NAME_HANDLER(L"enlargepreviewbutton", DM::DMEVT_CMD, OnEnlargePreview)
	EVENT_NAME_HANDLER(L"openpreviewpanelbutton", DM::DMEVT_CMD, OnOpenPreviewPanel)
	EVENT_NAME_HANDLER(L"closepreviewpanelbutton", DM::DMEVT_CMD, OnClosePreviewPanel)
	EVENT_NAME_HANDLER(L"ProjectToZipButton", DM::DMEVT_CMD, OnExportProjectToZip)
	EVENT_NAME_COMMAND(L"PausePlayPreviewBtn", OnPausePlayPreview)
	EVENT_ID_COMMAND_RANGE(GLBMENUBTN_ID_MIN, GLBMENUBTN_ID_MAX, OnGlobalMenuBtn)
END_EVENT_MAP()

EDMainWnd::EDMainWnd() :m_pMaxBtn(nullptr), m_pRestoreBtn(nullptr), m_pEffectTreeCtrl(nullptr), m_pTransitionTreeCtrl(NULL),
m_pZiyuankuListboxEx(nullptr), m_p2DStickerResListCombobox(nullptr), m_pMakeupResListCombobox(NULL), m_pMakeupTagListCombobox(NULL), m_pMakeupTargetFPSCombobox(NULL), m_pMakeupMajorAttrList(NULL), m_pMakeupAllowDiscardFrameCombobox(NULL),
m_pTraceposListCombobox(NULL), m_pStickerBlendModeCombobox(NULL), m_hTreeAddPopupItem(NULL), m_pTriggerAriseListCombobox(NULL), m_pTriggerActListCombobox(NULL),
m_pStickerTargetFPSCombobox(NULL), m_pStickerGroupSizeCombobox(NULL), m_pStickerGroupIndexCombobox(NULL), m_pStickerAllowDiscardFrameCombobox(NULL),
m_pTriggercicleListCombobox(NULL), m_bShrinkingPreview(false), m_p2DStickerNodeAttrList(NULL), m_pTransitionNodeAttrList(NULL), m_pStaTransitionName(NULL), m_pMakeupNodeAttrList(NULL),
m_iShrinkScale(100), m_iShrinkScaleDest(100), m_CtrlZAccel(DUIAccel::TranslateAccelKey(L"ctrl+z")), m_CtrlRAccel(DUIAccel::TranslateAccelKey(L"ctrl+r")),
m_CtrlSAccel(DUIAccel::TranslateAccelKey(L"ctrl+s")), m_CtrlNAccel(DUIAccel::TranslateAccelKey(L"ctrl+n")), m_F12Accel(DUIAccel::TranslateAccelKey(L"F12")), m_CtrlQAccel(DUIAccel::TranslateAccelKey(L"ctrl+q")), m_CtrlOAccel(DUIAccel::TranslateAccelKey(L"ctrl+o")), m_JSonMainParser(this),
m_pObjEditor(NULL), m_hObjSel(NULL), m_pAddFaceTraceComboListBtn(NULL), m_bJsonDataDirtyMark(false),
m_pFaceTraceListCombobox0(NULL), m_pFaceTraceListCombobox1(NULL), m_pFaceTraceListCombobox2(NULL), m_iTransitionIndex(1),
m_pFaceTraceListCombobox3(NULL), m_pFaceTraceListCombobox4(NULL), m_pFaceTraceListCombobox5(NULL), m_iMakeupTargetFPS(15), m_iStickerTargetFPS(15), m_pStickerPositionTypeCombobox(NULL),
m_pBeautifyAttrList(NULL), m_pFaceExchangeAttrList(NULL), m_pBackgroundEdgeAttrList(NULL), m_pStickerPositionRelationTypeCombobox(NULL), m_pStickerHotLinkCombobox(NULL), m_pZiyuankuPngIndexEdt(NULL),
m_pBeautifySmallFaceCombobox(NULL), m_pMaxFaceSuppportedListCombobox(NULL), m_pBackgroundEdgeWidthListCombobox(NULL), m_pBackgroundEdgeColorBtn(NULL), m_pBeautifyEnlargeEyeCombobox(NULL), m_pBeautifyShrinkFaceCombobox(NULL),
m_pBeautifyReddenCombobox(NULL), m_pBeautifyWhittenCombobox(NULL), m_pBeautifySmoothCombobox(NULL), m_pBeautifyConstrastCombobox(NULL), m_pBeautifySaturtionCombobox(NULL), m_pBackgroundEdgeColorSetCtrl(NULL)
{
	memset(m_bProjectAddedMakeupRes, 0, sizeof(m_bProjectAddedMakeupRes));

	m_dbEnlargeRatio	= 0.0;
	m_dbShrinkRatio		= 0.0;
	m_dbSmallRatio		= 0.0;
	m_dbReddenStrength	= 0.0;
	m_dbWhitenStrength	= 0.0;
	m_dbSmoothStrength	= 0.0;
	m_dbContrastStrength = 0.0;
	m_dbSaturation		= 0.0;
	m_iBackGroundEdgeColor = -1;
}

int EDMainWnd::OnCreate(LPVOID)
{
	SetMsgHandled(FALSE);
	return 0;
}

BOOL EDMainWnd::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	SetMainWndTitleText((CStringW)DEFAULTWNDTITLE);

	// 设置icon
	HICON hIcon = ::LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ED128));
	HICON hIconBig = ::LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ED256));
	SetIcon(hIcon, FALSE);
	SetIcon(hIconBig, TRUE);

	InstallIcon(DEFAULTWNDTITLE, hIcon, IDI_ED128);
	CreateSdkPreviewWnd();

	GetAccelMgr()->RegisterAccel(m_CtrlZAccel, this);
	GetAccelMgr()->RegisterAccel(m_CtrlRAccel, this);
	GetAccelMgr()->RegisterAccel(m_CtrlSAccel, this);
	GetAccelMgr()->RegisterAccel(m_CtrlNAccel, this);
	GetAccelMgr()->RegisterAccel(m_F12Accel,   this);
	GetAccelMgr()->RegisterAccel(m_CtrlQAccel, this);
	GetAccelMgr()->RegisterAccel(m_CtrlOAccel, this);

	m_pMaxBtn = FindChildByNameT<DUIButton>(L"maxmizeButton");		DMASSERT(m_pMaxBtn);
	m_pRestoreBtn = FindChildByNameT<DUIButton>(L"restoreButton");	DMASSERT(m_pRestoreBtn);

	m_pZiyuankuListboxEx = FindChildByNameT<ED::EDListBoxEx>(L"resLibraryListbox");  DMASSERT(m_pZiyuankuListboxEx);
	m_pEffectTreeCtrl = FindChildByNameT<ED::DUIEffectTreeCtrl>(EFFECTTREENAME);  DMASSERT(m_pEffectTreeCtrl);
	m_pEffectTreeCtrl->m_EventMgr.SubscribeEvent(DMEventTCSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnEffectTreeSeleChanged, this));

	m_p2DStickerNodeAttrList = FindChildByNameT<DUIList>(L"ds_2DStickerNodeAttr_list"); DMASSERT(m_p2DStickerNodeAttrList);
	m_pTransitionNodeAttrList = FindChildByNameT<DUIList>(L"ds_TransitionNodeAttr_list"); DMASSERT(m_pTransitionNodeAttrList);
	m_pMakeupNodeAttrList = FindChildByNameT<DUIList>(L"ds_MakeupV2NodeAttr_list"); DMASSERT(m_pMakeupNodeAttrList);
	m_pMakeupMajorAttrList = FindChildByNameT<DUIList>(L"ds_MakeupMajorAttr_list"); DMASSERT(m_pMakeupMajorAttrList);
	m_pBeautifyAttrList = FindChildByNameT<DUIList>(L"ds_BeautifyAttr_list"); DMASSERT(m_pBeautifyAttrList);
	m_pFaceExchangeAttrList = FindChildByNameT<DUIList>(L"ds_FaceExchangeAttr_list"); DMASSERT(m_pFaceExchangeAttrList);
	m_pBackgroundEdgeAttrList = FindChildByNameT<DUIList>(L"ds_BackgroundEdgeAttr_list"); DMASSERT(m_pBackgroundEdgeAttrList);

	if (m_p2DStickerNodeAttrList)//2D贴纸属性界面
	{
		m_p2DStickerNodeAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_p2DStickerNodeAttrList->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			if (!m_p2DStickerResListCombobox)//下面为同一个panel的控件
			{
				m_p2DStickerResListCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_reslistcombobox");
				DUIButton* pImportimgbtn = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_import2dstikerimgbtn");
				DUIButton* pPrevImgbtn = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_2dstickerprevrespreviewbtn");
				DUIButton* pNextImgbtn = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_2dstickernextrespreviewbtn");
				m_pTraceposListCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_traceposlistcombobox");
				m_pStickerBlendModeCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_blenmodecombobox");
				m_pStickerTargetFPSCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_TargetFPScombobox");
				m_pStickerGroupSizeCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_GroupSizeCombobox");
				m_pStickerGroupIndexCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_GroupIndexCombobox");
				m_pStickerAllowDiscardFrameCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_AllowDiscardFrameCombobox");
				m_pStickerPositionTypeCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_PositionTypeCombobox");
				m_pStickerPositionRelationTypeCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_PositionRelationTypeCombobox");
				m_pStickerHotLinkCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_HotLinkCombobox");
				if (m_p2DStickerResListCombobox)
				{
					m_p2DStickerResListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::On2DStickerRestComboLisChangeEvent, this));
				}
				if (pImportimgbtn)
				{
					pImportimgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnBtnImport2DImgCmdEvent, this));
				}
				if (pPrevImgbtn)
				{
					pPrevImgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuPrevResPreviewBtnCmdEvent, this));
				}
				if (pNextImgbtn)
				{
					pNextImgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuNextResPreviewBtnCmdEvent, this));
				}
				if (m_pTraceposListCombobox)
				{
					m_pTraceposListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnTracePosListComboChangeEvent, this));
				}
				if (m_pStickerBlendModeCombobox)
				{
					m_pStickerBlendModeCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerBlendModeComboChangeEvent, this));
				}
				if (m_pStickerTargetFPSCombobox)
				{
					m_pStickerTargetFPSCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnStickerTargetFPSEditInputEnter, this));
					m_pStickerTargetFPSCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pStickerTargetFPSCombobox->m_pEdit->GetEventMask());
					m_pStickerTargetFPSCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnStickerTargetFPSEditChange, this));
					m_pStickerTargetFPSCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerTargetFPSComboChangeEvent, this));
				}
				if (m_pStickerGroupSizeCombobox)
				{
					m_pStickerGroupSizeCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerGroupSizeComboChangeEvent, this));
				}
				if (m_pStickerGroupIndexCombobox)
				{
					m_pStickerGroupIndexCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerGroupIndexComboChangeEvent, this));
				}
				if (m_pStickerAllowDiscardFrameCombobox)
				{
					m_pStickerAllowDiscardFrameCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerAllowDiscardFrameComboChangeEvent, this));
				}
				if (m_pStickerPositionTypeCombobox)
				{
					m_pStickerPositionTypeCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerPositionTypeComboChangeEvent, this));
				}
				if (m_pStickerPositionRelationTypeCombobox)
				{
					m_pStickerPositionRelationTypeCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerPositionRelationTypeComboChangeEvent, this));
				}
				if (m_pStickerHotLinkCombobox)
				{
					m_pStickerHotLinkCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnStickerHotLinkComboChangeEvent, this));
				}
			}
			if (!m_pStickerPosEditItem[0])//下面为同一个panel的控件
			{
				m_pStickerPosEditItem[0] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"leftitemedit");
				m_pStickerPosEditItem[1] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"topitemedit");
				m_pStickerPosEditItem[2] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"rightitemedit");
				m_pStickerPosEditItem[3] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"bottomitemedit");
				m_pStickerPosEditItem[4] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"scalratioedit");
				m_pStickerPosEditItem[5] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"widthedit");
				m_pStickerPosEditItem[6] = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"heightedit");
			}

			if (!m_pTriggerAriseListCombobox)//下面为同一个panel的控件
			{
				m_pTriggerAriseListCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_triggerariselistcombobox");
				m_pTriggerActListCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_triggeractlistcombobox");
				m_pTriggercicleListCombobox = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_triggerciclelistcombobox");
				if (m_pTriggerAriseListCombobox)
				{
					m_pTriggerAriseListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnTriggerAriseListComboChangeEvent, this));
				}
				if (m_pTriggerActListCombobox)
				{
					m_pTriggerActListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnTriggerActListComboChangeEvent, this));
				}
				if (m_pTriggercicleListCombobox)
				{
					m_pTriggercicleListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnTriggerCicleListComboChangeEvent, this));
				}
			}
		}
	}
	if (m_pTransitionNodeAttrList)//transiton子节点属性
	{
		int count = (int)m_pTransitionNodeAttrList->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			if (!m_pTransitionTreeCtrl)//下面为同一个panel的控件
			{
				m_pTransitionTreeCtrl = m_pTransitionNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUITransitionTreeCtrl>(L"TransitionTree");
				m_pStaTransitionName = m_pTransitionNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIStatic>(L"staTransitionNodeName");
			}
		}
	}
	if (m_pMakeupNodeAttrList)//美妆属性界面
	{
		m_pMakeupNodeAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_pMakeupNodeAttrList->m_ChildPanelArray.GetCount(); DMASSERT(count == 2);
		for (int i = 0; i < count; i++)
		{
			if (!m_pMakeupResListCombobox)//下面为同一个panel的控件
			{
				DUIButton* pImportmakeupimgbtn = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_importmakeupimgbtn");
				DUIButton* pPrevImgbtn = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_makeupprevrespreviewbtn");
				DUIButton* pNextImgbtn = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_makeupnextrespreviewbtn");
				if (pImportmakeupimgbtn)
				{
					pImportmakeupimgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnBtnImportMakeupImgCmdEvent, this));
				}
				if (pPrevImgbtn)
				{
					pPrevImgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuPrevResPreviewBtnCmdEvent, this));
				}
				if (pNextImgbtn)
				{
					pNextImgbtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuNextResPreviewBtnCmdEvent, this));
				}
				m_pMakeupResListCombobox = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_makeupreslistcombobox");
				if (m_pMakeupResListCombobox)
				{
					m_pMakeupResListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnMakeupResListComboChangeEvent, this));
				}
				m_pMakeupTagListCombobox = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_MakeupTagCombobox");
				if (m_pMakeupTagListCombobox)
				{
					m_pMakeupTagListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnMakeupTagListComboChangeEvent, this));
				}
			}

			if (!m_pMakeUpPosEditItem[0])//下面为同一个panel的控件
			{
				m_pMakeUpPosEditItem[0] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"leftitemedit");
				m_pMakeUpPosEditItem[1] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"topitemedit");
				m_pMakeUpPosEditItem[2] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"rightitemedit");
				m_pMakeUpPosEditItem[3] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"bottomitemedit");
				m_pMakeUpPosEditItem[4] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"scalratioedit");
				m_pMakeUpPosEditItem[5] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"widthedit");
				m_pMakeUpPosEditItem[6] = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<PosEdit>(L"heightedit");
			}
		}
	}
	if (m_pMakeupMajorAttrList)
	{
		m_pMakeupMajorAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_pMakeupMajorAttrList->m_ChildPanelArray.GetCount(); DMASSERT(count == 1);
		for (int i = 0; i < count; i++)
		{
			if (!m_pMakeupTargetFPSCombobox)
			{
				m_pMakeupTargetFPSCombobox = m_pMakeupMajorAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_TargetFPSCombobox");
				if (m_pMakeupTargetFPSCombobox)
				{
					m_pMakeupTargetFPSCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnMakeupTargetFPSEditInputEnter, this));
					m_pMakeupTargetFPSCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pMakeupTargetFPSCombobox->m_pEdit->GetEventMask());
					m_pMakeupTargetFPSCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnMakupTargetFPSEditChange, this));
					m_pMakeupTargetFPSCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnMakeupTargetFPSComboChangeEvent, this));
				}
				m_pMakeupAllowDiscardFrameCombobox = m_pMakeupMajorAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_AllowDiscardFrameCombobox");
				if (m_pMakeupAllowDiscardFrameCombobox)
				{
					m_pMakeupAllowDiscardFrameCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnMakeupAllowDiscardFrameComboChangeEvent, this));
				}
			}
		}
	}

	if (m_pFaceExchangeAttrList)
	{
		m_pFaceExchangeAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_pFaceExchangeAttrList->m_ChildPanelArray.GetCount(); DMASSERT(count == 1);
		for (int i = 0; i < count; i++)
		{
			if (!m_pMaxFaceSuppportedListCombobox)
			{
				m_pMaxFaceSuppportedListCombobox = m_pFaceExchangeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_maxfacesupportedtypeCombobox");
				if (m_pMaxFaceSuppportedListCombobox)
				{
					m_pMaxFaceSuppportedListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnMaxFaceSupportedListComboChangeEvent, this));
				}
			}
		}
	}
	if (m_pBackgroundEdgeAttrList)
	{
		m_pBackgroundEdgeAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_pBackgroundEdgeAttrList->m_ChildPanelArray.GetCount(); DMASSERT(count == 1);
		for (int i = 0; i < count; i++)
		{
			if (!m_pBackgroundEdgeWidthListCombobox)
			{
				m_pBackgroundEdgeWidthListCombobox = m_pBackgroundEdgeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_backgroundedgewidthtypeCombobox");
				if (m_pBackgroundEdgeWidthListCombobox)
				{
					m_pBackgroundEdgeWidthListCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBackgroundEdgeWidthListComboChangeEvent, this));
				}
				m_pBackgroundEdgeColorSetCtrl = m_pBackgroundEdgeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIPAddressCtrl>(L"ds_backgroundedgeColorsetCtrl");
				for (int j = 0; j < 4; j++)
				{
					m_pBackgroundEdgeColorSetCtrl->m_pEdit[j]->SetEventMask(ENM_CHANGE | m_pBackgroundEdgeColorSetCtrl->m_pEdit[j]->GetEventMask());
					m_pBackgroundEdgeColorSetCtrl->m_pEdit[j]->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBackgroundEdgeColorSetCtrlEditChange, this));
				}
				m_pBackgroundEdgeColorBtn = m_pBackgroundEdgeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_BackGroundEdgeColorbtn");
				if (m_pBackgroundEdgeColorBtn)
				{
					m_pBackgroundEdgeColorBtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnBackgroundEdgeColorBtnCmdEvent, this));
				}
			}
		}
	}
	if (m_pBeautifyAttrList)
	{
		m_pBeautifyAttrList->m_EventMgr.SubscribeEvent(DMEventTCExpandArgs::EventID, Subscriber(&EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent, this));
		int count = (int)m_pBeautifyAttrList->m_ChildPanelArray.GetCount(); DMASSERT(count == 1);
		for (int i = 0; i < count; i++)
		{
			if (!m_pBeautifyEnlargeEyeCombobox)
			{
				m_pBeautifyEnlargeEyeCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_EnlargeEyeCombobox"); DMASSERT(m_pBeautifyEnlargeEyeCombobox);
				if (m_pBeautifyEnlargeEyeCombobox)
				{
					m_pBeautifyEnlargeEyeCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifyEnlargeEyeCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifyEnlargeEyeCombobox->m_pEdit->GetEventMask());
					m_pBeautifyEnlargeEyeCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EnlargeEye_EditChanged, this));
					m_pBeautifyEnlargeEyeCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EnlargeEye_ComboSelectChanged, this));
				}
				m_pBeautifyShrinkFaceCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_ShrinkFaceCombobox"); DMASSERT(m_pBeautifyShrinkFaceCombobox);
				if (m_pBeautifyShrinkFaceCombobox)
				{
					m_pBeautifyShrinkFaceCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifyShrinkFaceCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifyShrinkFaceCombobox->m_pEdit->GetEventMask());
					m_pBeautifyShrinkFaceCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_ShrinkFace_EditChanged, this));
					m_pBeautifyShrinkFaceCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_ShrinkFace_ComboSelectChanged, this));
				}
				m_pBeautifySmallFaceCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_SmallFaceCombobox"); DMASSERT(m_pBeautifySmallFaceCombobox);
				if (m_pBeautifySmallFaceCombobox)
				{
					m_pBeautifySmallFaceCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifySmallFaceCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifySmallFaceCombobox->m_pEdit->GetEventMask());
					m_pBeautifySmallFaceCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_SmallFace_EditChanged, this));
					m_pBeautifySmallFaceCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_SmallFace_ComboSelectChanged, this));
				}
				m_pBeautifyReddenCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_ReddenCombobox"); DMASSERT(m_pBeautifyReddenCombobox);
				if (m_pBeautifyReddenCombobox)
				{
					m_pBeautifyReddenCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifyReddenCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifyReddenCombobox->m_pEdit->GetEventMask());
					m_pBeautifyReddenCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Redden_EditChanged, this));
					m_pBeautifyReddenCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Redden_ComboSelectChanged, this));
				}
				m_pBeautifyWhittenCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_WhittenCombobox"); DMASSERT(m_pBeautifyWhittenCombobox);
				if (m_pBeautifyWhittenCombobox)
				{
					m_pBeautifyWhittenCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifyWhittenCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifyWhittenCombobox->m_pEdit->GetEventMask());
					m_pBeautifyWhittenCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Whitten_EditChanged, this));
					m_pBeautifyWhittenCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Whitten_ComboSelectChanged, this));
				}
				m_pBeautifySmoothCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_SmoothCombobox"); DMASSERT(m_pBeautifySmoothCombobox);
				if (m_pBeautifySmoothCombobox)
				{
					m_pBeautifySmoothCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifySmoothCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifySmoothCombobox->m_pEdit->GetEventMask());
					m_pBeautifySmoothCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Smooth_EditChanged, this));
					m_pBeautifySmoothCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Smooth_ComboSelectChanged, this));
				}
				m_pBeautifyConstrastCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_ContrastCombobox"); DMASSERT(m_pBeautifyConstrastCombobox);
				if (m_pBeautifyConstrastCombobox)
				{
					m_pBeautifyConstrastCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifyConstrastCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifyConstrastCombobox->m_pEdit->GetEventMask());
					m_pBeautifyConstrastCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Contrast_EditChanged, this));
					m_pBeautifyConstrastCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Contrast_ComboSelectChanged, this));
				}
				m_pBeautifySaturtionCombobox = m_pBeautifyAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_SaturationCombobox"); DMASSERT(m_pBeautifySaturtionCombobox);
				if (m_pBeautifySaturtionCombobox)
				{
					m_pBeautifySaturtionCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_EditInputEnter, this));
					m_pBeautifySaturtionCombobox->m_pEdit->SetEventMask(ENM_CHANGE | m_pBeautifySaturtionCombobox->m_pEdit->GetEventMask());
					m_pBeautifySaturtionCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Saturation_EditChanged, this));
					m_pBeautifySaturtionCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnBeautify_Saturation_ComboSelectChanged, this));
				}
			}
		}
	}

	DMASSERT(m_p2DStickerResListCombobox);
	DMASSERT(m_pTraceposListCombobox);
	DMASSERT(m_pStickerBlendModeCombobox);
	DMASSERT(m_pStickerTargetFPSCombobox);
	DMASSERT(m_pStickerGroupSizeCombobox);
	DMASSERT(m_pStickerGroupIndexCombobox);
	DMASSERT(m_pStickerAllowDiscardFrameCombobox);
	DMASSERT(m_pStickerPositionTypeCombobox);
	DMASSERT(m_pStickerPositionRelationTypeCombobox);
	DMASSERT(m_pStickerHotLinkCombobox);
	DMASSERT(m_pMakeupResListCombobox);
	DMASSERT(m_pMakeupTagListCombobox);
	DMASSERT(m_pMakeupTargetFPSCombobox);
	DMASSERT(m_pMakeupAllowDiscardFrameCombobox);

	DMASSERT(m_pMaxFaceSuppportedListCombobox);
	DMASSERT(m_pBackgroundEdgeWidthListCombobox);
	DMASSERT(m_pBackgroundEdgeColorSetCtrl);
	DMASSERT(m_pBackgroundEdgeColorBtn);
	DMASSERT(m_pBeautifyEnlargeEyeCombobox);
	DMASSERT(m_pBeautifyShrinkFaceCombobox);
	DMASSERT(m_pBeautifySmallFaceCombobox);
	DMASSERT(m_pBeautifyReddenCombobox);
	DMASSERT(m_pBeautifyWhittenCombobox);
	DMASSERT(m_pBeautifySmoothCombobox);
	DMASSERT(m_pBeautifyConstrastCombobox);
	DMASSERT(m_pBeautifySaturtionCombobox);

	DMASSERT(m_pTransitionTreeCtrl);
	DMASSERT(m_pMakeUpPosEditItem[0]);
	DMASSERT(m_pMakeUpPosEditItem[1]);
	DMASSERT(m_pMakeUpPosEditItem[2]);
	DMASSERT(m_pMakeUpPosEditItem[3]);
	DMASSERT(m_pStickerPosEditItem[0]);
	DMASSERT(m_pStickerPosEditItem[1]);
	DMASSERT(m_pStickerPosEditItem[2]);
	DMASSERT(m_pStickerPosEditItem[3]);

	m_pObjEditor = g_pMainWnd->FindChildByNameT<DUIObjEditor>(L"ds_objeditor"); DMASSERT(m_pObjEditor);
	m_pObjEditor->InitObjEditor();
	m_pObjEditor->SetDesignMode(SelectMode);
	m_pObjEditor->DM_SetVisible(true);

	OnClosePreviewPanel(NULL);
	DragAcceptFiles(TRUE);

	CStringW strCrashOrg, strCrashBak;
	g_pCtrlXml->GetCrashBakProj(strCrashOrg, strCrashBak);
	if (!strCrashOrg.IsEmpty() && !strCrashBak.IsEmpty())
	{
		ShowWindow(SW_SHOW);
		CStringW strFormat;
		strFormat.Format(L"检测到程序发生崩溃，已将工程：%s 备份到程序目录，是否打开和编辑该备份工程？(如果点否，将删除该临时备份：%s)", (LPCWSTR)strCrashOrg, (LPCWSTR)strCrashBak);
		if (ED_MessageBox(strFormat, MB_YESNOCANCEL, L"提示", m_hWnd) == IDYES)
		{
			HandleProjectOpen(strCrashBak);
		}
		else
		{
			RemoveDir(strCrashBak);
		}
	}

// 	CStringW strTeset;
// 	m_pObjEditor->SetString(strTeset);
	return TRUE;
}

void EDMainWnd::SetMainWndTitleText(CStringW strTitle)
{
	SetWindowText(strTitle);
	DUIWindow* pTitleWnd = FindChildByNameT<DUIWindow>(L"mainwndTitle"); DMASSERT(pTitleWnd);
	if (pTitleWnd)
	{
		pTitleWnd->SetAttribute(L"text", strTitle);
	}
}

void EDMainWnd::OnDestroy()
{
	StopPreviewTimeLine();
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlZAccel, this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlRAccel, this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlSAccel, this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlNAccel, this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_F12Accel,   this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlQAccel, this);
	GetContainer()->GetAccelMgr()->UnregisterAccel(m_CtrlOAccel, this);
	SetMsgHandled(FALSE);
}

bool EDMainWnd::OnAccelPressed(const DUIAccel& Accel)
{
	HWND hLastMainWnd = ::GetActiveWindow();
	if (hLastMainWnd != m_hWnd)
		return false;

	DUIAccel acc = Accel;
	if (m_CtrlZAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		HandleGlobalMenu(GLBMENU_BACKSTEP);
	}
	else if(m_CtrlRAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		HandleGlobalMenu(GLBMENU_FORWORDSTEP);
	}
	else if (m_CtrlSAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		if (!m_strProjectName.IsEmpty())
			HandleProjectSave();
	}
	else if (m_CtrlNAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		HandleGlobalMenu(GLBMENU_NEWPROJ);
	}
	else if (m_F12Accel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		if (!m_strProjectName.IsEmpty())
			HandleFileSaveAs();
	}
	else if (m_CtrlQAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		if (!m_strProjectName.IsEmpty())
			HandleProjectClose();
	}
	else if (m_CtrlOAccel.FormatHotkey().Compare(acc.FormatHotkey()) == 0)
	{
		HandleProjectOpen();
	}

	return false;
}

DMCode EDMainWnd::CreateSdkPreviewWnd()
{
	do
	{
		if (m_pSDKPreviewWnd && m_pSDKPreviewWnd->IsWindow())
		{
			break;
		}

		CStringW strXml = L"<dm initsize=\"330, 500\" minsize=\"100, 150\" bresize=\"0\" btranslucent=\"0\">\
						   		<root clrbg=\"pbgra(2c, 26, 20, ff)\" />\
							</dm>";
		std::string utfContent = EDBASE::w2u(strXml);

		m_pSDKPreviewWnd.Release();
		m_pSDKPreviewWnd.Attach(new SDKPreviewWnd());
		m_pSDKPreviewWnd->DM_CreateWindowEx((void *)utfContent.c_str(), utfContent.length(), DM_DEF_WINDOW_NAME, WS_CHILD | WS_VISIBLE/*WS_POPUP*/, WS_EX_TOOLWINDOW | WS_EX_NOACTIVATE, 0, 0, 0, 0, m_hWnd, NULL, false);// 创建主窗口
	
//		m_pSDKPreviewWnd->ModifyStyle(WS_CAPTION, 0);
//		m_pSDKPreviewWnd->ModifyStyle(WS_POPUPWINDOW, WS_CHILD);
	//	m_pSDKPreviewWnd->SetParent(m_hWnd);

		m_pSDKPreviewWnd->SendMessage(WM_INITDIALOG);
	//	m_pSDKPreviewWnd->CenterWindow();
	//	m_pSDKPreviewWnd->DM_AnimateWindow(1200, AW_CENTER);
		m_pSDKPreviewWnd->ShowWindow(SW_SHOW);
		//m_pSDKPreviewWnd->SetWindowPos(0/*m_hWnd*/, 410, 220, 0, 0, SWP_NOSIZE);
		DUIWindow* pPreviewPanelWnd = FindChildByNameT<DUIWindow>(L"ds_PreviewPanelWnd");
		if (pPreviewPanelWnd)
		{
			pPreviewPanelWnd->DV_UpdateChildLayout(); //强制改变  函数内改变sdk预览窗口的位置
		}
	//	m_pSDKPreviewWnd->DM_AnimateWindow(1000, AW_SLIDE | AW_VER_POSITIVE);
	} while (false);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnPausePlayPreview()
{
	do 
	{
		if (!m_pSDKPreviewWnd)
			break;
		DUICheckBox* pChkBtn = FindChildByNameT<DUICheckBox>(L"PausePlayPreviewBtn");
		if (!pChkBtn)
			break;
		pChkBtn->DM_IsChecked() ? m_pSDKPreviewWnd->previewPause() : m_pSDKPreviewWnd->previewResume();
	} while (false);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnTimeline()
{
	DUIWindow* pShrinkScaleShowWnd = FindChildByNameT<DUIWindow>(L"shrinkscaleshowwnd");
	do
	{
		if (!pShrinkScaleShowWnd)
			break;

	/*	static int iPluse = 0;
		if (++iPluse % 2 != 0)
		{
			break;
		}
		iPluse = 0; */

		if (m_bShrinkingPreview)
		{
			m_iShrinkScale--;
			if (m_iShrinkScale <= m_iShrinkScaleDest)
			{
				StopPreviewTimeLine();
				m_iShrinkScale = m_iShrinkScaleDest;
			}
		}
		else
		{
			m_iShrinkScale++;
			if (m_iShrinkScale >= m_iShrinkScaleDest)
			{
				StopPreviewTimeLine();
				m_iShrinkScale = m_iShrinkScaleDest;
			}
		}
		
		CStringW strScale;
		strScale.Format(L"%d%s", m_iShrinkScale, L"%");

		pShrinkScaleShowWnd->SetAttribute(L"text", strScale);
	} while (false);
	return DM_ECODE_OK;
}

void  EDMainWnd::ProcessCrash()
{
	static bool bFirst = true;
	if (!bFirst)
		return;
	bFirst = false;
	
	if (m_strProjectName.IsEmpty()) //没有工程
	{
		return;
	}

	CStringW strOrgProj = m_strProjectName;
	wchar_t szDate[64] = { 0 };
	struct tm t;
	time_t t1 = time(nullptr);
	localtime_s(&t, &t1);
	wcsftime(szDate, 64, L"%Y-%m-%d %H.%M.%S", &t);

	CStringW strProjectCrsDir = GetProgramDirectory() + L"\\ProjCrashStore\\";
	CreateDirectoryW(strProjectCrsDir, NULL);
	strProjectCrsDir += szDate;
	CreateDirectoryW(strProjectCrsDir, NULL);

	if (_wcsicmp(m_strProjectName, L"Untitled") == 0)
	{
		strProjectCrsDir += L"\\Untitled";
		m_strJSonFilePath = m_strProjectName = strProjectCrsDir;
		m_strJSonFileJSonPath = m_strJSonFilePath + CStringW(L"\\") + CStringW(L"Untitled.json");
	}
	else
	{
		if (m_JSonMainParser.RecursiveCheckIsDataDirty())
		{
			if (m_strProjectName.ReverseFind(L'\\') > 0)
			{
				CStringW strProjectName = m_strProjectName.Right(m_strProjectName.GetLength() - m_strProjectName.ReverseFind(L'\\'));
				strProjectCrsDir += strProjectName;
				m_strJSonFilePath = m_strProjectName = strProjectCrsDir;
				m_strJSonFileJSonPath = m_strJSonFilePath + strProjectName + CStringW(L".json");
			}
		}
		else
			RemoveDirectory(strProjectCrsDir);
	}
	HandleFileSaveAs(false, false);
	if (m_strJSonFilePath.CompareNoCase(strOrgProj) != 0)
		g_pCtrlXml->AddCrashBakProj(strOrgProj, m_strJSonFilePath);
	g_pCtrlXml->~DMCtrlXml();
}

void EDMainWnd::OnSize(UINT nType, DM::CSize size)
{
	if (!IsIconic()) 
	{
		DM::CRect rcWnd;
		::GetWindowRect(m_hWnd, &rcWnd);
		::OffsetRect(&rcWnd, -rcWnd.left, -rcWnd.top);
		rcWnd.InflateRect(0,0,1,1);  //lzlong add 不然窗口的左边界和下边界的一个像素会被遮住  看不到边框
		HRGN hWndRgn = ::CreateRoundRectRgn(0, 0, rcWnd.right, rcWnd.bottom,3,3);
		SetWindowRgn(hWndRgn, TRUE);
		::DeleteObject(hWndRgn);
	}

	if (0 != size.cx && 0 != size.cy&&m_pMaxBtn&&m_pRestoreBtn)
	{
		if (SIZE_MAXIMIZED == nType)
		{
			m_pMaxBtn->DM_SetVisible(false);
			m_pRestoreBtn->DM_SetVisible(true);
		}
		else if (SIZE_RESTORED == nType)
		{
			m_pMaxBtn->DM_SetVisible(true);
			m_pRestoreBtn->DM_SetVisible(false);
		}
	}

	SetMsgHandled(FALSE);
}

void EDMainWnd::OnLButtonDbClick(UINT nFlags, CPoint pt)
{
	do
	{
		SetActiveWindow();
		if (NULL == m_pRestoreBtn || NULL == m_pMaxBtn)
		{
			break;
		}

		DUIWND hHit = HitTestPoint(pt);
		DUIWindow *pWnd = g_pDMApp->FindDUIWnd(hHit);
		
		if (hHit != 3)// 不在Root窗口的下层窗口 			
		{
			break;
		}

		if (m_pMaxBtn->DM_IsVisible())
		{
			OnMaximize();
		}
		else
		{
			OnRestore();
		}
	} while (false);
	SetMsgHandled(FALSE);
}

DMCode EDMainWnd::OnClose()
{
	return OnBtnClose();
}

DMCode EDMainWnd::OnBtnClose()
{
	if (m_JSonMainParser.RecursiveCheckIsDataDirty())
	{
		int result = ED_MessageBox(L"工程已被修改，是否保存当前工程?", MB_YESNOCANCEL, L"提示");
		if (result == IDYES)
		{
			if (!HandleProjectSave())
				return DM_ECODE_FAIL;
		}
		else if (result == IDCANCEL)
		{
			return DM_ECODE_FAIL;
		}
	}
	UnInstallProject();

	ShowWindow(SW_HIDE);
	DestroyWindow();
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnMinimize()
{
	SendMessage(WM_SYSCOMMAND, SC_MINIMIZE);

	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnMaximize()
{
	DM_SetVisible(false, false);
	SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
	DM_SetVisible(true, false);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnRestore()
{
	DM_SetVisible(false, false);
	SendMessage(WM_SYSCOMMAND, SC_RESTORE);
	DM_SetVisible(true, false);
	return DM_ECODE_OK;
}

void EDMainWnd::OnDropFiles(HDROP hDropInfo)
{
	do
	{
		int nDropCount = DragQueryFile(hDropInfo, -1, NULL, 0);
		if (1 != nDropCount)
		{
			break;
		}

		wchar_t szPath[MAX_PATH] = { 0 };
		DragQueryFile(hDropInfo, 0, (LPWSTR)szPath, MAX_PATH);

		//HitTestPoint()
		// 判断是否为目录
		if (IsDirectoryExist(szPath))
		{
			HandleProjectOpen(szPath);
		}
		else
		{
			if (wcsstr(szPath, L".json") > 0)
			{
				CStringW strPath = szPath;

				int iSlash = strPath.ReverseFind(L'\\');
				if (-1 == iSlash)
				{
					iSlash = strPath.ReverseFind(L'/');
				}
				strPath = strPath.Left(iSlash);
				HandleProjectOpen(strPath);
			}
		}
	} while (false);
}

void EDMainWnd::OnCommand(UINT uNotifyCode, int nID, HWND wndCtl)
{
	if (nID >= GLBMENU_BASE)
	{
		HandleGlobalMenu(nID);
	}
	else if (nID >= RESLIBLISTBOX_BASE && nID < RESLIBLISTBOX_MAX)
	{
		if (m_pZiyuankuListboxEx)	m_pZiyuankuListboxEx->HandleResLibMenu(nID);
	}
	else if (nID >= IMPRESLIBBTN_BASE && nID < IMPRESLIBBTN_MAX)
	{
		HandleImpResLibMenu(nID);
	}
	else if (nID >= EFFECTTEMPBTN_BASE && nID < EFFECTTEMPBTN_MAX)
	{
		HandlAddEffectMenu(nID);
	}
	else if (nID >= EFFECTTREEMENU_BASE && nID < EFFECTTREEMENU_MAX)
	{
		HDMTREEITEM hItem = NULL;
		if (nID == EFFECTTREEMENU_RENAME || nID == EFFECTTREEMENU_DELETE || nID == EFFECTTREEMENU_DELEEFFECT || nID == EFFECTTREEMENU_UPITEM || nID == EFFECTTREEMENU_DOWNITEM)
		{
			hItem = m_pEffectTreeCtrl->GetSelectedItem();
		}

		HandleEffectTreeMenu(nID, hItem ? hItem : m_hTreeAddPopupItem);
	}
	else if (nID >= TransitionMenu_BASE && nID < TransitionMenu_MAX)
	{
		if (m_pTransitionTreeCtrl)
			m_pTransitionTreeCtrl->OnCommand(uNotifyCode, nID, wndCtl);
	}
	else if (nID >= EDITORELEMENT_BASE && nID < EDITORELEMENT_MAX)
	{
		HDMTREEITEM hItem = m_pEffectTreeCtrl->GetSelectedItem();
		HandleEditorElementMenu(nID, hItem);
	}
}

DMCode EDMainWnd::HandleGlobalMenu(int nID)
{
	switch (nID)
	{
	case GLBMENU_NEWEMPTYPROJ:
	{
		HandleProjectNew(L"Untitled");
	}
	break;
	case GLBMENU_NEWPROJ:
	{
		DMSmartPtrT<NewResDlg> pDlg;
		pDlg.Attach(new NewResDlg());
		int iRet = pDlg->DoModal(L"ds_newdlg", m_hWnd, true);
		SetForegroundWindow(m_hWnd);
		if (IDOK == iRet)//create project
		{
			if (pDlg->m_strResDir.Right(1) == L'\\' || pDlg->m_strResDir.Right(1) == L'/')
			{
				pDlg->m_strResDir = pDlg->m_strResDir.Left(pDlg->m_strResDir.GetLength() - 1);
			}
			m_strProjectName = pDlg->m_strResDir;

			int iSlash = m_strProjectName.ReverseFind(L'\\');
			if (-1 == iSlash)
			{
				iSlash = m_strProjectName.ReverseFind(L'/');
			}
			DMASSERT(iSlash > 0);
			CStringW strFolderName = m_strProjectName.Right(m_strProjectName.GetLength() - iSlash - 1);
			CStringW strJSonFileJSonPath = pDlg->m_strResDir + CStringW(L"\\") + strFolderName + CStringW(L".json");
			if (PathFileExists(strJSonFileJSonPath))
			{
				if (IDYES == ED_MessageBox(L"目录下已有工程存在，是否直接打开该工程？", MB_YESNO, L"提示", m_hWnd))
				{
					HandleProjectOpen(m_strJSonFilePath);
					return DM_ECODE_OK;
				}
			}

			HandleProjectNew(m_strProjectName);
			m_strJSonFilePath = pDlg->m_strResDir;
			m_strJSonFileJSonPath = strJSonFileJSonPath;
			g_pCtrlXml->AddRecentResDir(m_strProjectName);
		}
		else if (IDRETRY == iRet) //open recent
		{
			HandleProjectOpen(pDlg->m_strResDir);
		}
	}
	break;
	case GLBMENU_OPEN:
	{
		HandleProjectOpen();
	}
	break;
	case GLBMENU_CLOSE:
	{
		HandleProjectClose();
	}
	break;
	case GLBMENU_OPENRECENT:
	{
		HandleGlobalMenu(GLBMENU_NEWPROJ);
	}
	break;
	case GLBMENU_SAVE:
	{
		HandleProjectSave();
	}
	break;
	case GLBMENU_EXPORTZIP:
	{
		OnExportProjectToZip(NULL);
	}
	break;
	case GLBMENU_SAVEAS:
	{
		HandleFileSaveAs();
	}
	break;
	case GLBMENU_BACKSTEP:
	{
		m_actionSlotMgr.ExcutePrevSiblingAction();
	}
	break;
	case GLBMENU_FORWORDSTEP:
	{
		m_actionSlotMgr.ExcuteNextSiblingAction();
	}
	break;
	case GLBMENU_SELECTALL:
	{

	}
	break;
	case GLBMENU_CHINESELAN:
	{
	}
	break;
	case GLBMENU_ENGLISHLAN:
	{
		
	}
	break;
	case GLBMENU_ONLINEHELP:
	{
		SHELLEXECUTEINFOW shelli = { 0 };
		shelli.cbSize = sizeof(SHELLEXECUTEINFOW);
		shelli.fMask = SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;
		shelli.lpVerb = L"open";
		shelli.lpFile = L"http://www.baidu.com";
		shelli.nShow = SW_SHOW;
	//	::ShellExecuteExW(&shelli);
	}
	break;
	case GLBMENU_CHECKUPDATE:
	{
		SHELLEXECUTEINFOW shelli = { 0 };
		shelli.cbSize = sizeof(SHELLEXECUTEINFOW);
		shelli.fMask = SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;
		shelli.lpVerb = L"open";
		shelli.lpFile = L"http://release.pc.bigo.sg/workstore/Setup/bigo_effect_creator/";
		shelli.nShow = SW_SHOW;
		::ShellExecuteExW(&shelli);
	}
	break;
	case GLBMENU_FEEDBACK:
	{
		ED_MessageBox(L"请把问题和建议反馈给：liaozhilong1@bigo.sg。（如果程序有崩溃，请把程序目录下的CrashDump目录一起拷贝出来）", MB_OK, L"反馈");
	}
	break;
	default:
		break;
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::SetParameInfoTabCurSel(int iIndex)
{
	int iRet = DM_ECODE_FAIL;
	do 
	{
		DUITabCtrl* pParameInfoPanelTb = FindChildByNameT<DUITabCtrl>(L"ds_ParameInfoPaneltabctrl");
		if (!pParameInfoPanelTb)
			break;
		pParameInfoPanelTb->SetCurSel(iIndex);
	} while (false);
	return DM_ECODE_OK;
}

int EDMainWnd::GetParameInfoTabCurSel()
{
	int iRet = -1;
	do
	{
		DUITabCtrl* pParameInfoPanelTb = FindChildByNameT<DUITabCtrl>(L"ds_ParameInfoPaneltabctrl");
		if (!pParameInfoPanelTb)
			break;
		iRet = pParameInfoPanelTb->GetCurSel();
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnZiyuankuListboxSeleChanged()
{
	int iRet = DM_ECODE_FAIL;
	do 
	{
		if (!m_pZiyuankuListboxEx)
			break;

		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
			break;

		if (m_pEffectTreeCtrl) m_pEffectTreeCtrl->SelectItem(NULL); //强制取消选择
		SetParameInfoTabCurSel(PARAMEINFOTAB_RESLIBNODE);
		iRet = DM_ECODE_OK;

		LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
		if (!lpData)
		{
			DMASSERT(FALSE);
			break;
		}

		EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData;
		if (pResourceParser->m_ArrImageList.GetCount() > 0)
		{
			OnUpdateReslibPreviewPng(pResourceParser->m_ArrImageList[pResourceParser->m_iCurResPreviewPngIndex], pResourceParser->m_ArrImageList.GetCount(), pResourceParser->m_iCurResPreviewPngIndex, pResourceParser->m_pResourceParserOwner ? true : false, pResourceParser->m_strResTag);
		}
		else
			DMASSERT(FALSE);
	} while (false);

	if (DM_ECODE_OK != iRet)
		SetParameInfoTabCurSel(PARAMEINFOTAB_NULL);
	return iRet;
}

DMCode EDMainWnd::OnUpdateSettingPanelInfo(EDJSonParser* pJSonParser, bool bChgTab)
{
	int iRet = DM_ECODE_OK;
	int iChgNewPanelTab = -1;
	int iOldPanelTab = GetParameInfoTabCurSel();
	do 
	{
		if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDJSonMainParser::GetClassName()) ||
			0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsParser::GetClassName())) //主节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_NULL;
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDFaceMorphParser::GetClassName()))//Makeup大节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_MAKEUPMAJOR;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDFaceMorphParser* pFaceMorphParser = dynamic_cast<EDFaceMorphParser*>(pJSonParser);
				DUIWndAutoMuteGuard guard(m_pMakeupTargetFPSCombobox->m_pEdit);
				m_pMakeupTargetFPSCombobox->m_pEdit->SetWindowTextW(std::to_wstring(pFaceMorphParser->m_iTargetFPS).c_str()); //美妆TargetFps列表

				DUIWndAutoMuteGuard guard2(m_pMakeupAllowDiscardFrameCombobox);
				m_pMakeupAllowDiscardFrameCombobox->SetCurSel(pFaceMorphParser->m_bAllowDiscardFrame ? 0 : 1);
			}			
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDMakeupsNodeParser::GetClassName()))//Makeup子节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_MAKEUPNODE;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pJSonParser);
				SucaiListInsertToComboItem(TAGMAKEUPNODE, pMakeupNodeParser, m_pMakeupResListCombobox);
				OnUpdateMakeupTaglistItemCombo(pMakeupNodeParser);
				OnUpdateMakeupPreviewPng(pJSonParser);
			}
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()))//Parts子节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_2DSTICKERNODE;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pJSonParser);
				On2DEffectItemAttrUpdate(pPartsNodeParser);
				SucaiListInsertToComboItem(TAG2DSTICKER, pPartsNodeParser, m_p2DStickerResListCombobox);
				OnUpdate2dStickerPreviewPng(pJSonParser);
			}
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDTransitionsNodeParser::GetClassName()))//Transition子节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_TRANSITIONNODE;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDTransitionsNodeParser* pTransitionNodeParser = dynamic_cast<EDTransitionsNodeParser*>(pJSonParser);
				if (pTransitionNodeParser) m_pStaTransitionName->SetAttribute(L"text", std::wstring(EDBASE::a2w(pTransitionNodeParser->m_strJSonMemberKey)).c_str());
				m_pTransitionTreeCtrl->OnCommand(0, TransitionMenu_ShowTransition, 0, pJSonParser);
			}
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDBackgroundEdgeParser::GetClassName()))//BackGroundEdge节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_BACKGROUNDEDGE;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDBackgroundEdgeParser* pBackgroundEdgeParser = dynamic_cast<EDBackgroundEdgeParser*>(pJSonParser); DMASSERT(pBackgroundEdgeParser);
				int iSelect = 0;
				switch (pBackgroundEdgeParser->m_iWidth)
				{
				case 2:
					iSelect = 0;
					break;
				case 3:
					iSelect = 1;
					break;
				case 5:
					iSelect = 2;
					break;
				case 6:
					iSelect = 3;
					break;
				case 8:
					iSelect = 4;
					break;
				}
				DUIWndAutoMuteGuard guard(m_pBackgroundEdgeWidthListCombobox->GetListBox());
				m_pBackgroundEdgeWidthListCombobox->SetCurSel(iSelect);

				DMColor clr = pBackgroundEdgeParser->m_iColor;
				m_iBackGroundEdgeColor.r = clr.a;//商汤的保存的rgb有自己的顺序  需要做调整
				m_iBackGroundEdgeColor.g = clr.r;
				m_iBackGroundEdgeColor.b = clr.g;
				m_iBackGroundEdgeColor.a = clr.b;
				CStringW strClr;
				strClr.Format(L"pbgra(%02x,%02x,%02x,%02x)", m_iBackGroundEdgeColor.b, m_iBackGroundEdgeColor.g, m_iBackGroundEdgeColor.r, m_iBackGroundEdgeColor.a);
				m_pBackgroundEdgeColorBtn->SetAttribute(L"clrbg", strClr);
				DUIWndAutoMuteGuard guard0(m_pBackgroundEdgeColorSetCtrl->m_pEdit[0]); DUIWndAutoMuteGuard guard1(m_pBackgroundEdgeColorSetCtrl->m_pEdit[1]);
				DUIWndAutoMuteGuard guard2(m_pBackgroundEdgeColorSetCtrl->m_pEdit[2]); DUIWndAutoMuteGuard guard3(m_pBackgroundEdgeColorSetCtrl->m_pEdit[3]);
				m_pBackgroundEdgeColorSetCtrl->SetAddress(m_iBackGroundEdgeColor.r, m_iBackGroundEdgeColor.g, m_iBackGroundEdgeColor.b, m_iBackGroundEdgeColor.a);
			}
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDBeautifyParser::GetClassName()))//Beautify节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_BEAUTIFY;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				EDBeautifyParser* pBeautifyParser = dynamic_cast<EDBeautifyParser*>(pJSonParser); DMASSERT(pBeautifyParser);
				DUIWndAutoMuteGuard guard1(m_pBeautifyEnlargeEyeCombobox->m_pEdit);	DUIWndAutoMuteGuard guard2(m_pBeautifyShrinkFaceCombobox->m_pEdit);
				DUIWndAutoMuteGuard guard3(m_pBeautifySmallFaceCombobox->m_pEdit);	DUIWndAutoMuteGuard guard4(m_pBeautifyReddenCombobox->m_pEdit);
				DUIWndAutoMuteGuard guard5(m_pBeautifyWhittenCombobox->m_pEdit);	DUIWndAutoMuteGuard guard6(m_pBeautifySmoothCombobox->m_pEdit);
				DUIWndAutoMuteGuard guard7(m_pBeautifyConstrastCombobox->m_pEdit);	DUIWndAutoMuteGuard guard8(m_pBeautifySaturtionCombobox->m_pEdit);

				m_pBeautifyEnlargeEyeCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbEnlargeRatio));
				m_pBeautifyShrinkFaceCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbShrinkRatio));
				m_pBeautifySmallFaceCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbSmallRatio));
				m_pBeautifyReddenCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbReddenStrength));
				m_pBeautifyWhittenCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbWhitenStrength));
				m_pBeautifySmoothCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbSmoothStrength));
				m_pBeautifyConstrastCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbContrastStrength));
				m_pBeautifySaturtionCombobox->m_pEdit->SetWindowTextW(DoubleToString(pBeautifyParser->m_dbSaturation));
				m_dbEnlargeRatio = pBeautifyParser->m_dbEnlargeRatio; m_dbShrinkRatio = pBeautifyParser->m_dbShrinkRatio;
				m_dbSmallRatio = pBeautifyParser->m_dbEnlargeRatio;	m_dbReddenStrength = pBeautifyParser->m_dbReddenStrength;
				m_dbWhitenStrength = pBeautifyParser->m_dbWhitenStrength;m_dbSmoothStrength = pBeautifyParser->m_dbSmoothStrength;
				m_dbContrastStrength = pBeautifyParser->m_dbContrastStrength; m_dbSaturation = pBeautifyParser->m_dbSaturation;
			}
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDFaceExchangeParser::GetClassName()))//FaceExchange节点
		{
			iChgNewPanelTab = PARAMEINFOTAB_FACEEXCHANGE;
			if (bChgTab || !(!bChgTab && iChgNewPanelTab != iOldPanelTab)) //不需要change Table  当前table跟新table不是同一个  不需要更新信息
			{
				DUIWndAutoMuteGuard guard(m_pMaxFaceSuppportedListCombobox->GetListBox());
				EDFaceExchangeParser* pFaceExchangeParser = dynamic_cast<EDFaceExchangeParser*>(pJSonParser); DMASSERT(pFaceExchangeParser);
				m_pMaxFaceSuppportedListCombobox->SetCurSel(pFaceExchangeParser->m_iMaxFaceSupported - 3 < 0 ? 0 : pFaceExchangeParser->m_iMaxFaceSupported - 3);
			}
		}
		else
			iRet = DM_ECODE_FAIL;

		if (bChgTab && -1 != iChgNewPanelTab)
		{
			SetParameInfoTabCurSel(iChgNewPanelTab);
		}
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnEffectTreeSeleChanged(DMEventArgs* pEvent)
{
	int iRet = DM_ECODE_FAIL;
	do 
	{
		DMEventTCSelChangedArgs* pEvnt = (DMEventTCSelChangedArgs*)pEvent;
		HDMTREEITEM hItem = pEvnt->m_hNewSel;
		if (DMTVI_ROOT == hItem || NULL == m_pEffectTreeCtrl)
		{
			break;
		}
		if (m_pZiyuankuListboxEx && m_pZiyuankuListboxEx->GetCurSel() >= 0)
			m_pZiyuankuListboxEx->DM_OnKillFocus();//失去选中状态

		if (NULL == hItem)
		{
			if (m_hObjSel)
			{
				ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
				if (pData && pData->m_pRootWnd)
				{
					pData->m_pRootWnd->m_pCurSeleDUIWnd = NULL;
					m_pObjEditor->HideHoverFrameWnd();
					m_pObjEditor->HideDragFrameWnd();
					m_pObjEditor->HideReferenceLineFrameWnd();
				}
			}			
			SetParameInfoTabCurSel(PARAMEINFOTAB_NULL);
			m_hObjSel = NULL;
			break;
		}
		if (m_hObjSel == hItem)
		{
			break;
		}
		m_hObjSel = hItem;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}
		if (!pData->m_pRootWnd)
		{
			if (EFFECTTEMPBTN_TRANSITIONSNODE == pData->m_iEffectType ||//transition节点
				EFFECTTEMPBTN_BACKGROUNDEDGE == pData->m_iEffectType  ||//backgroundedge节点
				EFFECTTEMPBTN_BEAUTIFY == pData->m_iEffectType		  ||//beautify
				EFFECTTEMPBTN_FACEEXCHANGE == pData->m_iEffectType ) //faceexchange
			{
				OnUpdateSettingPanelInfo(pData->m_pJsonParser, true);
				iRet = DM_ECODE_OK;
			}
			break;
		}
		DMASSERT(pData->m_pDUIWnd);
		m_pObjEditor->ShowDesignChild(pData->m_pRootWnd);
		if (m_hObjSel && 0 == m_pEffectTreeCtrl->GetParentItem(m_hObjSel) || EFFECTTEMPBTN_MAKEUPS == pData->m_iEffectType)//选中父节点
		{
			m_pObjEditor->SetDesignMode(FixedMode);
			pData->m_pRootWnd->m_pCurSeleDUIWnd = NULL;
			m_pObjEditor->m_pDuiPos->UnInitLayout();
		}
		else if (pData->m_pRootWnd->m_pCurSeleDUIWnd != pData->m_pDUIWnd)//选中子节点
		{
			m_pObjEditor->SetDesignMode(SelectMode);
			pData->m_pRootWnd->m_pCurSeleDUIWnd = pData->m_pDUIWnd;
			if (EFFECTTEMPBTN_PARTNODE == pData->m_iEffectType)//2D贴纸节点
				m_pObjEditor->m_pDuiPos->InitLayout(pData->m_pDUIWnd->m_pLayout, (PosEdit**)&m_pStickerPosEditItem);
			else//美妆节点
				m_pObjEditor->m_pDuiPos->InitLayout(pData->m_pDUIWnd->m_pLayout, (PosEdit**)&m_pMakeUpPosEditItem);

			m_pObjEditor->DragFrameInSelMode();
			m_pObjEditor->ReferenceLineFrameInSelMode();
		}

		if (EFFECTTEMPBTN_2DSTICKERMAJOR == pData->m_iEffectType || EFFECTTEMPBTN_PARTS == pData->m_iEffectType || EFFECTTEMPBTN_MAKEUPS == pData->m_iEffectType)
		{
			OnUpdateSettingPanelInfo(pData->m_pJsonParser, true);
		}
		else if (/*EFFECTTEMPBTN_2DSTIKERNODE == pData->m_iEffectType || */EFFECTTEMPBTN_PARTNODE == pData->m_iEffectType)//2D贴纸节点
		{
			for (size_t i = 0; i < countof(pData->m_bAttrlistExp); i++)
			{
				if (m_p2DStickerNodeAttrList) m_p2DStickerNodeAttrList->ExpandItem(i, pData->m_bAttrlistExp[i]);
			}
			OnUpdateSettingPanelInfo(pData->m_pJsonParser, true);
		}
		else if (EFFECTTEMPBTN_NOSE == pData->m_iEffectType || EFFECTTEMPBTN_BLUSH == pData->m_iEffectType || EFFECTTEMPBTN_EYESHADOW == pData->m_iEffectType
			|| EFFECTTEMPBTN_EYE == pData->m_iEffectType || EFFECTTEMPBTN_LIPV2 == pData->m_iEffectType || EFFECTTEMPBTN_FACETRANSPLANT == pData->m_iEffectType)//美妆 子节点
		{
			for (size_t i = 0; i < countof(pData->m_bAttrlistExp); i++)
			{
				if (m_pMakeupNodeAttrList) m_pMakeupNodeAttrList->ExpandItem(i, pData->m_bAttrlistExp[i]);
			}
			OnUpdateSettingPanelInfo(pData->m_pJsonParser, true);
		}
		iRet = DM_ECODE_OK;
	} while (false);
	if (DM_ECODE_OK != iRet) SetParameInfoTabCurSel(PARAMEINFOTAB_NULL); //如果没有选中的tab默认选中空
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnOpenPreviewPanel(DMEventArgs* pEvent)
{
	DUIButton* pOpenPreviewPanelBtn = dynamic_cast<DUIButton*>(pEvent->m_pSender);
	if (pOpenPreviewPanelBtn)
	{
		DUIWindow* pParentWnd = pOpenPreviewPanelBtn->DM_GetWindow(GDW_PARENT);
		if (pParentWnd) pParentWnd->DM_SetVisible(false);
	}

	DUIWindow* pDuiPrevWnd = FindChildByNameT<DUIWindow>(L"ds_PreviewPanel"); DMASSERT(pDuiPrevWnd);
	if (pDuiPrevWnd)
	{
		pDuiPrevWnd->SetAttribute(L"bvisible", L"1");
	}

	DUIWindow* pSplitLayout = pDuiPrevWnd->DM_GetWindow(GDW_PARENT);
	if (pSplitLayout)
	{
		pSplitLayout->SetAttribute(DMAttr::DUISplitLayoutExAttr::bool_bhideonepartition, L"0");
		pSplitLayout->DV_UpdateChildLayout();
	}
	if (m_pSDKPreviewWnd)
	{
		m_pSDKPreviewWnd->ShowWindow(SW_SHOW);
	}

	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnClosePreviewPanel(DMEventArgs* pEvent)
{
	DUIButton* pOpenPreviewPanelBtn = FindChildByNameT<DUIButton>(L"openpreviewpanelbutton");
	if (pOpenPreviewPanelBtn)
	{
		DUIWindow* pParentWnd = pOpenPreviewPanelBtn->DM_GetWindow(GDW_PARENT);
		if (pParentWnd) pParentWnd->DM_SetVisible(true);
	}

	DUIWindow* pDuiPrevWnd = FindChildByNameT<DUIWindow>(L"ds_PreviewPanel"); DMASSERT(pDuiPrevWnd);
	if (pDuiPrevWnd)
	{
		DUIWindow* pSplitLayout = pDuiPrevWnd->DM_GetWindow(GDW_PARENT);
		if (pSplitLayout)
		{
			pSplitLayout->SetAttribute(DMAttr::DUISplitLayoutExAttr::bool_bhideonepartition, L"1");
			pSplitLayout->DV_UpdateChildLayout();
		}
	}
	if (m_pSDKPreviewWnd)
	{
		m_pSDKPreviewWnd->ShowWindow(SW_HIDE);
	}

	return DM_ECODE_OK;
}

DMCode EDMainWnd::StopPreviewTimeLine()
{
	return GetContainer()->OnUnregisterTimeline(this);
}

DMCode EDMainWnd::OnShrinkPreview(DMEventArgs* pEvent)
{
	StopPreviewTimeLine();
	if (PRIVIEWSHRINKMINIMUM == m_iShrinkScale)
		return DM_ECODE_FAIL;
	m_bShrinkingPreview = true;
	if (m_iShrinkScale == m_iShrinkScaleDest) //已经停止了timer
	{
		m_iShrinkScaleDest = m_iShrinkScale - PRIVIEWSHRINKENLARGESCOPE;
	}
	else //双击  在目标基础上再递减
		m_iShrinkScaleDest -= PRIVIEWSHRINKENLARGESCOPE;

	if (m_iShrinkScaleDest < PRIVIEWSHRINKMINIMUM)
	{
		m_iShrinkScaleDest = PRIVIEWSHRINKMINIMUM;
	}
	GetContainer()->OnRegisterTimeline(this);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnEnlargePreview(DMEventArgs* pEvent)
{
	StopPreviewTimeLine();
	if (PRIVIEWENLARGEMAXIMUM == m_iShrinkScale)
		return DM_ECODE_FAIL;
	m_bShrinkingPreview = false;
	if (m_iShrinkScale == m_iShrinkScaleDest) //已经停止了timer
	{
		m_iShrinkScaleDest = m_iShrinkScale + PRIVIEWSHRINKENLARGESCOPE;
	}
	else //双击  在目标基础上再递增
		m_iShrinkScaleDest += PRIVIEWSHRINKENLARGESCOPE;

	if (m_iShrinkScaleDest > PRIVIEWENLARGEMAXIMUM)
	{
		m_iShrinkScaleDest = PRIVIEWENLARGEMAXIMUM;
	}
	GetContainer()->OnRegisterTimeline(this);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnExportProjectToZip(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (m_strJSonFilePath.IsEmpty() || m_JSonMainParser.RecursiveCheckIsDataDirty())
		{
			ED_MessageBox(L"请先保存工程！", MB_OK, L"提示");
			break;
		}

		CStringT SavePath;
		if (!SaveZipDocumentProjDlg(m_strJSonFilePath, SavePath))
			break;
		
		CStringT SavePathTmp = SavePath;
		SavePathTmp.MakeLower();
		if (SavePathTmp.Find(L".zip") != SavePathTmp.GetLength() - 4)
		{
			SavePath += L".zip";
		}
		 
		CStringW str7zPath = GetProgramDirectory() + CStringW(L"\\7z\\7z.exe");
		CStringW strOutputPath = GetProgramDirectory() + CStringW(L"\\7z\\OutPut.txt");
		SECURITY_ATTRIBUTES sa = { sizeof(sa), NULL, TRUE };
		HANDLE hStdoutRedirect = CreateFileW(
			(LPCWSTR)strOutputPath,
			GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			&sa, 
			OPEN_ALWAYS /*| TRUNCATE_EXISTING*/,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		
		PROCESS_INFORMATION pi = { 0 };
		STARTUPINFO si = { 0 };
		si.cb = sizeof(si);
		si.wShowWindow = SW_HIDE;
		if (hStdoutRedirect != INVALID_HANDLE_VALUE)
		{
			si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
			si.hStdOutput = hStdoutRedirect;
		}

		BOOL success = FALSE;
		TCHAR szCommand[MAX_PATH];
		swprintf_s(szCommand, MAX_PATH, L"%s a \"%s\" \"%s\"", (LPCWSTR)str7zPath, (LPCWSTR)SavePath, (LPCWSTR)m_strJSonFilePath);
		success = CreateProcessW(NULL, szCommand, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
		if (success) {
			CloseHandle(pi.hThread);
			if (WAIT_OBJECT_0 == WaitForSingleObject(pi.hProcess, 2000))
			{
				if (hStdoutRedirect != INVALID_HANDLE_VALUE)
				{
					CloseHandle(hStdoutRedirect);
					hStdoutRedirect = INVALID_HANDLE_VALUE;

					HANDLE hRead = CreateFileW(
						(LPCWSTR)strOutputPath,
						GENERIC_READ,
						FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
						&sa,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						NULL);

					if (hRead != INVALID_HANDLE_VALUE)
					{
						char szOutPut[2048]; DWORD dwRead;
						memset(szOutPut, 0, sizeof(szOutPut));
						ReadFile(hRead, szOutPut, 2048, &dwRead, NULL);
						if (strstr(szOutPut, "Everything is Ok") != NULL)
						{
							OpenFolderAndSelectFile(SavePath);
							ED_MessageBox(L"导出成功！", MB_OK, L"提示");							
							iRet = DM_ECODE_OK;
						}
						CloseHandle(hRead);
					}
				}
			}
			else
			{
			}
			CloseHandle(pi.hProcess);
		}
		else {
			DWORD dwErr = GetLastError();
		}
		if (hStdoutRedirect != INVALID_HANDLE_VALUE)
			CloseHandle(hStdoutRedirect);
	} while (false);
	return iRet;
}

bool EDMainWnd::SaveZipDocumentProjDlg(const CStringW& projName, CStringT& SavePath)
{
	OPENFILENAME ofn;// 公共对话框结构。
	TCHAR szFile[MAX_PATH]; // 保存获取文件名称的缓冲区。     

	// 初始化选择文件对话框。
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = m_hWnd;
	ofn.lpstrFile = szFile;
	CStringW name = (projName.IsEmpty() ? (L"*.zip") : (projName + L".zip"));
	wcscpy_s(szFile, (LPCWSTR)name);
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = _T("zip File(*.zip)");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_SHOWHELP | OFN_OVERWRITEPROMPT;
	
	// 显示打开选择文件对话框。
	if (GetSaveFileName(&ofn))
	{
		SavePath = szFile;
		return true;
	}
	return false;
}

DMCode EDMainWnd::OnAddTempletEffect()
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		DUIButton* pAddEffectButton = FindChildByNameT<DUIButton>(L"AddEffectButton");  DMASSERT(pAddEffectButton);
		if (!pAddEffectButton)
			break;
		DMXmlDocument Doc;
		g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
		DMXmlNode XmlNode = Doc.Root();
		XmlNode.SetAttribute(L"itemhei", L"32");
		XmlNode.SetAttribute(L"font", L"face:微软雅黑, size : 14, weight : 400");
		XmlNode.SetAttribute(L"iconoffset", L"25");
		XmlNode.SetAttribute(L"textoffset", L"5");

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_2DEFFECT - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_2DEFFECT - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(L"iconoffset", L"0"); XmlItem.SetAttribute(L"iconsize", L"0,0"); XmlItem.SetAttribute(L"textoffset", L"10");
		XmlItem.SetAttribute(XML_BDISABLE, L"1");
		
		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"0");
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_STICKERSMAJOR - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_STICKERSMAJOR - EFFECTTEMPBTN_BASE].text);
		
		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"2");
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_TRANSITIONS - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_TRANSITIONS - EFFECTTEMPBTN_BASE].text);

		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"0");
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_2DSTICKERMAJOR - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_2DSTICKERMAJOR - EFFECTTEMPBTN_BASE].text);
		if (m_pEffectTreeCtrl->HasEffectTypeRootNode(EFFECTTEMPBTN_2DSTICKERMAJOR))
		{
			CStringW strText = g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_2DSTICKERMAJOR - EFFECTTEMPBTN_BASE].text;
			strText += (L"(仅支持一个)");
			XmlItem.SetAttribute(XML_BDISABLE, L"1");
			XmlItem.SetAttribute(XML_TEXT, strText);
		}

		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"2");//滤镜
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_FILTER - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_FILTER - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"1");//美妆
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_MAKEUPV2 - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_MAKEUPV2 - EFFECTTEMPBTN_BASE].text);
		if (m_pEffectTreeCtrl->HasEffectTypeRootNode(EFFECTTEMPBTN_MAKEUPV2))
		{
			CStringW strText = g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_MAKEUPV2 - EFFECTTEMPBTN_BASE].text;
			strText += (L"(仅支持一个)");
			XmlItem.SetAttribute(XML_BDISABLE, L"1");
			XmlItem.SetAttribute(XML_TEXT, strText);
		}

		{
			DMXmlNode XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_NOSE - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_NOSE - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

			XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_BLUSH - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_BLUSH - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

			XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_EYESHADOW - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_EYESHADOW - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

			XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_EYE - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_EYE - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

			XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_LIPV2 - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_LIPV2 - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

			XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
			XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_FACETRANSPLANT - EFFECTTEMPBTN_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_FACETRANSPLANT - EFFECTTEMPBTN_BASE].text);
			XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");
		}

		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"3"); //染发
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_HAIRCOLOR - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_HAIRCOLOR - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"4");//抠出人像
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_PORTRAITMAT - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_PORTRAITMAT - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_3DEFFECT - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_3DEFFECT - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(L"iconoffset", L"0"); XmlItem.SetAttribute(L"iconsize", L"0,0"); XmlItem.SetAttribute(L"textoffset", L"10");
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(L"height", L"60");	XmlItem.SetAttribute(L"skin", L"ds_commingsooneffect");
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_COMINGSOON - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_COMINGSOON - EFFECTTEMPBTN_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, L"1"); XmlItem.SetAttribute(L"textoffset", L"23");

		DUIMenu Menu;
		Menu.LoadMenu(XmlNode);
		CRect rcButton;
		pAddEffectButton->DV_GetWindowRect(rcButton);
		ClientToScreen(rcButton);
		CPoint pt(rcButton.left, rcButton.bottom);
		Menu.TrackPopupMenu(0, pt.x + 1, pt.y + 3, m_hWnd);
		iErr = DM_ECODE_OK;
	} while (false);

	return iErr;
}

DMCode EDMainWnd::PopRBtnDownEffectTreeMenu(HDMTREEITEM hItem)
{
	DMASSERT(hItem);
	if (NULL == hItem || DMTVI_ROOT == hItem || NULL == m_pEffectTreeCtrl)
	{
		return DM_ECODE_FAIL;
	}
	DMXmlDocument Doc;
	g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
	DMXmlNode XmlNode = Doc.Root();
	XmlNode.SetAttribute(L"MaxWidth", L"100");

	bool bInsertDelete = false;
	ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
	if (NULL == pData && !pData->IsValid())
		return DM_ECODE_FAIL;

	if (EFFECTTEMPBTN_PARTS == pData->m_iEffectType) //顶级root节点
	{
	}
	else if (EFFECTTEMPBTN_TRANSITIONS == pData->m_iEffectType)
	{
	}
	else if (EFFECTTEMPBTN_MAKEUPS == pData->m_iEffectType || EFFECTTEMPBTN_MAKEUPNODE == pData->m_iEffectType || EFFECTTEMPBTN_PARTNODE == pData->m_iEffectType
		|| EFFECTTEMPBTN_FACEEXCHANGE == pData->m_iEffectType || EFFECTTEMPBTN_BACKGROUNDEDGE == pData->m_iEffectType || EFFECTTEMPBTN_BEAUTIFY == pData->m_iEffectType)
	{
		bInsertDelete = true;
	}
	else if (EFFECTTEMPBTN_NOSE == pData->m_iEffectType || EFFECTTEMPBTN_BLUSH == pData->m_iEffectType || EFFECTTEMPBTN_EYESHADOW == pData->m_iEffectType ||
			 EFFECTTEMPBTN_EYE == pData->m_iEffectType || EFFECTTEMPBTN_LIPV2 == pData->m_iEffectType || EFFECTTEMPBTN_FACETRANSPLANT == pData->m_iEffectType)
	{
		bInsertDelete = true;
	}
	else if (EFFECTTEMPBTN_MAKEUPV2 == pData->m_iEffectType)
	{
		bInsertDelete = true;
		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDEFFECT - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDEFFECT - EFFECTTREEMENU_BASE].text);
		XmlItem.SetAttribute(XML_SKIN, L"ds_submenubgexp");
		DMXmlNode XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].text);
		XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].text);
		XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].text);
		XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].text);
		XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].text);
		XmlChildItem = XmlItem.InsertChildNode(XML_ITEM);
		XmlChildItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].id)); XmlChildItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].text);
	}
	
	DMXmlNode XmlItem;
	if (EFFECTTEMPBTN_TRANSITIONSNODE == pData->m_iEffectType)
	{
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_RENAME - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_RENAME - EFFECTTREEMENU_BASE].text);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_DELETE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_DELETE - EFFECTTREEMENU_BASE].text);
	}
	else if (EFFECTTEMPBTN_BEAUTIFY == pData->m_iEffectType)
	{
	}
	else if (m_pEffectTreeCtrl->GetParentItem(hItem))
	{
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_UPITEM - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_UPITEM - EFFECTTREEMENU_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, m_pEffectTreeCtrl->GetPrevSiblingItem(hItem) ? L"0" : L"1");
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_DOWNITEM - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_DOWNITEM - EFFECTTREEMENU_BASE].text);
		if (m_pEffectTreeCtrl->GetNextSiblingItem(hItem))
		{
			ObjTreeDataPtr pNextData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetNextSiblingItem(hItem));
			if (pNextData && pNextData->m_iEffectType == EFFECTTEMPBTN_BEAUTIFY)
			{
				XmlItem.SetAttribute(XML_BDISABLE, L"1");
			}
		}
		else
			XmlItem.SetAttribute(XML_BDISABLE, L"1");
	}
	
	if (bInsertDelete)
	{
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_DELETE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_DELETE - EFFECTTREEMENU_BASE].text);
	}

	if (XmlNode.IsValid())
	{
		DUIMenu Menu;
		Menu.LoadMenu(XmlNode);
		POINT pt;
		GetCursorPos(&pt);
		Menu.TrackPopupMenu(0, pt.x, pt.y, m_hWnd);
	}

	return DM_ECODE_OK;
}

DMCode EDMainWnd::PopEditorElementMenu(HDMTREEITEM hItem)
{
	DMASSERT(hItem);
	if (NULL == hItem || DMTVI_ROOT == hItem || NULL == m_pEffectTreeCtrl)
	{
		return DM_ECODE_FAIL;
	}
	DMXmlDocument Doc;
	g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
	DMXmlNode XmlNode = Doc.Root();
	XmlNode.SetAttribute(L"MaxWidth", L"75");
	XmlNode.SetAttribute(L"itemhei", L"28");

	ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem);
	if (NULL == pData && !pData->IsValid())
		return DM_ECODE_FAIL;
	int nType = pData->m_iEffectType;

	DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
	XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_UPELEMENT - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_UPELEMENT - EDITORELEMENT_BASE].text);
	XmlItem.SetAttribute(XML_BDISABLE, m_pEffectTreeCtrl->GetPrevSiblingItem(hItem) ? L"0" : L"1");

	XmlItem = XmlNode.InsertChildNode(XML_ITEM);
	XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_DOWNELEMENT - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_DOWNELEMENT - EDITORELEMENT_BASE].text);
	if (m_pEffectTreeCtrl->GetNextSiblingItem(hItem))
	{
		ObjTreeDataPtr pNextData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetNextSiblingItem(hItem));
		if (pNextData && pNextData->m_iEffectType == EFFECTTEMPBTN_BEAUTIFY)
		{
			XmlItem.SetAttribute(XML_BDISABLE, L"1");
		}
	}
	else
		XmlItem.SetAttribute(XML_BDISABLE, L"1");

	if (!m_pEffectTreeCtrl->IsItemEyeDisable(hItem))
	{
		if (m_pEffectTreeCtrl->IsItemEyeChk(hItem))
		{
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_SETUNVISIBLE - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_SETUNVISIBLE - EDITORELEMENT_BASE].text);
		}
		else
		{
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_SETVISIBLE - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_SETVISIBLE - EDITORELEMENT_BASE].text);
		}
	}

	XmlItem = XmlNode.InsertChildNode(XML_ITEM);
	XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_IMPORTIMG - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_IMPORTIMG - EDITORELEMENT_BASE].text);
	//XmlItem = XmlNode.InsertChildNode(XML_ITEM);
	//XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_RESETPOSELEMENT - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_RESETPOSELEMENT - EDITORELEMENT_BASE].text);
	XmlItem = XmlNode.InsertChildNode(XML_ITEM);
	XmlItem.SetAttribute(XML_ID, IntToString(g_EditorElementMenuItem[EDITORELEMENT_DELEELEMENT - EDITORELEMENT_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EditorElementMenuItem[EDITORELEMENT_DELEELEMENT - EDITORELEMENT_BASE].text);

	DUIMenu Menu;
	Menu.LoadMenu(XmlNode);
	POINT pt;
	GetCursorPos(&pt);
	Menu.TrackPopupMenu(0, pt.x, pt.y, m_hWnd);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::PopEffectTreeAddBtnMenu(HDMTREEITEM hItem)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (NULL == hItem || DMTVI_ROOT == hItem || NULL == m_pEffectTreeCtrl)
		{
			return DM_ECODE_FAIL;
		}
		m_hTreeAddPopupItem = hItem;

		DM::LPTVITEMEX Data = m_pEffectTreeCtrl->GetItem(hItem);
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem);
		if (NULL == Data || NULL == pData || !pData->IsValid())
			break;

		if (EFFECTTEMPBTN_MAKEUPV2 == pData->m_iEffectType || EFFECTTEMPBTN_MAKEUPS == pData->m_iEffectType)
		{
			DMXmlDocument Doc;
			g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
			DMXmlNode XmlNode = Doc.Root();
			XmlNode.SetAttribute(L"MaxWidth", L"130");
			XmlNode.SetAttribute(L"itemhei", L"34");

			DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].text);

			CRect rc;
			Data->pPanel->OnGetContainerRect(&rc);
			ClientToScreen(&rc);

			DUIMenu Menu;
			Menu.LoadMenu(XmlNode);
			POINT pt;
			GetCursorPos(&pt);
			Menu.TrackPopupMenu(0, rc.right - 40, rc.top, m_hWnd);
			iErr = DM_ECODE_OK;
		}
		else if (EFFECTTEMPBTN_STICKERSMAJOR == pData->m_iEffectType || EFFECTTEMPBTN_PARTS == pData->m_iEffectType)
		{
			DMXmlDocument Doc;
			g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
			DMXmlNode XmlNode = Doc.Root();
		/*	XmlNode.SetAttribute(L"MaxWidth", L"155");
			XmlNode.SetAttribute(L"itemhei", L"34");

			DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADD2DSTICKER - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADD2DSTICKER - EFFECTTREEMENU_BASE].text);
			
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDMAKEUPS - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDMAKEUPS - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDFaceMorphParser::GetClassNameW()) ? L"0" : L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEEXCHANGE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEEXCHANGE - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDFaceExchangeParser::GetClassNameW()) ? L"0" : L"1");
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBACKGROUNDEDGE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBACKGROUNDEDGE - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDBackgroundEdgeParser::GetClassNameW()) ? L"0" : L"1");
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEDEFORMATION - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEDEFORMATION - EFFECTTREEMENU_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBEAUTIFY - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBEAUTIFY - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDBeautifyParser::GetClassNameW()) ? L"0" : L"1");
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_IMPORT3DSTICKER - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_IMPORT3DSTICKER - EFFECTTREEMENU_BASE].text);
			*/
			XmlNode.SetAttribute(L"itemhei", L"32");
			XmlNode.SetAttribute(L"font", L"face:微软雅黑, size : 14, weight : 400");
			XmlNode.SetAttribute(L"iconoffset", L"25");
			XmlNode.SetAttribute(L"textoffset", L"5");

			DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_TEXT, L"Stickers");
			XmlItem.SetAttribute(L"iconoffset", L"0"); XmlItem.SetAttribute(L"iconsize", L"0,0"); XmlItem.SetAttribute(L"textoffset", L"10");
			XmlItem.SetAttribute(XML_BDISABLE, L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"0");
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADD2DSTICKER - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADD2DSTICKER - EFFECTTREEMENU_BASE].text);

			XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"1");//美妆
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDMAKEUPS - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDMAKEUPS - EFFECTTREEMENU_BASE].text);
			{
				DMXmlNode XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

				XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

				XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

				XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

				XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");

				XmlItemNode = XmlItem.InsertChildNode(XML_ITEM); XmlItemNode.SetAttribute(L"skin", L"ds_SubmenubgSkin");
				XmlItemNode.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].id)); XmlItemNode.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE].text);
				XmlItemNode.SetAttribute(L"iconoffset", L"0"); XmlItemNode.SetAttribute(L"iconsize", L"0,0"); XmlItemNode.SetAttribute(L"textoffset", L"18");
			}
			XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"4");//FaceExchange
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEEXCHANGE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDFACEEXCHANGE - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDFaceExchangeParser::GetClassNameW()) ? L"0" : L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"2");//Background edge
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBACKGROUNDEDGE - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBACKGROUNDEDGE - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDBackgroundEdgeParser::GetClassNameW()) ? L"0" : L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM); XmlItem.SetAttribute(L"icon", L"3"); //Beautify
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBEAUTIFY - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDBEAUTIFY - EFFECTTREEMENU_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, NULL == m_JSonMainParser.FindChildParserByClassName(EDBeautifyParser::GetClassNameW()) ? L"0" : L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_3DEFFECT - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_3DEFFECT - EFFECTTEMPBTN_BASE].text);
			XmlItem.SetAttribute(L"iconoffset", L"0"); XmlItem.SetAttribute(L"iconsize", L"0,0"); XmlItem.SetAttribute(L"textoffset", L"10");
			XmlItem.SetAttribute(XML_BDISABLE, L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(L"height", L"60");	XmlItem.SetAttribute(L"skin", L"ds_commingsooneffect");
			XmlItem.SetAttribute(XML_BDISABLE, L"1");

			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_COMINGSOON - EFFECTTEMPBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_COMINGSOON - EFFECTTEMPBTN_BASE].text);
			XmlItem.SetAttribute(XML_BDISABLE, L"1"); XmlItem.SetAttribute(L"textoffset", L"23");

			CRect rc;
			Data->pPanel->OnGetContainerRect(&rc);
			ClientToScreen(&rc);
			DUIMenu Menu;
			Menu.LoadMenu(XmlNode);
			POINT pt;
			GetCursorPos(&pt);
			Menu.TrackPopupMenu(0, rc.right - 40, rc.top, m_hWnd);
			iErr = DM_ECODE_OK;
		}
		else if (EFFECTTEMPBTN_TRANSITIONS == pData->m_iEffectType)
		{
			DMXmlDocument Doc;
			g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
			DMXmlNode XmlNode = Doc.Root();
			XmlNode.SetAttribute(L"MaxWidth", L"110");
			XmlNode.SetAttribute(L"itemhei", L"34");

			DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_EffectTreeMenuItem[EFFECTTREEMENU_ADDTRANSITION - EFFECTTREEMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_EffectTreeMenuItem[EFFECTTREEMENU_ADDTRANSITION - EFFECTTREEMENU_BASE].text);

			CRect rc;
			Data->pPanel->OnGetContainerRect(&rc);
			ClientToScreen(&rc);
			DUIMenu Menu;
			Menu.LoadMenu(XmlNode);
			POINT pt;
			GetCursorPos(&pt);
			Menu.TrackPopupMenu(0, rc.right - 40, rc.top, m_hWnd);
			iErr = DM_ECODE_OK;
		}
	} while (false);
	return iErr;
}

DMCode EDMainWnd::OnImportReslib()
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		DUIButton* pImportResButton = FindChildByNameT<DUIButton>(L"ImportResButton");  DMASSERT(pImportResButton);
		if (!pImportResButton)
			break;
		DMXmlDocument Doc;
		g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
		DMXmlNode XmlNode = Doc.Root();
		XmlNode.SetAttribute(L"MaxWidth", L"126");
		XmlNode.SetAttribute(L"textoffset", L"5");

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPIMGFRAME - IMPRESLIBBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPIMGFRAME - IMPRESLIBBTN_BASE].text);
		//XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		//XmlItem.SetAttribute(XML_ID, IntToString(g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPGIFFILE - IMPRESLIBBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPGIFFILE - IMPRESLIBBTN_BASE].text);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPFROMFOLDER - IMPRESLIBBTN_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_ImpResLibBtnMenuItem[IMPRESLIBBTN_IMPFROMFOLDER - IMPRESLIBBTN_BASE].text);
		
		DUIMenu Menu;
		Menu.LoadMenu(XmlNode);
		CRect rcButton;
		pImportResButton->DV_GetWindowRect(rcButton);
		ClientToScreen(rcButton);
		CPoint pt(rcButton.left, rcButton.bottom);
		Menu.TrackPopupMenu(0, pt.x+1, pt.y+5, m_hWnd);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::HandlAddEffectMenu(int nID, EDJSonParserPtr pJsonParser/* = NULL*/)
{
	if (!m_pEffectTreeCtrl)
		return DM_ECODE_FAIL;

	switch (nID)
	{
	case EFFECTTEMPBTN_STICKERSMAJOR: //"Stickers"
	{
		HDMTREEITEM hItemRoot = m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT);
		if (hItemRoot)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItemRoot);
			if (pData && EFFECTTEMPBTN_STICKERSMAJOR == pData->m_iEffectType) //exist
			{
				return DM_ECODE_FAIL;
			}
		}
		HDMTREEITEM hItem = m_pEffectTreeCtrl->InsertRootItem(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_STICKERSMAJOR - EFFECTTEMPBTN_BASE].text, (EffectTempletType)nID, DMTVI_FIRST, true, false);
		m_pEffectTreeCtrl->SetItemEyeVisible(hItem, false);
		BindObjTreeData(NULL, NULL, hItem, EFFECTTEMPBTN_STICKERSMAJOR, &m_JSonMainParser);
	}
	break;
	case EFFECTTEMPBTN_TRANSITIONS: //"Transitions"
	{
		EDJSonParser* pTransitionParser = pJsonParser;
		if (!pTransitionParser)
		{//点击按钮 生成Transition节点
			pTransitionParser = m_JSonMainParser.CreateChildParser(EDTransitionsParser::GetClassNameW()); DMASSERT(pTransitionParser);
		}
		HDMTREEITEM hItem = m_pEffectTreeCtrl->InsertRootItem(g_EffectTempletBtnMenuItem[EFFECTTEMPBTN_TRANSITIONS - EFFECTTEMPBTN_BASE].text, (EffectTempletType)nID, DMTVI_LAST, true, false);
		m_pEffectTreeCtrl->SetItemEyeVisible(hItem, false);
		BindObjTreeData(NULL, NULL, hItem, EFFECTTEMPBTN_TRANSITIONS, pTransitionParser);
	}
	break;
/*	case EFFECTTEMPBTN_2DSTICKERMAJOR: //"2D贴纸"
	{
		g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot() = true; //屏蔽添加子项  最后才动作监听
		//HDMTREEITEM hItem = add2DStickerMajor((EffectTempletType)nID);
		HDMTREEITEM hItem = m_pEffectTreeCtrl->InsertRootItem(L"2DSticker", (EffectTempletType)EFFECTTEMPBTN_2DSTICKERMAJOR, DMTVI_FIRST, true, false); DMASSERT(hItem);
		BindRootWndToObjEditor(hItem, EFFECTTEMPBTN_2DSTICKERMAJOR);
		PopEffectTreeAddBtnMenu(hItem);//统一入口  添加一项子项
		g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot() = false;
		m_pTransitionTreeCtrl->MarkStickerItemDirty();
	}
	break; */
// 	case EFFECTTEMPBTN_FILTER: //"滤镜"
// 	{
// 		m_pEffectTreeCtrl->InsertRootItem(L"Filter_1", (EffectTempletType)nID, DMTVI_FIRST, true, true);
// 	}
// 	break;
// 	case EFFECTTEMPBTN_HAIRCOLOR: //染发
// 	{
// 		m_pEffectTreeCtrl->InsertRootItem(L"HairColor_1", (EffectTempletType)nID, DMTVI_FIRST, true, true);
// 	}
// 	break;
// 	case EFFECTTEMPBTN_PORTRAITMAT: //抠出人像
// 	{
// 		m_pEffectTreeCtrl->InsertRootItem(L"SegmentBody", (EffectTempletType)nID, DMTVI_FIRST, true, true);
// 	}
// 	break;

/*	// 下面都是没用的了 6-17
	case EFFECTTEMPBTN_NOSE://鼻子
	case EFFECTTEMPBTN_BLUSH: //腮红
	case EFFECTTEMPBTN_EYESHADOW: //眼影
	case EFFECTTEMPBTN_EYE: //美瞳
	case EFFECTTEMPBTN_LIPV2: //唇彩
	case EFFECTTEMPBTN_FACETRANSPLANT: //换脸
	{
		HDMTREEITEM hItem = m_pEffectTreeCtrl->InsertRootItem(L"FaceMakeup", EFFECTTEMPBTN_MAKEUPV2, DMTVI_FIRST, true, false); DMASSERT(hItem);//美妆 
		if (!hItem)
			break;

		BindRootWndToObjEditor(hItem, EFFECTTEMPBTN_MAKEUPV2);
		g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot() = true; //屏蔽添加子项  最后才动作监听
		if (nID == EFFECTTEMPBTN_NOSE)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_NOSE, hItem);
		}
		else if (nID == EFFECTTEMPBTN_BLUSH)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_BLUSH, hItem);
		}
		else if (nID == EFFECTTEMPBTN_EYESHADOW)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_EYESHADOW, hItem);
		}
		else if (nID == EFFECTTEMPBTN_EYE)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_EYE, hItem);
		}
		else if (nID == EFFECTTEMPBTN_LIPV2)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_LIPV2, hItem);
		}
		else if (nID == EFFECTTEMPBTN_FACETRANSPLANT)
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_FACETRANSPLANT, hItem);
		}
		g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot() = false;
		m_pTransitionTreeCtrl->MarkStickerItemDirty();
	}
	break;  */
	default:
		break;
	}

	return DM_ECODE_OK;
}

DMCode EDMainWnd::NewJsonParserCreatedNotify(EDJSonParser* pJSonParser)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (!pJSonParser)
			break;

		if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsParser::GetClassNameW())) // parts
		{
			HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR); //顶级贴纸节点
		}
		else if (pJSonParser->V_GetClassName() == EDFaceMorphParser::GetClassNameW()) // EDFaceMorphParser
		{
			HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR);//顶级贴纸节点
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDMAKEUPS, pJSonParser->GetParser(GPS_PARENT)->m_hTreeBindItem, pJSonParser);
		}
		else if (pJSonParser->V_GetClassName() == EDTransitionsParser::GetClassNameW()) // EDTransitionsParser
		{
			HandlAddEffectMenu(EFFECTTEMPBTN_TRANSITIONS, pJSonParser);//Transition节点
		}
	/*	else if (pJSonParser->V_GetClassName() == EDTransitionsNodeParser::GetClassNameW()) // EDTransitionsNodeParser
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDTRANSITION, pJSonParser->GetParser(GPS_PARENT)->m_hTreeBindItem, pJSonParser);//Transition子节点
		}
		else if (pJSonParser->V_GetClassName() == EDConditionsParser::GetClassNameW()) // EDConditionsParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDConditionsNodeParser::GetClassNameW()) // EDConditionsNodeParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDTargetsParser::GetClassNameW()) // EDTargetsParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDTargetsNodeParser::GetClassNameW()) // EDTargetsNodeParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDBeautifyPartsParser::GetClassNameW()) // EDBeautifyPartsParser
		{
		}*/
		else if (pJSonParser->V_GetClassName() == EDBeautifyParser::GetClassNameW()) // EDBeautifyParser
		{
			if (!m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT))
				HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR);//顶级贴纸节点
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDBEAUTIFY, m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT), pJSonParser);//Beautify节点
		}
		else if (pJSonParser->V_GetClassName() == EDFaceExchangeParser::GetClassNameW()) // EDEDFaceExchangeParser
		{
			if (!m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT))
				HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR);//顶级贴纸节点
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDFACEEXCHANGE, m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT), pJSonParser);
		}
		else if (pJSonParser->V_GetClassName() == EDBackgroundEdgeParser::GetClassNameW()) // EDBackgroundEdgeParser
		{
			if (!m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT))
				HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR);//顶级贴纸节点
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDBACKGROUNDEDGE, m_pEffectTreeCtrl->GetChildItem((HDMTREEITEM)DMTVI_ROOT), pJSonParser);
		}
 		else if (pJSonParser->V_GetClassName() == EDResourceNodeParser::GetClassNameW())
 		{
 			OnReloadSucaikuListbox(NULL);//暂时没有数据填充
 		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

bool EDMainWnd::JSParseFinishTraversingEffectTree(HDMTREEITEM hItem)
{
	CArray<HDMTREEITEM> TreeHItemArray;
	HDMTREEITEM hSiblingItem = m_pEffectTreeCtrl->GetChildItem(hItem); //lzlong change to this  下面的遍历有问题
	while (hSiblingItem)
	{
		JSParseFinishTraversingEffectTree(hSiblingItem);//递归
		TreeHItemArray.Add(hSiblingItem);
		hSiblingItem = m_pEffectTreeCtrl->GetNextSiblingItem(hSiblingItem);
	}
	for (size_t index = 0; index < TreeHItemArray.GetCount(); index++) //一个一个向下移动
	{
		AdjustTreeItemPosByZPositon(TreeHItemArray[index]);
	}
	return true;
}

//导入json文件并解析节点完成
DMCode EDMainWnd::ParseJSNodeObjFinished(EDJSonParser* pJSonParser)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!pJSonParser)
			break;

		if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDJSonMainParser::GetClassNameW())) // main Node
		{
			JSParseFinishTraversingEffectTree(DMTVI_ROOT);
		}
		else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsNodeParser::GetClassNameW())) // parts Node
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_ADD2DSTICKER, pJSonParser->GetParser(GPS_PARENT)->GetParser(GPS_PARENT)->m_hTreeBindItem, pJSonParser); //partnode -> parts -> main
			OnUpdateSettingPanelInfo(pJSonParser, false);
		}
		else if (pJSonParser->V_GetClassName() == EDFaceMorphParser::GetClassNameW()) // EDFaceMorphParser
		{
			OnUpdateSettingPanelInfo(pJSonParser, false);
		}
 		else if (pJSonParser->V_GetClassName() == EDMakeupsParser::GetClassNameW()) // EDMakeupsParser
 		{
 		}
		else if (pJSonParser->V_GetClassName() == EDMakeupsNodeParser::GetClassNameW()) // EDMakeupsNodeParser
		{
			EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pJSonParser); DMASSERT(pMakeupNodeParser);
			if (pMakeupNodeParser->m_pRelativeResourceNodeParser && pMakeupNodeParser->m_pRelativeResourceNodeParser->m_ArrImageList.GetCount() < 1)
			{
				CStringW strFormat; strFormat.Format(L"节点：%s  %s", pMakeupNodeParser->m_pRelativeResourceNodeParser->m_strName, L"Makeup节点没有图片资源，将要删除该节点");
				DMASSERT_EXPR(FALSE, strFormat);
				EDJSonParser* pJSonResourceParser = pMakeupNodeParser->m_pRelativeResourceNodeParser;
				pMakeupNodeParser->DestoryParser();
				pJSonResourceParser->DestoryParser();
				break;
			}

			int nID = EFFECTTREEMENU_EYESHADOW;			
			if (pMakeupNodeParser->m_strTag == MAKEUPTAG_EYESHADOW)
			{
				nID = EFFECTTREEMENU_EYESHADOW;
			}
			else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_BLUSH)
			{
				nID = EFFECTTREEMENU_BLUSH;
			}
			else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_EYE)
			{
				nID = EFFECTTREEMENU_EYE;
			}
			else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_NOSE)
			{
				nID = EFFECTTREEMENU_NOSE;
			}
			else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_NONE)
			{
				nID = EFFECTTREEMENU_FACETRANSPLANT;
			}
			HandleEffectTreeMenu(nID, pJSonParser->GetParser(GPS_PARENT)->GetParser(GPS_PARENT)->m_hTreeBindItem, pJSonParser); //makeupnode -> makeup -> facemorph
			OnUpdateSettingPanelInfo(pJSonParser, false);
		}
		else if (pJSonParser->V_GetClassName() == EDTransitionsNodeParser::GetClassNameW()) // EDTransitionsNodeParser
		{
			HandleEffectTreeMenu(EFFECTTREEMENU_ADDTRANSITION, pJSonParser->GetParser(GPS_PARENT)->m_hTreeBindItem, pJSonParser);//Transition子节点
		}
		else if (pJSonParser->V_GetClassName() == EDTransitionsParser::GetClassNameW()) // EDTransitionsParser 完成解析  这时候需要设置Transition子节点的名字 这个时候子节点的名字才是正确的
		{
			EDJSonParser* pChildParser = pJSonParser->GetParser(GPS_FIRSTCHILD);
			while (pChildParser)
			{
				if (!pChildParser->m_hTreeBindItem)
				{
					DMASSERT(pChildParser->m_hTreeBindItem);
					break;
				}
				DM::LPTVITEMEX pData = m_pEffectTreeCtrl->GetItem(pChildParser->m_hTreeBindItem);
				if (!pData)
				{
					DMASSERT(pData);
					break;
				}
				m_pEffectTreeCtrl->RenameTreeItem(pData, std::wstring(EDBASE::a2w(pChildParser->m_strJSonMemberKey)).c_str());
				pChildParser = pChildParser->GetParser(GPS_NEXTSIBLING);
			}
		}
		else if (pJSonParser->V_GetClassName() == EDConditionsParser::GetClassNameW()) // EDConditionsParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDConditionsNodeParser::GetClassNameW()) // EDConditionsNodeParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDTargetsParser::GetClassNameW()) // EDTargetsParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDTargetsNodeParser::GetClassNameW()) // EDTargetsNodeParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDBeautifyPartsParser::GetClassNameW()) // EDBeautifyPartsParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDBeautifyParser::GetClassNameW()) // EDBeautifyParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDFaceExchangeParser::GetClassNameW()) // EDEDFaceExchangeParser
		{
		}
		else if (pJSonParser->V_GetClassName() == EDBackgroundEdgeParser::GetClassNameW()) // EDBackgroundEdgeParser
		{
			m_pEffectTreeCtrl->SetTreeItemEyeBtnChk(pJSonParser->m_hTreeBindItem, pJSonParser->IsParserEnable(), false);
		}
		else if (pJSonParser->V_GetClassName() == EDResourceNodeParser::GetClassNameW())
		{
			OnReloadSucaikuListbox(dynamic_cast<EDResourceNodeParser*>(pJSonParser));//有数据填充
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::JsonParserOnFreeNotify(EDJSonParser* pJSonParser)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!pJSonParser)
			break;

		if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDResourceNodeParser::GetClassNameW())) // EDResourceNode Node
		{
			OnRemoveSucaikuListboxItem(dynamic_cast<EDResourceNodeParser*>(pJSonParser));
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

CStringW EDMainWnd::JSonParserGetJSonFilePath()
{
	return m_strJSonFilePath;
}

bool TraversingRecursionCallback(DM::LPTVITEMEX* lpTvitem , LPARAM lp)
{
	do
	{
		if (!lpTvitem || !*lpTvitem || !(*lpTvitem)->lParam)
			break;

		MainParser* pMainParser = (MainParser*)lp;
		EDMainWnd* pThis = (EDMainWnd*)pMainParser->pMainWnd;
		EDJSonParser* pJSonParser = (EDJSonParser*)pMainParser->pParser;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)(*lpTvitem)->lParam;
		if (pData->m_pJsonParser == pJSonParser) //找到了 当前是关联的parser
		{
			if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDMakeupsNodeParser::GetClassName()) || 0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()))
			{
				EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pJSonParser);
				EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pJSonParser);
				if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDMakeupsNodeParser::GetClassName()) && pMakeupNodeParser)
				{
					pThis->m_pEffectTreeCtrl->RenameTreeItem(*lpTvitem, pMakeupNodeParser->m_strName);
					pThis->SucaiListInsertToComboItem(TAGMAKEUPNODE, pMakeupNodeParser, pThis->m_pMakeupResListCombobox);
				}
				if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()) && pPartsNodeParser)
				{
					if (pPartsNodeParser->m_pRelativeResourceNodeParser && pPartsNodeParser->m_pRelativeResourceNodeParser->m_ArrImageList.GetCount() < 1)
					{
						CStringW strFormat; strFormat.Format(L"节点：%s  %s", pPartsNodeParser->m_pRelativeResourceNodeParser->m_strName, L"partNode节点没有图片资源，将要删除该节点");
						DMASSERT_EXPR(FALSE, strFormat);
						pPartsNodeParser->m_pRelativeResourceNodeParser->DestoryParser();
						pThis->HandleEffectTreeMenu(EFFECTTREEMENU_DELEEFFECT, pData->m_pJsonParser->m_hTreeBindItem, pData->m_pJsonParser);
						break;
					}

					pThis->m_pEffectTreeCtrl->RenameTreeItem(*lpTvitem, std::wstring(EDBASE::a2w(pPartsNodeParser->m_strJSonMemberKey)).c_str());
					pThis->SucaiListInsertToComboItem(TAG2DSTICKER, pPartsNodeParser, pThis->m_p2DStickerResListCombobox);
				}

				if (pThis->m_pEffectTreeCtrl->GetSelectedItem() && *lpTvitem == pThis->m_pEffectTreeCtrl->GetItem(pThis->m_pEffectTreeCtrl->GetSelectedItem())) //更新的刚好是当前选中项  
				{
					if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDMakeupsNodeParser::GetClassName()))
					{
						pThis->OnUpdateMakeupPreviewPng(pJSonParser);
					}
					else if (0 == _wcsicmp(pJSonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()))
					{
						pThis->OnUpdate2dStickerPreviewPng(pJSonParser);
					}
					else
						DMASSERT(FALSE);
				}

				if (pThis->m_pEffectTreeCtrl->GetSelectedItem())
				{
					ObjTreeDataPtr pData = (ObjTreeDataPtr)pThis->m_pEffectTreeCtrl->GetItemData(pThis->m_pEffectTreeCtrl->GetSelectedItem());
					if (pData && pData->m_pJsonParser == pJSonParser)
					{
						pThis->OnUpdateEditorElemPng(pData);
					}
				}
				else
				{
					pThis->OnUpdateEditorElemPng(pData);
				}
			}
			else
				DMASSERT(FALSE);//更新资源  都是makeup和partnode
		}
	} while (false);	
	return false; //表示继续递归下去
}

DMCode EDMainWnd::RelativeResourceNodeParserChgNotify(EDJSonParser* pJSonParser)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (!pJSonParser || !m_pEffectTreeCtrl)
			break;

		MainParser mainparser;
		mainparser.pMainWnd = this;	mainparser.pParser = pJSonParser;
		m_pEffectTreeCtrl->TraversingRecursion(DMTVI_ROOT, TraversingRecursionCallback, (LPARAM)&mainparser);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::ResourceNodeOwnerParserChgNotify(EDJSonParser* pJSonParser)
{
	return OnReloadSucaikuListbox(dynamic_cast<EDResourceNodeParser*>(pJSonParser));
}

DMCode EDMainWnd::JSonParserDataDirtyNodtify(EDJSonParser* pJSonParser)
{
	if (!m_bJsonDataDirtyMark)
	{
		m_bJsonDataDirtyMark = true;
		SetMainWndTitle(m_bJsonDataDirtyMark);
	}
	return DM_ECODE_OK;
}

//调整Item的顺序  Beautify和deformation必须在底层
DMCode EDMainWnd::AdjustTreeItemPos(HDMTREEITEM hItem)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (!pData)
			break;
		if (pData->m_iEffectType == EFFECTTEMPBTN_BEAUTIFY || pData->m_iEffectType == EFFECTTEMPBTN_DEFORMATION) //Beautify和deformation必须在底层
			break;

		while (m_pEffectTreeCtrl->GetPrevSiblingItem(hItem))
		{
			HDMTREEITEM hPrevItem = m_pEffectTreeCtrl->GetPrevSiblingItem(hItem);
			ObjTreeDataPtr pPrevData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetPrevSiblingItem(hItem)); DMASSERT(pPrevData);
			if (!pPrevData)
				break;
			if (pPrevData->m_iEffectType == EFFECTTEMPBTN_BEAUTIFY || pPrevData->m_iEffectType == EFFECTTEMPBTN_DEFORMATION) //Beautify和deformation必须在底层 自己必须往上一层移动
			{
				HandleEditorElementMenu(EDITORELEMENT_UPELEMENT, hItem); //往上走
				iErr = DM_ECODE_OK;
			}
			else
				break;
		}
	} while (false);
	return iErr;
}

DMCode EDMainWnd::HandleEffectTreeMenu(int nID, HDMTREEITEM hItem, EDJSonParserPtr pJsonParser /*= NULL*/)
{
	if (!m_pEffectTreeCtrl)
		return DM_ECODE_FAIL;

	ObjTreeDataPtr pData = NULL;
	if (!hItem)
	{
		hItem = m_pEffectTreeCtrl->GetSelectedItem();
		DMASSERT(hItem);
		if (!hItem)
			return DM_ECODE_FAIL;
	}

	pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
	if (!pData)
	{
		return DM_ECODE_FAIL;
	}

	switch (nID)
	{
	case EFFECTTREEMENU_ADD2DSTICKER://"Add 2DSticker"
	{
		if (!pJsonParser)
		{//手动添加节点
			EDJSonParser* pPartsParser = NULL;
			if (_wcsicmp(pData->m_pJsonParser->V_GetClassName(), (LPCWSTR)EDPartsParser::GetClassName()) == 0)
			{
				pPartsParser = pData->m_pJsonParser;
			}
			else
			{
				pPartsParser = pData->m_pJsonParser->FindChildParserByClassName(EDPartsParser::GetClassNameW());
			}

			if (!pPartsParser)//没有创建parts
			{
				pPartsParser = pData->m_pJsonParser->CreateChildParser(EDPartsParser::GetClassNameW()); DMASSERT(pPartsParser);
				BindRootWndToObjEditor(hItem, EFFECTTEMPBTN_PARTS, pPartsParser);
			}
			EDJSonParser* pPartsNodeParser = pPartsParser->CreateChildParser(EDPartsNodeParser::GetClassNameW(), JSPARSER_FIRST); DMASSERT(pPartsNodeParser);

			int iRet = Show_ResSelectDlg(TAG2DSTICKER, pPartsNodeParser, m_hWnd);
			if (IDCONTINUE == iRet && OnImportImgCmdImp(NULL, pPartsNodeParser) == DM_ECODE_OK) //点击了导入图片
			{
				LPARAM lpData = m_p2DStickerResListCombobox->GetItemData(m_p2DStickerResListCombobox->GetCount() - 1); DMASSERT(lpData);
				if (lpData)
				{
					pPartsNodeParser->RelativeResourceNodeParser((EDResourceNodeParser*)lpData);
				}
			}
			else if (IDOK != iRet)
			{
				pPartsNodeParser->DestoryParser();
				break;
			}
			AddEffectNodeItem(hItem, std::wstring(EDBASE::a2w(pPartsNodeParser->m_strJSonMemberKey)).c_str(), true, false, true, (EffectTempletType)EFFECTTEMPBTN_PARTNODE, pPartsNodeParser);
			AdjustTreeItemPos(hItem); //调整Item的顺序  Beautify和deformation必须在底层
		}
		else
		{//json文件加载
			BindRootWndToObjEditor(hItem, EFFECTTEMPBTN_PARTS, pJsonParser->GetParser(GPS_PARENT));//绑定parts
			AddEffectNodeItem(hItem, std::wstring(EDBASE::a2w(pJsonParser->m_strJSonMemberKey)).c_str(), pJsonParser->IsParserEnable(), false, false, (EffectTempletType)EFFECTTEMPBTN_PARTNODE, pJsonParser);
		}
	}
	break;
	case EFFECTTREEMENU_ADDMAKEUPS://"Add Makeups"
	{
		bool bManualAdd = false;
		EDJSonParser* pFaceMorphParser = NULL;
		if (!pJsonParser)
		{//点击添加美妆
			pFaceMorphParser = m_JSonMainParser.CreateChildParser(EDFaceMorphParser::GetClassNameW()); DMASSERT(pFaceMorphParser);
			bManualAdd = true;
		}
		else
		{//json文件加载
			pFaceMorphParser = pJsonParser;
		}

		HDMTREEITEM hMakeupItem = m_pEffectTreeCtrl->InsertRootItem(L"Makeups", EFFECTTEMPBTN_MAKEUPS, DMTVI_LAST, false, false, hItem);
		if (!hMakeupItem)
			break;

 		BindRootWndToObjEditor(hMakeupItem, EFFECTTEMPBTN_MAKEUPS, pFaceMorphParser);
		AdjustTreeItemPos(hMakeupItem);//调整Item的顺序  Beautify和deformation必须在底层
		if (bManualAdd)
			m_pEffectTreeCtrl->SelectItem(hMakeupItem);
	}
	break;
	case EFFECTTREEMENU_ADDFACEEXCHANGE://"Add Face Exchange"
	{
		bool bManualAdd = false;
		EDJSonParser* pFaceExchangeParser = NULL;
		if (!pJsonParser)
		{//点击添加
			pFaceExchangeParser = m_JSonMainParser.CreateChildParser(EDFaceExchangeParser::GetClassNameW()); DMASSERT(pFaceExchangeParser);
			bManualAdd = true;
		}
		else
		{//json文件加载
			pFaceExchangeParser = pJsonParser;
		}
		HDMTREEITEM hChildItem = m_pEffectTreeCtrl->InsertChildItem(L"FaceExchange", hItem, DMTVI_LAST, false, false, L"ds_portraiteffect");
		m_pEffectTreeCtrl->SetItemEyeVisible(hChildItem, false);//不需要眼睛
		BindObjTreeData(NULL, NULL, hChildItem, EFFECTTEMPBTN_FACEEXCHANGE, pFaceExchangeParser);
		AdjustTreeItemPos(hChildItem);//调整Item的顺序  Beautify和deformation必须在底层
		if (bManualAdd)
			m_pEffectTreeCtrl->SelectItem(hChildItem);
	}
	break;
	case EFFECTTREEMENU_ADDBACKGROUNDEDGE://"Add Background Edge"
	{
		bool bManualAdd = false;
		EDJSonParser* pBackgroundEdgeParser = NULL;
		if (!pJsonParser)
		{//点击添加
			pBackgroundEdgeParser = m_JSonMainParser.CreateChildParser(EDBackgroundEdgeParser::GetClassNameW()); DMASSERT(pBackgroundEdgeParser);
			bManualAdd = true;
		}
		else
		{//json文件加载
			pBackgroundEdgeParser = pJsonParser;
		}
		HDMTREEITEM hChildItem = m_pEffectTreeCtrl->InsertChildItem(L"Background Edge", hItem, DMTVI_LAST, false, false, L"ds_facetraceico");
		BindObjTreeData(NULL, NULL, hChildItem, EFFECTTEMPBTN_BACKGROUNDEDGE, pBackgroundEdgeParser);
		AdjustTreeItemPos(hChildItem);
		if (bManualAdd)
			m_pEffectTreeCtrl->SelectItem(hChildItem);
	}
	break;
	case EFFECTTREEMENU_ADDFACEDEFORMATION://"Add Face Deformation"
	{
		::MessageBox(NULL, L"Add Face Deformation", L"sadfg", NULL);
	}
	break;
	case EFFECTTREEMENU_ADDBEAUTIFY://"Add Beautify"
	{
		bool bManualAdd = false;
		EDJSonParser* pBeautifyParser = NULL;
		if (!pJsonParser)
		{//点击添加
			EDJSonParser* pBeautifyPartParser = m_JSonMainParser.FindChildParserByClassName(EDBeautifyPartsParser::GetClassNameW());
			if (!pBeautifyPartParser)
			{
				pBeautifyPartParser = m_JSonMainParser.CreateChildParser(EDBeautifyPartsParser::GetClassNameW()); DMASSERT(pBeautifyPartParser);
			}
			pBeautifyParser = pBeautifyPartParser->CreateChildParser(EDBeautifyParser::GetClassNameW()); DMASSERT(pBeautifyParser);
			bManualAdd = true;
		}
		else
		{//json文件加载
			pBeautifyParser = pJsonParser;
		}
		HDMTREEITEM hChildItem = m_pEffectTreeCtrl->InsertChildItem(L"Beautify", hItem, DMTVI_LAST, false, false, L"ds_haircoloreffect");
		m_pEffectTreeCtrl->SetItemEyeVisible(hChildItem, false);//不需要眼睛
		m_pEffectTreeCtrl->SetFixnailiconVisible(hChildItem, true); //显示钉子图标
		BindObjTreeData(NULL, NULL, hChildItem, EFFECTTEMPBTN_BEAUTIFY, pBeautifyParser);
		if (bManualAdd)
			m_pEffectTreeCtrl->SelectItem(hChildItem);
	}
	break;
	case EFFECTTREEMENU_IMPORT3DSTICKER://"Import 3D Sticker"
	{
		::MessageBox(NULL, L"Import 3D Sticker", L"sadfg", NULL);
	}
	break;
	case EFFECTTREEMENU_ADDTRANSITION://"Add Transition" 
	{
		EDJSonParser* pTransitionNodeParser = NULL;
		if (!pJsonParser)
		{//点击添加
			pTransitionNodeParser = pData->m_pJsonParser->CreateChildParser(EDTransitionsNodeParser::GetClassNameW()); DMASSERT(pTransitionNodeParser);
			int iIndex = pData->m_pJsonParser->m_Node.m_nChildrenCount;
			CStringA strTransitionName;
			EDJSonParserPtr pParser = pData->m_pJsonParser->GetParser(GPS_FIRSTCHILD);
			while (pParser)
			{
				strTransitionName.Format("Transition%d", iIndex);
				if (strTransitionName == pTransitionNodeParser->m_strJSonMemberKey)
				{
					iIndex++;
					pParser = pData->m_pJsonParser->GetParser(GPS_FIRSTCHILD);
					continue;
				}
				pParser = pParser->GetParser(GPS_NEXTSIBLING);
			}
			pTransitionNodeParser->SetJSonMemberKey(strTransitionName);
			m_pTransitionTreeCtrl->OnCommand(0, TransitionMenu_AddTransition, 0, pTransitionNodeParser, true);
		}
		else
		{//json文件加载
			pTransitionNodeParser = pJsonParser;
		}
		HDMTREEITEM hChildItem = m_pEffectTreeCtrl->InsertChildItem(std::wstring(EDBASE::a2w(pTransitionNodeParser->m_strJSonMemberKey)).c_str(), hItem, DMTVI_LAST, false, false);
		m_pEffectTreeCtrl->SetItemEyeVisible(hChildItem, false);//不需要眼睛
		BindObjTreeData(NULL, NULL, hChildItem, EFFECTTEMPBTN_TRANSITIONSNODE, pTransitionNodeParser);
		if (!pJsonParser)//点击添加
			m_pEffectTreeCtrl->SelectItem(hChildItem);
	}
	break;
	case EFFECTTREEMENU_RENAME:
	{
		m_pEffectTreeCtrl->RenameTreeItem(hItem);
	}
	break;
	case EFFECTTREEMENU_UPITEM:
	{
		HandleEditorElementMenu(EDITORELEMENT_UPELEMENT, hItem);
	}
	break;
	case EFFECTTREEMENU_DOWNITEM:
	{
		HandleEditorElementMenu(EDITORELEMENT_DOWNELEMENT, hItem);
	}
	break;
	case EFFECTTREEMENU_DELETE:
	case EFFECTTREEMENU_DELEEFFECT:
	{
		if (hItem)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem);
			if (pData && pData->m_iEffectType == EFFECTTEMPBTN_TRANSITIONSNODE || 
				pData && pData->m_iEffectType == EFFECTTEMPBTN_FACEEXCHANGE ||
				pData && pData->m_iEffectType == EFFECTTEMPBTN_BACKGROUNDEDGE)
			{
				//pData->m_pJsonParser->DestoryParser();//在effecttree 的nodefree里面释放
			}
			else if (pData && pData->m_iEffectType == EFFECTTEMPBTN_BEAUTIFY)
			{
				g_pMainWnd->m_JSonMainParser.MarkDataDirty(true);
				pData->m_pJsonParser->GetParser(GPS_PARENT)->DestoryParser();//析构beautifyParts跟beautify
				pData->m_pJsonParser = NULL;
			}
			else
			{
				m_pObjEditor->UnlinkTreeChildNode(hItem);
			}
		}
		m_pEffectTreeCtrl->SelectItem(NULL);//为了触发selectchange
		m_pEffectTreeCtrl->RemoveItem(hItem);
	}
	break;
	case EFFECTTREEMENU_NOSE://鼻子 Nose
	case EFFECTTREEMENU_BLUSH:	//鼻子 Blush
	case EFFECTTREEMENU_EYESHADOW:	//眼影 EyeShadow
	case EFFECTTREEMENU_EYE:	//美瞳 Eye
	case EFFECTTREEMENU_LIPV2:	//唇彩 Lip
	case EFFECTTREEMENU_FACETRANSPLANT:// 换脸 face transplant
	{
		CStringW strMakeUpType; EffectTempletType MenuId;
		if (EFFECTTREEMENU_EYESHADOW == nID)
		{
			strMakeUpType = MAKEUPTAG_EYESHADOW;
			MenuId = EFFECTTEMPBTN_EYESHADOW;
		}
		else if (EFFECTTREEMENU_BLUSH == nID)
		{
			strMakeUpType = MAKEUPTAG_BLUSH;
			MenuId = EFFECTTEMPBTN_BLUSH;
		}
		else if (EFFECTTREEMENU_EYE == nID)
		{
			strMakeUpType = MAKEUPTAG_EYE;
			MenuId = EFFECTTEMPBTN_EYE;
		}
		else if (EFFECTTREEMENU_LIPV2 == nID)
		{
			strMakeUpType = MAKEUPTAG_LIP;
			MenuId = EFFECTTEMPBTN_LIPV2;
		}
		else if (EFFECTTREEMENU_NOSE == nID)
		{
			strMakeUpType = MAKEUPTAG_NOSE;
			MenuId = EFFECTTEMPBTN_NOSE;
		}
		else if (EFFECTTREEMENU_FACETRANSPLANT == nID)
		{
			strMakeUpType = MAKEUPTAG_NONE;
			MenuId = EFFECTTEMPBTN_FACETRANSPLANT;
		}
		else
			DMASSERT(FALSE);

		if (!pJsonParser)//没有传递parser进来 说明是通过点击按键进来的
		{
			EDJSonParserPtr pFaceMorphparser = m_JSonMainParser.FindChildParserByClassName(EDFaceMorphParser::GetClassNameW());
			if (!pFaceMorphparser)
			{
				HandleEffectTreeMenu(EFFECTTREEMENU_ADDMAKEUPS, m_hTreeAddPopupItem, NULL);
				pFaceMorphparser = m_JSonMainParser.FindChildParserByClassName(EDFaceMorphParser::GetClassNameW()); DMASSERT(pFaceMorphparser);
			}

			hItem = pFaceMorphparser->m_hTreeBindItem;
			EDJSonParserPtr pMakeupJsonParser = pFaceMorphparser->FindChildParserByClassName(EDMakeupsParser::GetClassNameW());
			if (!pMakeupJsonParser)
			{
				pMakeupJsonParser = pFaceMorphparser->CreateChildParser(EDMakeupsParser::GetClassNameW()); DMASSERT(pMakeupJsonParser);
			}
			EDJSonParser* pParser = pMakeupJsonParser->CreateChildParser(EDMakeupsNodeParser::GetClassNameW(), JSPARSER_FIRST);
			EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pParser); DMASSERT(pMakeupNodeParser);

			pMakeupNodeParser->m_strTag = strMakeUpType;
			ImportLocalMakeupImages(nID, strMakeUpType);

			EDResourceNodeParser* pBindResourceNodeParser = NULL;
			int iRet = Show_ResSelectDlg(TAGMAKEUPNODE, pMakeupNodeParser, m_hWnd);
			if (IDCONTINUE == iRet && OnImportImgCmdImp(NULL, pMakeupNodeParser) == DM_ECODE_OK) //点击了导入图片
			{
				pBindResourceNodeParser = (EDResourceNodeParser*)m_pMakeupResListCombobox->GetItemData(m_pMakeupResListCombobox->GetCount() - 1); DMASSERT(pBindResourceNodeParser);
				pMakeupNodeParser->RelativeResourceNodeParser((EDResourceNodeParser*)pBindResourceNodeParser);
			}
			else if (IDOK != iRet)
			{
				pMakeupNodeParser->DestoryParser();
				break;
			}
			AddEffectNodeItem(hItem, pMakeupNodeParser->m_strName, true, !m_pEffectTreeCtrl->IsItemEyeChkAndNotDisable(hItem), true, (EffectTempletType)MenuId, pMakeupNodeParser);
		}
		else
		{//json加载
			AddEffectNodeItem(hItem, dynamic_cast<EDMakeupsNodeParser*>(pJsonParser)->m_strName, pJsonParser->IsParserEnable(), !m_pEffectTreeCtrl->IsItemEyeChkAndNotDisable(hItem), false, (EffectTempletType)MenuId, pJsonParser);
		}
	}
	break;
	default:
		break;
	}
	
	return DM_ECODE_OK;
}

DMCode EDMainWnd::HandleEditorElementMenu(int nID, HDMTREEITEM hItem)
{
	if (!m_pEffectTreeCtrl || !hItem)
		return DM_ECODE_FAIL;

	switch (nID)
	{
	case EDITORELEMENT_UPELEMENT:
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (!pData)
			break;

		DUIWindowPtr pPreInsertWnd = NULL;
		HDMTREEITEM hPrevPrevItem = NULL;
		HDMTREEITEM hPrevItem = m_pEffectTreeCtrl->GetPrevSiblingItem(hItem); DMASSERT(hPrevItem);
		if (hPrevItem)
		{
			hPrevPrevItem = m_pEffectTreeCtrl->GetPrevSiblingItem(hPrevItem);
			ObjTreeDataPtr pPrevData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hPrevItem); DMASSERT(pPrevData);
			if (pPrevData && pData->m_pDUIWnd)
			{
				pPreInsertWnd = pPrevData->m_pDUIWnd;
				DUIWindow* pParent = pData->m_pDUIWnd->DM_GetWindow(GDW_PARENT); DMASSERT(pParent);
				if (pParent)//调整窗口顺序
				{
					if (0 == _wcsicmp(pData->m_pJsonParser->V_GetClassName(), pPrevData->m_pJsonParser->V_GetClassName())) //同类型的Item才发生调整 如makeup跟partnode 否则跳出
					{
						pParent->DM_RemoveChildWnd(pData->m_pDUIWnd);
						pParent->DM_InsertChild(pData->m_pDUIWnd, pPreInsertWnd);
						pParent->DM_Invalidate();
					}				
				}
			}			
		}

		//调整json数据顺序
		if (!hPrevItem)
		{
			DMASSERT(FALSE);//这里不会出现的
		}
		else
		{
			ObjTreeDataPtr pPrevData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hPrevItem); DMASSERT(pPrevData && pPrevData->m_pJsonParser);
			if (pPrevData && pPrevData->m_pJsonParser && pData)
			{
				if (_wcsicmp(pData->m_pJsonParser->V_GetClassName(), (LPCWSTR)pPrevData->m_pJsonParser->V_GetClassName()) == 0) //只能在同级项改变位置 partnode跟makeup是不能调整位置的
					pPrevData->m_pJsonParser->MoveParserItemToNewPos(pData->m_pJsonParser);//发现位置需相反 			
			}
		}
		//调整树Item顺序
		
		if (0 != _wcsicmp(EDFaceMorphParser::GetClassNameW(), pData->m_pJsonParser->V_GetClassName()))//非makeup    makeup带子节点   移动的时候会出错
			m_pEffectTreeCtrl->MoveItemToNewPos(hItem, m_pEffectTreeCtrl->GetParentItem(hItem) ? m_pEffectTreeCtrl->GetParentItem(hItem) : DMTVI_ROOT, hPrevPrevItem ? hPrevPrevItem : DMTVI_FIRST);
		else//makeup
		{
			m_pEffectTreeCtrl->MoveItemToNewPos(hPrevItem, m_pEffectTreeCtrl->GetParentItem(hItem) ? m_pEffectTreeCtrl->GetParentItem(hItem) : DMTVI_ROOT, hItem);
		}
	}
	break;
	case EDITORELEMENT_DOWNELEMENT:
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (!pData)
			break;

		HDMTREEITEM hNextSiblingItem = m_pEffectTreeCtrl->GetNextSiblingItem(hItem); DMASSERT(hNextSiblingItem);
		if (!hNextSiblingItem)
			break;
		
		ObjTreeDataPtr pNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextSiblingItem); DMASSERT(pNextSiblingData);
		if (!pNextSiblingData)
			break;

		//调整窗口位置
		do 
		{
			DUIWindowPtr pNextNextInsertWnd = NULL;
			HDMTREEITEM hNextNextSiblingItem = m_pEffectTreeCtrl->GetNextSiblingItem(hNextSiblingItem);
			if (hNextNextSiblingItem)
			{
				ObjTreeDataPtr pNextNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextNextSiblingItem); DMASSERT(pNextNextSiblingData);
				if (pNextNextSiblingData)
				{
					pNextNextInsertWnd = pNextNextSiblingData->m_pDUIWnd;
					if (0 != _wcsicmp(pNextNextSiblingData->m_pJsonParser->V_GetClassName(), pData->m_pJsonParser->V_GetClassName())) //同类型的Item才发生调整 如makeup跟partnode 否则跳出
						break;
				}
			}

			DUIWindow* pParent = pData->m_pDUIWnd->DM_GetWindow(GDW_PARENT); DMASSERT(pParent);
			if (pParent && pData->m_pDUIWnd)//调整窗口顺序
			{
				pParent->DM_RemoveChildWnd(pData->m_pDUIWnd);
				pParent->DM_InsertChild(pData->m_pDUIWnd, pNextNextInsertWnd ? pNextNextInsertWnd : DUIWND_FIRST);
				pParent->DM_Invalidate();
			}
		} while (false);		

		//调整json数据顺序
		if (!hNextSiblingItem)
		{
			DMASSERT(FALSE);//这里不会出现的
		}
		else
		{
			ObjTreeDataPtr pNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextSiblingItem); DMASSERT(pNextSiblingData && pNextSiblingData->m_pJsonParser);
			if (pNextSiblingData && pNextSiblingData->m_pJsonParser && pData && pData->m_pJsonParser)
			{
				if (_wcsicmp(pData->m_pJsonParser->V_GetClassName(), (LPCWSTR)pNextSiblingData->m_pJsonParser->V_GetClassName()) == 0) //只能在同级项改变位置 partnode跟makeup是不能调整位置的
					pData->m_pJsonParser->MoveParserItemToNewPos(pNextSiblingData->m_pJsonParser);
			}
		}
		//调整树Item顺序
		if (0 != _wcsicmp(EDFaceMorphParser::GetClassNameW(), pData->m_pJsonParser->V_GetClassName()))//非makeup  makeup带子节点   移动的时候会出错
			m_pEffectTreeCtrl->MoveItemToNewPos(hItem, m_pEffectTreeCtrl->GetParentItem(hItem) ? m_pEffectTreeCtrl->GetParentItem(hItem) : DMTVI_ROOT, hNextSiblingItem);
		else
			m_pEffectTreeCtrl->MoveItemToNewPos(hNextSiblingItem, m_pEffectTreeCtrl->GetParentItem(hItem) ? m_pEffectTreeCtrl->GetParentItem(hItem) : DMTVI_ROOT, m_pEffectTreeCtrl->GetPrevSiblingItem(hItem) ? m_pEffectTreeCtrl->GetPrevSiblingItem(hItem) : DMTVI_FIRST);
	}
	break;
	case EDITORELEMENT_SETUNVISIBLE:
	case EDITORELEMENT_SETVISIBLE :
	{
		m_pEffectTreeCtrl->TriggerClickItemEye(hItem);
	}
	break;
	case EDITORELEMENT_IMPORTIMG:
	{
		if (!m_pEffectTreeCtrl->GetSelectedItem())
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetSelectedItem());
		if (pData && EFFECTTEMPBTN_2DSTIKERNODE == pData->m_iEffectType)//2D贴纸节点
		{
			OnBtnImport2DImgCmdEvent(NULL);
		}
		else
		{
			OnBtnImportMakeupImgCmdEvent(NULL);
		}
	}
	break;
	case EDITORELEMENT_RESETPOSELEMENT:
	{

	}
	break;
	case EDITORELEMENT_DELEELEMENT:
	{
		HandleEffectTreeMenu(EFFECTTREEMENU_DELETE, hItem);
	}
	break;
	default:
		break;
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::ImportLocalMakeupImages(int nID, CStringW strMakeUpTag)
{
	if (EFFECTTREEMENU_EYESHADOW == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_EYESHADOW - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
		CArray<CStringW> ResFilesArrayB; ResFilesArrayB.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowB].text); ImportMakeupArrayImages(ResFilesArrayB, strMakeUpTag);
		CArray<CStringW> ResFilesArrayC; ResFilesArrayC.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowC].text); ImportMakeupArrayImages(ResFilesArrayC, strMakeUpTag);
		CArray<CStringW> ResFilesArrayD; ResFilesArrayD.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowD].text); ImportMakeupArrayImages(ResFilesArrayD, strMakeUpTag);
		CArray<CStringW> ResFilesArrayE; ResFilesArrayE.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowE].text); ImportMakeupArrayImages(ResFilesArrayE, strMakeUpTag);
		CArray<CStringW> ResFilesArrayF; ResFilesArrayF.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowF].text); ImportMakeupArrayImages(ResFilesArrayF, strMakeUpTag);
		CArray<CStringW> ResFilesArrayG; ResFilesArrayG.InsertAt(0, g_MakeupResItems[MakeupRes_EyeShadowG].text); ImportMakeupArrayImages(ResFilesArrayG, strMakeUpTag);
	}
	else if (EFFECTTREEMENU_BLUSH == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_BLUSH - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.Add(g_MakeupResItems[MakeupRes_BlushA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
		CArray<CStringW> ResFilesArrayB; ResFilesArrayB.Add(g_MakeupResItems[MakeupRes_BlushB].text); ImportMakeupArrayImages(ResFilesArrayB, strMakeUpTag);
	}
	else if (EFFECTTREEMENU_EYE == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_EYE - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.Add(g_MakeupResItems[MakeupRes_EyeA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
		CArray<CStringW> ResFilesArrayB; ResFilesArrayB.Add(g_MakeupResItems[MakeupRes_EyeB].text); ImportMakeupArrayImages(ResFilesArrayB, strMakeUpTag);
		CArray<CStringW> ResFilesArrayC; ResFilesArrayC.Add(g_MakeupResItems[MakeupRes_EyeC].text); ImportMakeupArrayImages(ResFilesArrayC, strMakeUpTag);
		CArray<CStringW> ResFilesArrayD; ResFilesArrayD.Add(g_MakeupResItems[MakeupRes_EyeD].text); ImportMakeupArrayImages(ResFilesArrayD, strMakeUpTag);
	}
	else if (EFFECTTREEMENU_LIPV2 == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_LIPV2 - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.Add(g_MakeupResItems[MakeupRes_LipA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
		CArray<CStringW> ResFilesArrayB; ResFilesArrayB.Add(g_MakeupResItems[MakeupRes_LipB].text); ImportMakeupArrayImages(ResFilesArrayB, strMakeUpTag);
	}
	else if (EFFECTTREEMENU_NOSE == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_NOSE - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.Add(g_MakeupResItems[MakeupRes_NoseA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
		CArray<CStringW> ResFilesArrayB; ResFilesArrayB.Add(g_MakeupResItems[MakeupRes_NoseB].text); ImportMakeupArrayImages(ResFilesArrayB, strMakeUpTag);
	}
	else if (EFFECTTREEMENU_FACETRANSPLANT == nID && !m_bProjectAddedMakeupRes[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE])
	{
		m_bProjectAddedMakeupRes[EFFECTTREEMENU_FACETRANSPLANT - EFFECTTREEMENU_BASE] = true;
		CArray<CStringW> ResFilesArrayA; ResFilesArrayA.Add(g_MakeupResItems[MakeupRes_FaceTransplantA].text); ImportMakeupArrayImages(ResFilesArrayA, strMakeUpTag);
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::HandleImpResLibMenu(int nID)
{
	DMCode iErr = DM_ECODE_FAIL;
	switch (nID)
	{
	case IMPRESLIBBTN_IMPIMGFRAME:
	{
		iErr = ImportImages(false);
	}
	break;
	case IMPRESLIBBTN_IMPGIFFILE:
	{
		ED_MessageBox(L"导入gif", MB_OK);
	}
	break;
	case IMPRESLIBBTN_IMPFROMFOLDER:
	{
		iErr = ImportImageRootFile();
	}
	break;
/*	case IMPRESLIBBTN_IMPMAKEUPIMG:
	{
		if (m_pMakeupResListCombobox)
		{
			int iIndex = m_pMakeupResListCombobox->GetCurSel() >= 0 ? m_pMakeupResListCombobox->GetCurSel() : 0;
			LPARAM lpData = m_pMakeupResListCombobox->GetItemData(iIndex);
			EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData; DMASSERT(pResourceParser);
			if (pResourceParser)
			{
				EDResourceNodeParser* pMakeupResourceNode = ImportMakeupImages(pResourceParser->m_strTag);
				if (pMakeupResourceNode)
				{
					OnReloadSucaikuListbox();
					ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
					EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pData->m_pJsonParser);
					if (pMakeupNodeParser)
					{
						pMakeupNodeParser->RelativeResourceNodeParser(pMakeupResourceNode);
						SucaiListInsertToComboItem(TAGMAKEUPNODE, pData->m_pJsonParser, m_pMakeupResListCombobox);
					}
					iErr = DM_ECODE_OK;
				}
			}
		}
	}
	break;*/
	default:
		DMASSERT(FALSE);
		break;
	}

	return iErr;
}

DMCode EDMainWnd::OnGlobalMenuBtn(int idFrom)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		DUIWindow* pCur = FindChildById(idFrom);
		if (NULL == pCur)
		{
			break;
		}

		DMXmlDocument Doc;
		g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_proj");
		DMXmlNode XmlNode = Doc.Root();
		XmlNode.SetAttributeInt(XML_BSHADOW, 1);
		InitFileMenu(XmlNode, idFrom);
		InitEditMenu(XmlNode, idFrom);
		InitLangMenu(XmlNode, idFrom);
		InitHelpMenu(XmlNode, idFrom);

		DUIMenu Menu;
		Menu.LoadMenu(XmlNode);
		CRect rcButton = pCur->m_rcWindow;
		ClientToScreen(rcButton);
		CPoint pt(rcButton.left, rcButton.bottom);
		Menu.TrackPopupMenu(0, pt.x, pt.y, m_hWnd);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::InitFileMenu(DMXmlNode& XmlNode, int idFrom)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (GLBMENUBTN_ID_FILE != idFrom)
		{
			break;
		}

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_NEW - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_NEW - GLBMENU_BASE].text);
		XmlItem.SetAttribute(XML_SKIN, L"ds_menubgexp");
		{
			DMXmlNode XmlItemNew = XmlItem.InsertChildNode(XML_ITEM);
			XmlItemNew.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_NEWPROJ - GLBMENU_BASE].id)); XmlItemNew.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_NEWPROJ - GLBMENU_BASE].text);
		
			XmlItemNew = XmlItem.InsertChildNode(XML_ITEM);
			XmlItemNew.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_NEWEMPTYPROJ - GLBMENU_BASE].id)); XmlItemNew.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_NEWEMPTYPROJ - GLBMENU_BASE].text);

			//	XmlItemNew.SetAttribute(L"height", L"33");
	/*		XmlItemNew = XmlItem.InsertChildNode(XML_ITEM);
			XmlItemNew.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_MODULEPROJ - GLBMENU_BASE].id)); XmlItemNew.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_MODULEPROJ - GLBMENU_BASE].text);
			XmlItemNew.SetAttribute(XML_SKIN, L"ds_menubgexp");
			{
				DMXmlNode XmlItemModule = XmlItemNew.InsertChildNode(XML_ITEM);
				XmlItemModule.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_NEWEMPTYPROJ - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_NEWEMPTYPROJ - GLBMENU_BASE].text);
			} */
		}
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_OPEN - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_OPEN - GLBMENU_BASE].text); XmlItem.SetAttributeInt(XML_BDISABLE, 0);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_CLOSE - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_CLOSE - GLBMENU_BASE].text); XmlItem.SetAttributeInt(XML_BDISABLE, 0);
		XmlItem.SetAttribute(XML_BDISABLE, m_strProjectName.IsEmpty() ? L"1" : L"0");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_OPENRECENT - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_OPENRECENT - GLBMENU_BASE].text);
		//XmlItem.SetAttribute(XML_SKIN, L"ds_menubgexp");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_SAVE - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_SAVE - GLBMENU_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, m_strProjectName.IsEmpty() ? L"1" : L"0");

		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_EXPORTZIP - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_EXPORTZIP - GLBMENU_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, m_strProjectName.IsEmpty() ? L"1" : L"0");
		
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_SAVEAS - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_SAVEAS - GLBMENU_BASE].text);
		XmlItem.SetAttribute(XML_BDISABLE, m_strProjectName.IsEmpty() ? L"1" : L"0");
		Init_Debug_XmlBuf(XmlNode);

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::InitEditMenu(DMXmlNode& XmlNode, int idFrom)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (GLBMENUBTN_ID_EDIT != idFrom)
		{
			break;
		}

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_BACKSTEP - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_BACKSTEP - GLBMENU_BASE].text); 
		XmlItem.SetAttributeInt(XML_BDISABLE, m_actionSlotMgr.IsExistPrevSiblingSteps() ? 0 : 1);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_FORWORDSTEP - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_FORWORDSTEP - GLBMENU_BASE].text); 
		XmlItem.SetAttributeInt(XML_BDISABLE, m_actionSlotMgr.IsExistNextSiblingSteps() ? 0 : 1);
	/*	XmlNode.InsertChildNode(XML_SEP);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_SELECTALL - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_SELECTALL - GLBMENU_BASE].text); XmlItem.SetAttributeInt(XML_BDISABLE, 0);
	*/	Init_Debug_XmlBuf(XmlNode);

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::InitLangMenu(DMXmlNode& XmlNode, int idFrom)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (GLBMENUBTN_ID_LANG != idFrom)
		{
			break;
		}

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_CHINESELAN - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_CHINESELAN - GLBMENU_BASE].text);
		XmlItem.SetAttribute(L"bcheck", L"1");
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_ENGLISHLAN - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_ENGLISHLAN - GLBMENU_BASE].text);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::InitHelpMenu(DMXmlNode& XmlNode, int idFrom)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (GLBMENUBTN_ID_HELP != idFrom)
		{
			break;
		}

		DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_ONLINEHELP - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_ONLINEHELP - GLBMENU_BASE].text);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_CHECKUPDATE - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_CHECKUPDATE - GLBMENU_BASE].text);
		XmlItem = XmlNode.InsertChildNode(XML_ITEM);
		XmlItem.SetAttribute(XML_ID, IntToString(g_GlbMenuItem[GLBMENU_FEEDBACK - GLBMENU_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_GlbMenuItem[GLBMENU_FEEDBACK - GLBMENU_BASE].text);
		Init_Debug_XmlBuf(XmlNode);

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::UpdateWndPngRes(DUIWindow* pWnd, const CStringW& strPngPath)
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (!pWnd)
			break;

		DWORD dwSize = GetFileSizeW(strPngPath);
		if (dwSize <= 0)
		{
			IDMSkin* pFindSkin = g_pDMApp->GetSkin(strPngPath);
			if (!pFindSkin)
			break;
			pWnd->SetAttribute(L"skin", strPngPath);
			iRet = DM_ECODE_OK;
			break;
		}

		DMBufT<byte> pBuf;
		pBuf.Allocate(dwSize);
		DWORD dwRead;
		GetFileBufW(strPngPath, (void**)&pBuf, dwSize, dwRead);

		CStringW strSkinName;
		static int index = 0;
		strSkinName.Format(L"uniquepngid%d", index++);
		CStringW strXml;
		strXml.Format(L"<imglist id=\"%s\" states=\"1\" />", (LPCWSTR)strSkinName);
		g_pDMApp->AddSkin(pBuf, dwSize, L"png", strXml);
		pWnd->SetAttribute(L"skin", strSkinName);
		iRet = DM_ECODE_OK;

// 		DMSmartPtrT<IDMSkin> pSkin;
// 		pWnd->m_pDUIXmlInfo->m_pStyle->GetBgSkin(&pSkin);
// 		DMSmartPtrT<IDMBitmap> pBitmap;
//		pSkin->GetBitmap(&pBitmap);
// 		if (pBitmap)
//		{
//			iRet = DM_ECODE_OK; //FreshPreviewBgFn(pBitmap->GetWidth(), pBitmap->GetHeight());
// 		}
		
	/*	HANDLE hFile = CreateFile((LPCTSTR)strPngPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			IDMSkin* pFindSkin = g_pDMApp->GetSkin(strPngPath);
			if (!pFindSkin)
				break;
			
			DMSmartPtrT<IDMBitmap> pBitmap;
			pFindSkin->GetBitmap(&pBitmap);
			if (!pBitmap)
				break;
			
			//pWnd->m_pDUIXmlInfo->m_pStyle->SetAttribute(L"skin", strPngPath, true);
			//pSkin->UpdateSkin((WPARAM)(LPCWSTR)strPngPath, 0);

			DMImgListSkinImpl* pImgSkin = dynamic_cast<DMImgListSkinImpl*>(pSkin.get()); //这些注释等同上面的代码
			if (!pImgSkin)
				break;
			pImgSkin->OnAttributeGetImage(strPngPath, false);
			iRet = FreshPreviewBgFn(pBitmap->GetWidth(), pBitmap->GetHeight());
		char *pBuf = (char*)malloc(file_size);
		DWORD dwRead;
		if (ReadFile(hFile, pBuf, file_size, &dwRead, NULL))
		{
			pSkin->SetBitmap((LPBYTE)pBuf, file_size, L"png");//png is not usege
			DMSmartPtrT<IDMBitmap> pBitmap;
			pSkin->GetBitmap(&pBitmap);
			if (pBitmap)
			{
				iRet = FreshPreviewBgFn(pBitmap->GetWidth(), pBitmap->GetHeight());
			}
		}
		if (hFile != INVALID_HANDLE_VALUE) CloseHandle(hFile);
		if (pBuf) free(pBuf);  */

	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnUpdateResPreviewWndPngPos(DUIWindow* pWnd)
{
	if (!pWnd)
		return DM_ECODE_FAIL;

	int iLeft = 3, iTop = 3, iRight = -3, iBottom = -3;
	DMSmartPtrT<IDMSkin> pSkin;
	pWnd->m_pDUIXmlInfo->m_pStyle->GetBgSkin(&pSkin);
	if (!pSkin)
		return DM_ECODE_FAIL;

	DMSmartPtrT<IDMBitmap> pBitmap;
	pSkin->GetBitmap(&pBitmap);
	if (!pBitmap)
		return DM_ECODE_FAIL;

	int iWidth = pBitmap->GetWidth();
	int iHeight = pBitmap->GetHeight();	

	DUIWindow* pParent = pWnd->DM_GetWindow(GDW_PARENT);
	if (pParent)
	{
		CRect rt;
		pParent->DV_GetClientRect(&rt);

		if (iWidth > iHeight)
		{
			if (iWidth >= rt.Width() - 6)
			{
				int iRealHeight = (rt.Height() - 6) * iHeight / iWidth;
				iTop = ((rt.Height() - 6) - iRealHeight) / 2 + 3;
				iBottom = -iTop;
			}
			else
			{
				iTop = (rt.Height() - iHeight) / 2;
				iBottom = (rt.Height() + iHeight) / 2;
				iLeft = (rt.Width() - iWidth) / 2;
				iRight = (rt.Width() + iWidth) / 2;
			}
		}
		else
		{
			if (iHeight >= rt.Height() - 6)
			{
				int iRealWidth = (rt.Width() - 6) * iWidth / iHeight;
				iLeft = ((rt.Width() - 6) - iRealWidth) / 2 + 3;
				iRight = -iLeft;
			}
			else
			{
				iTop = (rt.Height() - iHeight) / 2;
				iBottom = (rt.Height() + iHeight) / 2;
				iLeft = (rt.Width() - iWidth) / 2;
				iRight = (rt.Width() + iWidth) / 2;
			}
		}
	}

	CStringW strPos;
	strPos.Format(L"%d,%d,%d,%d", iLeft, iTop, iRight, iBottom);
	pWnd->SetAttribute(L"pos", strPos);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnUpdateEditorElemPngPos(DUIWindow* pWnd, double fWidthScale, double fHeightScale)
{
	if (!pWnd)
		return DM_ECODE_FAIL;

	DMSmartPtrT<IDMSkin> pSkin;
	pWnd->m_pDUIXmlInfo->m_pStyle->GetBgSkin(&pSkin);
	if (!pSkin)
		return DM_ECODE_FAIL;

	DMSmartPtrT<IDMBitmap> pBitmap;
	pSkin->GetBitmap(&pBitmap);
	if (!pBitmap)
		return DM_ECODE_FAIL;

	int iWidth = static_cast<int> (pBitmap->GetWidth() * fWidthScale); // 去掉warning
	int iHeight = static_cast<int>(pBitmap->GetHeight() * fHeightScale);

	CRect rect;
	pWnd->DV_GetClientRect(rect);
	rect.right = rect.left + iWidth;
	rect.bottom = rect.top + iHeight;
	rect.NormalizeRect();

	return m_pObjEditor->m_pDragFrame->SetElementRect(rect, false) ? DM_ECODE_OK : DM_ECODE_FAIL;
}

DMCode EDMainWnd::OnUpdate2dStickerPreviewPng(EDJSonParserPtr pJSonParser)
{
	DMCode iRet = DM_ECODE_FAIL;
	CStringW strPngPath;
	int iImageCnt = 0;
	do
	{
		if (!pJSonParser)
			break;

		EDResourceNodeParser* pResourceNodeParser = pJSonParser->GetRelativeResourceNodeParser();
		if (!pResourceNodeParser) //没关联资源
			break;

		iImageCnt = pResourceNodeParser->m_ArrImageList.GetCount();
		if (iImageCnt > 0)
			strPngPath = pResourceNodeParser->m_ArrImageList.GetAt(pResourceNodeParser->m_iCurResPreviewPngIndex < (int)pResourceNodeParser->m_ArrImageList.GetCount() ? pResourceNodeParser->m_iCurResPreviewPngIndex : 0);
		else
		{
			DMASSERT(FALSE);
			break;
		}
	} while (false);
	if (!IsFileExist(strPngPath))
		strPngPath = DEFAULTPREVIEWSKIN;

	if (m_p2DStickerNodeAttrList)
	{
		int count = (int)m_p2DStickerNodeAttrList->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			DUIButton* pPrevImgbtn = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_2dstickerprevrespreviewbtn");
			DUIButton* pNextImgbtn = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_2dstickernextrespreviewbtn");
			if (pPrevImgbtn)
			{
				pPrevImgbtn->SetAttribute(L"bvisible", iImageCnt > 1 ? L"1" : L"0");
			}
			if (pNextImgbtn)
			{
				pNextImgbtn->SetAttribute(L"bvisible", iImageCnt > 1 ? L"1" : L"0");
			}

			DUIWindow* pWnd = m_p2DStickerNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_2dstickerpreviewwnd");
			if (pWnd)
			{
				if (DM_ECODE_OK == UpdateWndPngRes(pWnd, strPngPath) && DM_ECODE_OK == OnUpdateResPreviewWndPngPos(pWnd))
				{
					iRet = DM_ECODE_OK;
				}
				else
				{
					pWnd->SetAttribute(L"skin", DEFAULTPREVIEWSKIN);
				}
				break;
			}
		}
	}	
	return iRet;
}

DMCode EDMainWnd::OnUpdateMakeupPreviewPng(EDJSonParserPtr pJSonParser)
{
	DMCode iRet = DM_ECODE_FAIL;
	CStringW strPngPath;
	int iImageCnt = 0;
	do
	{
		if (!pJSonParser)
			break;

		EDResourceNodeParser* pResourceNodeParser = pJSonParser->GetRelativeResourceNodeParser();
		if (!pResourceNodeParser) //没关联资源
			break;

		iImageCnt = pResourceNodeParser->m_ArrImageList.GetCount();
		if (iImageCnt > 0)
			strPngPath = pResourceNodeParser->m_ArrImageList.GetAt(pResourceNodeParser->m_iCurResPreviewPngIndex < (int)pResourceNodeParser->m_ArrImageList.GetCount() ? pResourceNodeParser->m_iCurResPreviewPngIndex : 0);
		else
		{
			DMASSERT(FALSE);
			break;
		}
	} while (false);

	if (!IsFileExist(strPngPath))
		strPngPath = DEFAULTPREVIEWSKIN;

	if (m_pMakeupNodeAttrList)
	{
		int count = (int)m_pMakeupNodeAttrList->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			DUIButton* pPrevImgbtn = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_makeupprevrespreviewbtn");
			DUIButton* pNextImgbtn = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIButton>(L"ds_makeupnextrespreviewbtn");
			if (pPrevImgbtn)
			{
				pPrevImgbtn->SetAttribute(L"bvisible", iImageCnt > 1 ? L"1" : L"0");
			}
			if (pNextImgbtn)
			{
				pNextImgbtn->SetAttribute(L"bvisible", iImageCnt > 1 ? L"1" : L"0");
			}

			DUIWindow* pWnd = m_pMakeupNodeAttrList->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_makeuppreviewwnd");
			if (pWnd)
			{
				if (DM_ECODE_OK == UpdateWndPngRes(pWnd, strPngPath) && DM_ECODE_OK == OnUpdateResPreviewWndPngPos(pWnd))
				{
					iRet = DM_ECODE_OK;
				}
				else
				{
					pWnd->SetAttribute(L"skin", DEFAULTPREVIEWSKIN);
				}
				break;
			}
		}
	}
	return iRet;
}

DMCode EDMainWnd::OnUpdateEditorElemPng(const CStringW& strPngPath)
{
	DMCode iRet = DM_ECODE_FAIL;
	ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);

	if (pData && pData->IsValid() && pData->m_pDUIWnd && pData->m_pJsonParser)
	{
		double wScale = 1.0, hScale = 1.0;
		wScale = hScale = pData->m_pJsonParser->GetParserImageScale();

		if (DM_ECODE_OK == UpdateWndPngRes(pData->m_pDUIWnd, strPngPath) && DM_ECODE_OK == OnUpdateEditorElemPngPos(pData->m_pDUIWnd, wScale, hScale))
		{
			iRet = DM_ECODE_OK;
		}
		else
		{
			pData->m_pDUIWnd->SetAttribute(L"skin", DEFAULTPREVIEWSKIN, false);
		}
	}

	return iRet;
}

DMCode EDMainWnd::OnUpdateEditorElemPng(ObjTreeDataPtr pData)
{
	DMCode iRet = DM_ECODE_FAIL;
	CStringW strPngPath = DEFAULTPREVIEWSKIN;
	do 
	{
		if (!pData || !pData->m_pJsonParser)
			break;
		EDResourceNodeParser * pResouceNodeParser = pData->m_pJsonParser->GetRelativeResourceNodeParser();
		if (!pResouceNodeParser)
			break;

		if (pResouceNodeParser->m_ArrImageList.GetCount() > 0)
			strPngPath = pResouceNodeParser->m_ArrImageList.GetAt(pResouceNodeParser->m_iCurResPreviewPngIndex < (int)pResouceNodeParser->m_ArrImageList.GetCount() ? pResouceNodeParser->m_iCurResPreviewPngIndex : 0);
		else
		{
			DMASSERT(FALSE);
			break;
		}
	} while (false);

	if (!IsFileExist(strPngPath))
	{
		DMASSERT(FALSE);
		strPngPath = DEFAULTPREVIEWSKIN;
	}

	if (pData && pData->IsValid() && pData->m_pDUIWnd)
	{
 		double wScale = 1.0, hScale = 1.0;
		if (pData->m_pJsonParser)
			wScale = hScale = pData->m_pJsonParser->GetParserImageScale();

		if (DM_ECODE_OK == UpdateWndPngRes(pData->m_pDUIWnd, strPngPath) && DM_ECODE_OK == OnUpdateEditorElemPngPos(pData->m_pDUIWnd, wScale, hScale))
		{
			iRet = DM_ECODE_OK;
		}
		else
		{
			DMASSERT(FALSE);
			pData->m_pDUIWnd->SetAttribute(L"skin", DEFAULTPREVIEWSKIN, false);
		}
	}
	return iRet;
}

void EDMainWnd::SetZiyuankuPngIndexEdtText(int iIndex)
{
	CStringW strPngIndex;
	strPngIndex.Format(L"%d", iIndex + 1);
	DUIWndAutoMuteGuard g(m_pZiyuankuPngIndexEdt);
	m_pZiyuankuPngIndexEdt->SetWindowTextW(strPngIndex);
	m_pZiyuankuPngIndexEdt->SetSel(MAKELONG(m_pZiyuankuPngIndexEdt->GetWindowTextLengthW(), m_pZiyuankuPngIndexEdt->GetWindowTextLengthW()));
}

DMCode EDMainWnd::OnUpdateReslibPreviewPng(const CStringW& strPngPath, DWORD dwframeCnt, int iPreviewIndex, bool bInUse, CStringW strTagInfo)
{
	DMCode iRet = DM_ECODE_FAIL;
	DUIList* plist = FindChildByNameT<DUIList>(L"ds_resattr_list");
	if (plist)
	{
		int count = (int)plist->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			DUIWindow* pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_reslibpreviewwnd");
			if (pWnd)
			{
				if (DM_ECODE_OK == UpdateWndPngRes(pWnd, strPngPath) && DM_ECODE_OK == OnUpdateResPreviewWndPngPos(pWnd))
				{
					iRet = DM_ECODE_OK;
				}
				else
				{
					pWnd->SetAttribute(L"skin", L"ds_deflibresskinpreviewbg", false);
				}

				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankuimgframecnt"); DMASSERT(pWnd);
				if (pWnd)
				{
					CStringW strText;
					strText.Format(L"%d", dwframeCnt);
					pWnd->DV_SetWindowText(strText);
				}
				
				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankuuseinfo"); DMASSERT(pWnd);
				if (pWnd)
				{
					pWnd->SetAttribute(L"text", bInUse ? L"占用" : L"空闲");
				}

				DUIComboBox* pCombobox = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIComboBox>(L"ds_ziyuankuimgtype"); DMASSERT(pCombobox);
				if (pCombobox)
				{
					DUIWndAutoMuteGuard guard(pCombobox->GetListBox());
					pCombobox->SetCurSel(strTagInfo == TAG2DSTICKER ? 0 : 1);
					pCombobox->SetAttribute(L"bdisable", bInUse ? L"1" : L"0");
					pCombobox->SetAttribute(L"clrbg", bInUse ? L"rgba(28, 2d, 32, FF)": L"rgba(40, 4b, 57, FF)"); //clrbgdisable 好像无效
					pCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangingArgs::EventID, Subscriber(&EDMainWnd::OnSucaikuResTagComboChangingEvent, this));
					pCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDMainWnd::OnSucaikuResTagComboChangeEvent, this));
				}

				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankudetailbtn"); DMASSERT(pWnd);
				if (pWnd)
				{
					pWnd->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnClikSucaikuDetailBtnEvent, this));
				}

				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankudetailedit"); DMASSERT(pWnd);
				if (pWnd)
				{
					pWnd->SetAttribute(L"bvisible", L"0");//隐藏edit 详情
				}

				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankuprevrespreviewbtn"); DMASSERT(pWnd);
				if (pWnd)
				{
					pWnd->SetAttribute(L"bvisible", dwframeCnt > 1 ? L"1" : L"0");
					pWnd->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuPrevResPreviewBtnCmdEvent, this));
				}

				pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIWindow>(L"ds_ziyuankunextrespreviewbtn"); DMASSERT(pWnd);
				if (pWnd)
				{
					pWnd->SetAttribute(L"bvisible", dwframeCnt > 1 ? L"1" : L"0");
					pWnd->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&EDMainWnd::OnZiyuankuNextResPreviewBtnCmdEvent, this));
				}

				m_pZiyuankuPngIndexEdt = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIRichEdit>(L"ds_ziyuankupngindexedit"); DMASSERT(m_pZiyuankuPngIndexEdt);
				if (m_pZiyuankuPngIndexEdt)
				{
					m_pZiyuankuPngIndexEdt->SetAttribute(L"bvisible", dwframeCnt > 1 ? L"1" : L"0");
					m_pZiyuankuPngIndexEdt->SetEventMask(ENM_CHANGE | m_pZiyuankuPngIndexEdt->GetEventMask());
					m_pZiyuankuPngIndexEdt->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, Subscriber(&EDMainWnd::OnZiyuanku_PngIndex_EditChanged, this));
					SetZiyuankuPngIndexEdtText(iPreviewIndex);
				}
				break;
			}
		}
	}
	return iRet;
}

DMCode EDMainWnd::OnZiyuanku_PngIndex_EditChanged(DMEventArgs* pEvt)
{
	int iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	if (EN_CHANGE == pEvent->m_iNotify)
	{
		do
		{
			if (!m_pZiyuankuPngIndexEdt)
				break;
			if (!m_pZiyuankuListboxEx)
				break;
			LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(m_pZiyuankuListboxEx->GetCurSel());
			if (!lpData)
			{
				DMASSERT(FALSE);
				break;
			}
			
			EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData;
			CStringW strText = m_pZiyuankuPngIndexEdt->GetWindowTextW();
			int iIndexPng = _wtoi((LPCWSTR)strText);
			if (iIndexPng <= 0 || iIndexPng > (int)pResourceParser->m_ArrImageList.GetCount())
			{
				CStringW strTmp;
				strTmp.Format(L"Valid Range is [1, %d]", pResourceParser->m_ArrImageList.GetCount());
				ED_MessageBox(strTmp, MB_OK, L"Error", m_hWnd);
				SetZiyuankuPngIndexEdtText(pResourceParser->m_iCurResPreviewPngIndex);
			}
			else
			{
				pResourceParser->SetCurResPreviewPngIndex(iIndexPng - 1);
				OnUpdateReslibPreviewPng(pResourceParser->m_ArrImageList[pResourceParser->m_iCurResPreviewPngIndex], pResourceParser->m_ArrImageList.GetCount(), pResourceParser->m_iCurResPreviewPngIndex, pResourceParser->m_pResourceParserOwner ? true : false, pResourceParser->m_strResTag);
				iRet = DM_ECODE_OK;
			}
		} while (false);
	}
	return iRet;
}

DMCode EDMainWnd::OnZiyuankuPrevResPreviewBtnCmdEvent(DMEventArgs* pEvent)
{
	int iRet = DM_ECODE_FAIL;
	EDResourceNodeParser* pResourceParser = NULL;
	do
	{
		if (!m_pZiyuankuListboxEx || !m_pEffectTreeCtrl)
			break;
		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
		{
			if (!m_pEffectTreeCtrl->GetSelectedItem())
				break;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetSelectedItem());
			if (pData && pData->m_pJsonParser)
				pResourceParser = pData->m_pJsonParser->GetRelativeResourceNodeParser();
		}
		else
		{
			LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
			if (!lpData)
			{
				DMASSERT(FALSE);
				break;
			}
			pResourceParser = (EDResourceNodeParser*)lpData;
		}	
		if (!pResourceParser)
			break;

		if (pResourceParser->m_ArrImageList.GetCount() > 1)
		{
			if (pResourceParser->m_iCurResPreviewPngIndex == 0)
			{
				pResourceParser->SetCurResPreviewPngIndex(pResourceParser->m_ArrImageList.GetCount() - 1);
			}
			else
			{
				pResourceParser->SetCurResPreviewPngIndex(pResourceParser->m_iCurResPreviewPngIndex - 1);
			}
			OnUpdateReslibPreviewPng(pResourceParser->m_ArrImageList[pResourceParser->m_iCurResPreviewPngIndex], pResourceParser->m_ArrImageList.GetCount(), pResourceParser->m_iCurResPreviewPngIndex, pResourceParser->m_pResourceParserOwner ? true : false, pResourceParser->m_strResTag);
			
			SetZiyuankuPngIndexEdtText(pResourceParser->m_iCurResPreviewPngIndex);
			iRet = DM_ECODE_OK;
		}
		else
			DMASSERT(FALSE);
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnZiyuankuNextResPreviewBtnCmdEvent(DMEventArgs* pEvent)
{
	int iRet = DM_ECODE_FAIL;
	EDResourceNodeParser* pResourceParser = NULL;
	do
	{
		if (!m_pZiyuankuListboxEx || !m_pEffectTreeCtrl)
			break;
		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
		{
			if (!m_pEffectTreeCtrl->GetSelectedItem())
				break;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_pEffectTreeCtrl->GetSelectedItem());
			if (pData && pData->m_pJsonParser)
				pResourceParser = pData->m_pJsonParser->GetRelativeResourceNodeParser();
		}
		else
		{
			LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
			if (!lpData)
			{
				DMASSERT(FALSE);
				break;
			}
			pResourceParser = (EDResourceNodeParser*)lpData;
		}
		if (!pResourceParser)
			break;

		if (pResourceParser->m_ArrImageList.GetCount() > 1)
		{
			if (pResourceParser->m_iCurResPreviewPngIndex >= (int)pResourceParser->m_ArrImageList.GetCount())
			{
				pResourceParser->SetCurResPreviewPngIndex(0);
			}
			else
				pResourceParser->SetCurResPreviewPngIndex(pResourceParser->m_iCurResPreviewPngIndex + 1);

			OnUpdateReslibPreviewPng(pResourceParser->m_ArrImageList[pResourceParser->m_iCurResPreviewPngIndex], pResourceParser->m_ArrImageList.GetCount(), pResourceParser->m_iCurResPreviewPngIndex, pResourceParser->m_pResourceParserOwner ? true : false, pResourceParser->m_strResTag);
			SetZiyuankuPngIndexEdtText(pResourceParser->m_iCurResPreviewPngIndex);
		}
		else
			DMASSERT(FALSE);
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnSucaikuResTagComboChangingEvent(DMEventArgs* pEvent)
{
	int iErr = DM_ECODE_FAIL;
	DMEventLBSelChangingArgs* pEvnt = (DMEventLBSelChangingArgs*)pEvent;	
	do
	{
		if (!m_pZiyuankuListboxEx || !pEvnt)
			break;

		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
			break;

		LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
		if (!lpData)
		{
			DMASSERT(FALSE);
			break;
		}
		EDResourceNodeParser* pResourceNodeParser = (EDResourceNodeParser*)lpData;
		CStringW strOutTip;
		if (!EDResourceParser::IsResourceNameCharacterValid(pResourceNodeParser->m_strName, pResourceNodeParser->m_strResTag == TAG2DSTICKER ? TAGMAKEUPNODE : TAG2DSTICKER, pResourceNodeParser->m_ArrImageList.GetCount() > 1, strOutTip))
		{
			ED_MessageBox(strOutTip, MB_OK, L"命名不兼容", m_hWnd);
			pEvnt->m_bCancel = true;
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::OnSucaikuResTagComboChangeEvent(DMEventArgs* pEvent)
{
	int iErr = DM_ECODE_FAIL;
	do
	{
		if (!m_pZiyuankuListboxEx)
			break;
		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
			break;		

		LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
		if (!lpData)
		{
			DMASSERT(FALSE);
			break;
		}

		EDResourceNodeParser* pResourceNodeParser = (EDResourceNodeParser*)lpData;
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		pResourceNodeParser->SetJSonAttributeString(EDAttr::EDResourceNodeParserAttr::STRING_tag, (pEvt->m_nNewSel == 0) ? TAG2DSTICKER : TAGMAKEUPNODE);
		OnReloadSucaikuListbox(pResourceNodeParser);
		m_pZiyuankuListboxEx->SetCurSel(iSeleItem);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::OnClikSucaikuDetailBtnEvent(DMEventArgs* pEvent)
{
	DMCode iErr = DM_ECODE_FAIL;
	DUIList* plist = FindChildByNameT<DUIList>(L"ds_resattr_list"); DMASSERT(plist);
	do
	{
		if (!m_pZiyuankuListboxEx || !plist)
			break;
		int iSeleItem = m_pZiyuankuListboxEx->GetCurSel();
		if (iSeleItem < 0)
			break;

		LPARAM lpData = m_pZiyuankuListboxEx->GetItemData(iSeleItem);
		if (!lpData)
		{
			DMASSERT(FALSE);
			break;
		}

		int count = (int)plist->m_ChildPanelArray.GetCount();
		for (int i = 0; i < count; i++)
		{
			DUIRichEdit* pWnd = plist->m_ChildPanelArray[i]->FindChildByNameT<DUIRichEdit>(L"ds_ziyuankudetailedit");
			if (pWnd)
			{
				EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData;
				pWnd->SetWindowTextW(pResourceParser->m_strImagePath);
				pWnd->SetAttribute(L"bvisible", L"1");//隐藏edit 详情
				iErr = DM_ECODE_OK;
				break;
			}			
		}
	}while (false);
	return iErr;
}

DMCode EDMainWnd::SucaiListInsertToComboItem(CStringW strTag, EDJSonParser* pJsonParserToCmp, DUIComboBox* pCombobox)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!pJsonParserToCmp || !pCombobox)
			break;

		pCombobox->ResetContent();
		pCombobox->GetListBox()->m_EventMgr.SetMuted(true);

		int iSeleItem = pCombobox->GetCurSel(); int iCurItem = 0;
		if (EDResourceParser::sta_pResourceParser)
		{
			EDJSonParser* pParser = EDResourceParser::sta_pResourceParser->GetParser(GPS_FIRSTCHILD);
			while (pParser)
			{
				EDResourceNodeParser* pResourceParser = dynamic_cast<EDResourceNodeParser*>(pParser);
				if (pResourceParser)
				{
					if (pResourceParser->m_strResTag == strTag && (!pResourceParser->m_pResourceParserOwner || pResourceParser == pJsonParserToCmp->GetRelativeResourceNodeParser()))//当前资源没有被占用   或者被当前节点占用的资源
					{
						pCombobox->InsertItem(-1, pResourceParser->m_strName, -1, -1, (LPARAM)pResourceParser);
						if (pResourceParser == pJsonParserToCmp->GetRelativeResourceNodeParser()/*pResourceParser->m_strImagePath == pMakeupNodeParser->m_strImagePath*/)
						{
							iSeleItem = iCurItem;
						}
						iCurItem++;
					}
				}
				pParser = pParser->GetParser(GPS_NEXTSIBLING);
			}
		}
		pCombobox->SetCurSel(iSeleItem);
		pCombobox->GetListBox()->m_EventMgr.SetMuted(false);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode EDMainWnd::OnUpdateMakeupTaglistItemCombo(EDMakeupsNodeParser* pMakeupNodeParser)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pMakeupNodeParser || !m_pMakeupTagListCombobox)
		{
			DMASSERT(pMakeupNodeParser);
			break;
		}
		DUIWndAutoMuteGuard guard(m_pMakeupTagListCombobox->GetListBox());	

		int iIndex = -1;
		if (pMakeupNodeParser->m_strTag == MAKEUPTAG_EYE)
			iIndex = 0;
		else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_EYESHADOW)
			iIndex = 1;
		else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_BLUSH)
			iIndex = 2;
		else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_NOSE)
			iIndex = 3;
		else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_LIP)
			iIndex = 4;
		else if (pMakeupNodeParser->m_strTag == MAKEUPTAG_NONE)
			iIndex = 5;
		DMASSERT(iIndex > -1);
		m_pMakeupTagListCombobox->SetCurSel(iIndex);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnReloadSucaikuListbox(EDResourceNodeParser* pResourceNodeParser /*= NULL*/)
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		//处理资源
		if (!m_pZiyuankuListboxEx)
			break;

		bool bResetAll = true;
		bool bUpdateOk = false;
		if (pResourceNodeParser)
			bResetAll = false;

		if (bResetAll)
		{
			m_pZiyuankuListboxEx->DeleteAllItems();
			if (EDResourceParser::sta_pResourceParser)
			{
				EDJSonParser* pParser = EDResourceParser::sta_pResourceParser->GetParser(GPS_FIRSTCHILD);
				while (pParser)
				{
					EDResourceNodeParser* pResourceParser = dynamic_cast<EDResourceNodeParser*>(pParser); DMASSERT(pResourceParser);
					if (pResourceParser)
					{
						m_pZiyuankuListboxEx->InsertNewItem(pResourceParser->m_strName, -1, pResourceParser->m_strResTag == TAG2DSTICKER, pResourceParser->m_pResourceParserOwner ? true : false, (LPARAM)pResourceParser);
					}
					pParser = pParser->GetParser(GPS_NEXTSIBLING);
				}
			}
		}
		else
		{
			for (int index = 0; index < m_pZiyuankuListboxEx->GetCount(); index++)
			{
				EDJSonParser* pParser = (EDJSonParser*)m_pZiyuankuListboxEx->GetItemData(index);
				if (pParser == pResourceNodeParser)
				{
					m_pZiyuankuListboxEx->UpdateItemInfo(pResourceNodeParser->m_strName, index, pResourceNodeParser->m_strResTag == TAG2DSTICKER, pResourceNodeParser->m_pResourceParserOwner ? true : false, (LPARAM)pResourceNodeParser);
					bUpdateOk = true;
					break;
				}
			}
		}

		if (!bResetAll && !bUpdateOk)
		{
			if (pResourceNodeParser)
			{
				m_pZiyuankuListboxEx->InsertNewItem(pResourceNodeParser->m_strName, m_pZiyuankuListboxEx->GetCount(), pResourceNodeParser->m_strResTag == TAG2DSTICKER, pResourceNodeParser->m_pResourceParserOwner ? true : false, (LPARAM)pResourceNodeParser);
			}
		}
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnRemoveSucaikuListboxItem(EDResourceNodeParser* pResourceNodeParser)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		//处理资源
		if (!m_pZiyuankuListboxEx || !pResourceNodeParser)
			break;
		for (int index = 0; index < m_pZiyuankuListboxEx->GetCount(); index++)
		{
			EDJSonParser* pParser = (EDJSonParser*)m_pZiyuankuListboxEx->GetItemData(index);
			if (pParser == pResourceNodeParser)
			{
				m_pZiyuankuListboxEx->DeleteItem(index);
				iRet = DM_ECODE_OK;
				break;
			}
		}		
	} while (false);
	return iRet;
}

DMCode EDMainWnd::On2DStickerRestComboLisChangeEvent(DMEventArgs* pEvent)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl || !m_pMakeupResListCombobox)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}

		EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pData->m_pJsonParser); DMASSERT(pPartsNodeParser);
		if (!pPartsNodeParser)
			break;

		LPARAM lpData = m_p2DStickerResListCombobox->GetItemData(m_p2DStickerResListCombobox->GetCurSel());
		EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData; DMASSERT(pResourceParser);
		if (!pResourceParser)
			break;

		pPartsNodeParser->RelativeResourceNodeParser(pResourceParser);
	} while (false);
	return iErr;

	/*DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (!m_p2DStickerResListCombobox)
			break;

		int iCurSele = m_p2DStickerResListCombobox->GetCurSel();
		std::wstring selItemText = L"";
		int width = 168;
		int height = 168;
		int frameCnt = 0;
		if (iCurSele == 0) //选择了“无”
		{
			iRet = OnUpdate2dStickerPreviewPng(DEFAULTPREVIEWSKIN);
			OnUpdateEditorElemPng(DEFAULTPREVIEWSKIN);
		}
		else
		{
			LPARAM lpData = m_p2DStickerResListCombobox->GetItemData(iCurSele);
			EDCore::BLResourceData* pResData = (EDCore::BLResourceData*)lpData;
			if (!pResData)
				break;
			if (pResData->type() != EDCore::BLResourceData::resource_2D)
				break;
			
			EDCore::Bl2DResourceData* p2DResData = dynamic_cast<EDCore::Bl2DResourceData*>(pResData);
			if (!p2DResData)
				break;
			std::wstring strPath = g_pCoreApp->activeDoc().tmpFilePath() + p2DResData->filePath();
			CStringW strName = m_p2DStickerResListCombobox->GetCBText();
			strPath += L"\\";
			strPath += (LPCWSTR)strName;
			strPath += L"_000." + p2DResData->suffix();
			iRet = OnUpdate2dStickerPreviewPng(strPath.c_str());
			selItemText = (LPCWSTR)strName;
			width = p2DResData->width();
			height = p2DResData->height();
			frameCnt = p2DResData->frameCnt();

			OnUpdateEditorElemPng(strPath.c_str());
		}

		auto partItem = getPartItem();
		if (partItem && partItem->resourceName() != selItemText)
		{
			EDCORE::BLDocument& doc = g_pCoreApp->activeDoc();
			doc.beginTrans();
			partItem->setResourceName(selItemText);
			partItem->setFrameCount(frameCnt);
			partItem->setWidth(width);
			partItem->setHeight(height);
			doc.endTrans();
		}
	} while (false);
	return iRet; */
}

DMCode EDMainWnd::OnBtnImport2DImgCmdEvent(DMEventArgs* pEvent)
{
	return OnImportImgCmdImp(m_p2DStickerResListCombobox, NULL);
}

DMCode EDMainWnd::OnBtnImportMakeupImgCmdEvent(DMEventArgs* pEvent)
{
	return OnImportImgCmdImp(m_pMakeupResListCombobox, NULL);
}

DMCode EDMainWnd::OnImportImgCmdImp(DUIComboBox* pListCombobox, EDJSonParserPtr pJsonParser)
{
	DMCode iErr = DM_ECODE_FAIL;
	if (DM_ECODE_OK == ImportImages(false, true, pJsonParser))
	{
		if (pListCombobox)
			pListCombobox->SetCurSel(pListCombobox->GetCount() - 1);
		iErr = DM_ECODE_OK;
	}
	return iErr;
}

DMCode EDMainWnd::OnTracePosListComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTraceposListCombobox)
			break;
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSele = pEvt->m_nNewSel;
		if (iSele < 0) iSele = 0;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}

		EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pData->m_pJsonParser); DMASSERT(pPartsNodeParser);
		if (!pPartsNodeParser)
			break;

		pPartsNodeParser->SetReferencePoint(g_tracePoints[iSele].x, g_tracePoints[iSele].y);
		m_pObjEditor->ReferenceLineFrameInSelMode();

		int iWidth = static_cast<int>(pPartsNodeParser->m_iWidth * pPartsNodeParser->GetParserImageScale()); // 去掉warning
		int iHeight = static_cast<int>(pPartsNodeParser->m_iHeight * pPartsNodeParser->GetParserImageScale());
		if (pData->m_pDUIWnd)
		{
			CRect rect;
			pData->m_pDUIWnd->DV_GetClientRect(rect);
			rect.right = rect.left + iWidth;
			rect.bottom = rect.top + iHeight;
			rect.NormalizeRect();
			m_pObjEditor->m_pDragFrame->SetElementRect(rect);
		}
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnStickerBlendModeComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pStickerBlendModeCombobox || !m_pEffectTreeCtrl)
		{
			DMASSERT(FALSE);
			break;
		}
			
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSeleItem = pEvt->m_nNewSel;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
			break;

		pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_blendMode, iSeleItem);

	} while (false);
	return iRet;
}

//TargetFPS
DMCode EDMainWnd::OnStickerTargetFPSComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pStickerTargetFPSCombobox || !m_pEffectTreeCtrl)
		{
			DMASSERT(FALSE);
			break;
		}
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
 		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
			break;
		
		DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
		if (!pListBox)
			break;

		CStringW iCurText;
		pListBox->GetText(iSelectItem, iCurText);
		int iFpsValue = std::stoi((LPCWSTR)iCurText);
		pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_targetFPS, iFpsValue);

		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//Sticker TargetFPS combobox edit 输入enter
DMCode EDMainWnd::OnStickerTargetFPSEditInputEnter(DMEventArgs* pEvent)
{
	if (m_pStickerTargetFPSCombobox)
	{
		CStringW strText = m_pStickerTargetFPSCombobox->m_pEdit->GetWindowTextW();

		bool bExist = false;
		for (int i = 0; i < m_pStickerTargetFPSCombobox->GetCount(); i++)
		{
			CStringW strTmp = m_pStickerTargetFPSCombobox->GetLBText(i);
			if (strTmp == strText)
			{
				bExist = true;
				break;
			}
		}
		if (!bExist)
			m_pStickerTargetFPSCombobox->InsertItem(-1, strText);
	}
	return DM_ECODE_OK;
}

//Sticker TargetFPS combobox edit 改变
DMCode EDMainWnd::OnStickerTargetFPSEditChange(DMEventArgs* pEvt)
{
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	if (EN_CHANGE == pEvent->m_iNotify)
	{
		if (m_pStickerTargetFPSCombobox)
		{
			CStringW strText = m_pStickerTargetFPSCombobox->m_pEdit->GetWindowTextW();
			int iTargetFPS = _wtoi((LPCWSTR)strText);
			if (iTargetFPS <= 0 || iTargetFPS > 60)
			{
				ED_MessageBox(L"The effective range is [1, 60]", MB_OK, L"Error", m_hWnd);
				CStringW strTmp;
				strTmp.Format(L"%d", m_iStickerTargetFPS);
				m_pStickerTargetFPSCombobox->m_pEdit->SetWindowTextW(strTmp);
			}
			else
			{
				m_iStickerTargetFPS = iTargetFPS;

				ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
				if (!pData)
				{
					DMASSERT(FALSE);
					return DM_ECODE_FAIL;
				}				
				pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_targetFPS, iTargetFPS);
			}
		}
	}
	return DM_ECODE_OK;
}

//GroupSize
DMCode EDMainWnd::OnStickerGroupSizeComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pStickerGroupSizeCombobox || !m_pStickerGroupIndexCombobox)
			break;
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSele = pEvt->m_nNewSel;
		if (iSele < 0) iSele = 0;

		int nGroupIndex = m_pStickerGroupIndexCombobox->GetCurSel();
		m_pStickerGroupIndexCombobox->ResetContent();
		for (int i = 0; i <= iSele; i++)
		{
			CStringW strIndex;
			strIndex.Format(L"%d", i);
			m_pStickerGroupIndexCombobox->InsertItem(-1, strIndex);
		}

		if (nGroupIndex >= m_pStickerGroupIndexCombobox->GetCount())
			m_pStickerGroupIndexCombobox->SetCurSel(m_pStickerGroupIndexCombobox->GetCount() - 1);
		else
			m_pStickerGroupIndexCombobox->SetCurSel(nGroupIndex);

		int newValue = iSele + 1; // 下标是从0开始的，下拉选项是从1开始，相差1。
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		pData->m_pJsonParser->SetJSonMemberAttributeInt(EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup, EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_groupSize, newValue);

		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
//GroupIndex
DMCode EDMainWnd::OnStickerGroupIndexComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pStickerGroupIndexCombobox)
			break;
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelect = pEvt->m_nNewSel;

		if (iSelect < 0) iSelect = 0;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		pData->m_pJsonParser->SetJSonMemberAttributeInt(EDAttr::EDPartsNodeParserAttr::COMPOSITE_stickerGroup, EDAttr::EDPartsNodeParserAttr::EDPartsNodeStickerGroup::INT_index, iSelect);

		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
//AllowDiscardFrame
DMCode EDMainWnd::OnStickerAllowDiscardFrameComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pStickerAllowDiscardFrameCombobox)
			break;
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelect = pEvt->m_nNewSel;
		if (iSelect < 0) iSelect = 0;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		pData->m_pJsonParser->SetJSonAttributeBool(EDAttr::EDPartsNodeParserAttr::BOOL_allowDiscardFrame, iSelect == 0);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnStickerPositionTypeComboChangeEvent(DMEventArgs* pEvent)
{
 	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelect = pEvt->m_nNewSel;
		if (iSelect < 0 || iSelect >= _countof(g_PositionTypes))
		{
			DMASSERT(FALSE);
			break;
		}

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		
		pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_positionType, g_PositionTypes[iSelect].typeId);
		pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_positionType2, g_PositionTypes[iSelect].type2Id);
		EDPartsNodeParser* pPartsNodePs = dynamic_cast<EDPartsNodeParser*>(pData->m_pJsonParser); DMASSERT(pPartsNodePs);
		if (iSelect < 3) // 0 1 2
		{
			m_pTraceposListCombobox->SetCurSel(0);
			pPartsNodePs->SetScalePoint(74, 77);
		}
		else if (iSelect < _countof(g_PositionTypes) - 2)
		{
			m_pTraceposListCombobox->SetCurSel(10);
			pPartsNodePs->SetScalePoint(110, 111);
		}
		else //foreground background
		{
			m_pTraceposListCombobox->SetCurSel(11);
			pPartsNodePs->SetScalePoint(106, 107);
			pPartsNodePs->SetRotateCenterPoint(106);
		}

		int iWidth  = static_cast<int>(pPartsNodePs->m_iWidth * pPartsNodePs->GetParserImageScale()); // 去掉warning
		int iHeight = static_cast<int>(pPartsNodePs->m_iHeight * pPartsNodePs->GetParserImageScale());
		if (pData->m_pDUIWnd)
		{
			CRect rect;
			pData->m_pDUIWnd->DV_GetClientRect(rect);
			rect.right = rect.left + iWidth;
			rect.bottom = rect.top + iHeight;
			rect.NormalizeRect();
			m_pObjEditor->m_pDragFrame->SetElementRect(rect);//更新坐标值到json
		}
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//StickerPostionRelationTye
DMCode EDMainWnd::OnStickerPositionRelationTypeComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelect = pEvt->m_nNewSel;
		if (iSelect < 0) iSelect = 0;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDPartsNodeParserAttr::INT_positionRelationType, iSelect == 0 ? 1 : 0);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
//HotLinkC
DMCode EDMainWnd::OnStickerHotLinkComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelect = pEvt->m_nNewSel;
		if (iSelect < 0) iSelect = 0;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData)
		{
			DMASSERT(FALSE);
			break;
		}
		pData->m_pJsonParser->SetJSonAttributeBool(EDAttr::EDPartsNodeParserAttr::BOOL_hotlinkEnable, iSelect == 0 ? true : false);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//触发类型combobox
DMCode EDMainWnd::OnTriggerAriseListComboChangeEvent(DMEventArgs* pEvent)
{
	// todo: remove
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTriggerAriseListCombobox)
			break;
		if (!m_pTriggerActListCombobox)
			break;
		if (!m_pTriggercicleListCombobox)
			break;

		DUIWindow* pTriggerActContanerWnd = m_pTriggerActListCombobox->DM_GetWindow(GDW_PARENT);
		DUIWindow* pTriggerCicleContanerWnd = m_pTriggercicleListCombobox->DM_GetWindow(GDW_PARENT);

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;

		switch (iSelectItem)
		{
			//无触发 下面只显示循环次数
		case NoneTrigger:
		{
			if (pTriggerActContanerWnd)
			{
				pTriggerActContanerWnd->DM_SetVisible(false);
			}
			if (pTriggerCicleContanerWnd)
			{
				pTriggerCicleContanerWnd->DM_SetVisible(true);
			}

			break;
		}
			//触发出现，下面显示触发动作和循环次数
		case TriggerAppear:
		{
			m_pTriggerActListCombobox->SetCurSel(0);
			if (pTriggerActContanerWnd)
			{
				pTriggerActContanerWnd->DM_SetVisible(true);
			}
			m_pTriggerActListCombobox->SetCurSel(0);
			if (pTriggerCicleContanerWnd)
			{
				pTriggerCicleContanerWnd->DM_SetVisible(true);
			}


			if (m_p2DStickerNodeAttrList) m_p2DStickerNodeAttrList->UpdateAllPanelLayout();
			break;
		}
			//触发出现和触发消失  下面只显示触发动作
		case TriggerAppearWithAct:
		case TriggerDisappear:
		{
			if (pTriggerActContanerWnd)
			{
				pTriggerActContanerWnd->DM_SetVisible(true);
			}
			if (pTriggerCicleContanerWnd)
			{
				pTriggerCicleContanerWnd->DM_SetVisible(false);
			}
			break;
		}
		default:
			break;
		}

		DUIWindow* pWnd = NULL;
		if (pWnd = pTriggerActContanerWnd ? pTriggerActContanerWnd : pTriggerCicleContanerWnd) //用键盘操作combobox 下面的布局不更新  好像没效果
		{
			DUIWindow* pOwner = pWnd->DM_GetWindow(GDW_PARENT)->DM_GetWindow(GDW_PARENT);
			pOwner->DM_Invalidate();
		}

		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//触发动作combobox
DMCode EDMainWnd::OnTriggerActListComboChangeEvent(DMEventArgs* pEvent)
{
	// todo: remove
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTriggerActListCombobox)
			break;

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;

	} while (false);
	return iRet;
}

//循环次数combobox
DMCode EDMainWnd::OnTriggerCicleListComboChangeEvent(DMEventArgs* pEvent)
{
	// todo: remove
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTriggercicleListCombobox)
			break;

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;

	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnAddFaceTraceComboListBtn(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (!m_pAddFaceTraceComboListBtn)
			break;

		if (!m_pFaceTraceListCombobox0 || !m_pFaceTraceListCombobox1 || !m_pFaceTraceListCombobox2 ||
			!m_pFaceTraceListCombobox3 || !m_pFaceTraceListCombobox4 || !m_pFaceTraceListCombobox5)
			break;

		if (!m_pFaceTraceListCombobox2->DM_GetWindow(GDW_PARENT)->DM_IsVisible())
			m_pFaceTraceListCombobox2->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
		else if (!m_pFaceTraceListCombobox3->DM_GetWindow(GDW_PARENT)->DM_IsVisible())
			m_pFaceTraceListCombobox3->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
		else if (!m_pFaceTraceListCombobox4->DM_GetWindow(GDW_PARENT)->DM_IsVisible())
			m_pFaceTraceListCombobox4->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
		else if (!m_pFaceTraceListCombobox5->DM_GetWindow(GDW_PARENT)->DM_IsVisible())
		{
			m_pFaceTraceListCombobox5->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
			m_pAddFaceTraceComboListBtn->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
			m_pAddFaceTraceComboListBtn->DM_EnableWindow(FALSE);
		}
	} while (false);

	m_pTransitionNodeAttrList->UpdateAllPanelLayout();//用键盘操作combobox 下面的布局不更新  强制刷新

	return iRet;
}

//人脸跟踪 跟踪选项
DMCode EDMainWnd::OnFaceTraceListCombo0ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pFaceTraceListCombobox0 || !m_pFaceTraceListCombobox1 || !m_pFaceTraceListCombobox2 ||
			!m_pFaceTraceListCombobox3 || !m_pFaceTraceListCombobox4 || !m_pFaceTraceListCombobox5 || !m_pAddFaceTraceComboListBtn)
			break;

		DUIWindow* pFaceTraceCombo1ContanerWnd = m_pFaceTraceListCombobox1->DM_GetWindow(GDW_PARENT);
		DUIWindow* pFaceTraceCombo2ContanerWnd = m_pFaceTraceListCombobox2->DM_GetWindow(GDW_PARENT);
		DUIWindow* pFaceTraceCombo3ContanerWnd = m_pFaceTraceListCombobox3->DM_GetWindow(GDW_PARENT);
		DUIWindow* pFaceTraceCombo4ContanerWnd = m_pFaceTraceListCombobox4->DM_GetWindow(GDW_PARENT);
		DUIWindow* pFaceTraceCombo5ContanerWnd = m_pFaceTraceListCombobox5->DM_GetWindow(GDW_PARENT);

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;
		
		switch (iSelectItem)
		{
			//人脸跟踪 所有人脸
		case FaceTraceAllFace:
		{
			if (pFaceTraceCombo1ContanerWnd)
				pFaceTraceCombo1ContanerWnd->DM_SetVisible(false);
			if (pFaceTraceCombo2ContanerWnd)
				pFaceTraceCombo2ContanerWnd->DM_SetVisible(false);
			if (pFaceTraceCombo3ContanerWnd)
				pFaceTraceCombo3ContanerWnd->DM_SetVisible(false);
			if (pFaceTraceCombo4ContanerWnd)
				pFaceTraceCombo4ContanerWnd->DM_SetVisible(false);
			if (pFaceTraceCombo5ContanerWnd)
				pFaceTraceCombo5ContanerWnd->DM_SetVisible(false);

			m_pAddFaceTraceComboListBtn->DM_GetWindow(GDW_PARENT)->DM_SetVisible(false);
			break;
		}
		//人脸跟踪 自定义
		case FaceTraceDefine:
		{
			if (pFaceTraceCombo1ContanerWnd)
			{
				pFaceTraceCombo1ContanerWnd->DM_SetVisible(true);
				pFaceTraceCombo1ContanerWnd->DM_GetWindow(GDW_PARENT)->DM_GetWindow(GDW_PARENT)->DV_UpdateChildLayout();
				m_pTransitionNodeAttrList->m_ChildPanelArray[0]->DV_UpdateChildLayout(); DM_Invalidate();
			}
			m_pAddFaceTraceComboListBtn->DM_EnableWindow(TRUE);
			m_pAddFaceTraceComboListBtn->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
			break;
		}
		default:
			break;
		}

		m_pTransitionNodeAttrList->UpdateAllPanelLayout();//用键盘操作combobox 下面的布局不更新  强制刷新

		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
//人脸跟踪 跟踪一
DMCode EDMainWnd::OnFaceTraceListCombo1ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	return iRet;
}
//人脸跟踪 跟踪二
DMCode EDMainWnd::OnFaceTraceListCombo2ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	return iRet;
}
//人脸跟踪 跟踪三
DMCode EDMainWnd::OnFaceTraceListCombo3ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	return iRet;
}
//人脸跟踪 跟踪四
DMCode EDMainWnd::OnFaceTraceListCombo4ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	return iRet;
}
//人脸跟踪 跟踪五
DMCode EDMainWnd::OnFaceTraceListCombo5ChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	return iRet;
}

DMCode EDMainWnd::OnParamInfoSetNodeAttrListExpandEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}
		DMEventTCExpandArgs* pExpandEvent = (DMEventTCExpandArgs*)pEvent;
		int iItem = (int)pExpandEvent->m_hItem;
		if (iItem<0 || iItem > 1)
			break;
		pData->m_bAttrlistExp[iItem] = static_cast<bool>(!!pExpandEvent->m_bCollapsed);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnMakeupResListComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl || !m_pMakeupResListCombobox)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}

		EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pData->m_pJsonParser); DMASSERT(pMakeupNodeParser);
		if (!pMakeupNodeParser)
 			break;
		
		LPARAM lpData = m_pMakeupResListCombobox->GetItemData(m_pMakeupResListCombobox->GetCurSel());
		EDResourceNodeParser* pResourceParser = (EDResourceNodeParser*)lpData; DMASSERT(pResourceParser);
		if (!pResourceParser)
			break;

		pMakeupNodeParser->RelativeResourceNodeParser(pResourceParser);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//美妆 Tag列表
DMCode EDMainWnd::OnMakeupTagListComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl || !m_pMakeupTagListCombobox)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}

		EDMakeupsNodeParser* pMakeupNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pData->m_pJsonParser); DMASSERT(pMakeupNodeParser);
		if (!pMakeupNodeParser)
			break;

		CStringW strMakeupTag = MAKEUPTAG_EYE;
		int iSelect = m_pMakeupTagListCombobox->GetCurSel();
		switch (iSelect)
		{
		case 0:
			strMakeupTag = MAKEUPTAG_EYE;
			break;
		case 1:
			strMakeupTag = MAKEUPTAG_EYESHADOW;
			break;
		case 2:
			strMakeupTag = MAKEUPTAG_BLUSH;
			break;
		case 3:
			strMakeupTag = MAKEUPTAG_NOSE;
			break;
		case 4:
			strMakeupTag = MAKEUPTAG_LIP;
			break;
		case 5:
			strMakeupTag = MAKEUPTAG_NONE;
			break;
		default:
			DMASSERT(FALSE);
			break;
		}
		pMakeupNodeParser->SetJSonAttributeString(EDAttr::EDMakeupsNodeParserAttr::STRING_tag, (LPCWSTR)strMakeupTag);

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;
		iRet = DM_ECODE_OK;

	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnMakeupTargetFPSComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSele = pEvt->m_nNewSel;
		if (iSele < 0) iSele = 0;

		DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
		if (!pListBox)
			break;

		CStringW iCurText;
		pListBox->GetText(iSele, iCurText);
		m_iMakeupTargetFPS = std::stoi((LPCWSTR)iCurText);

		if (!m_pEffectTreeCtrl)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}
		EDFaceMorphParser* pFaceMorphParser = dynamic_cast<EDFaceMorphParser*>(pData->m_pJsonParser); DMASSERT(pFaceMorphParser);
		if (!pFaceMorphParser)
		{
			DMASSERT(FALSE);
			break;
		}
		pFaceMorphParser->SetJSonAttributeInt(EDAttr::EDFaceMorphParserAttr::INT_targetFPS, m_iMakeupTargetFPS);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//美妆 FPS选择
DMCode EDMainWnd::OnMakeupTargetFPSEditInputEnter(DMEventArgs* pEvent)
{
	if (m_pMakeupTargetFPSCombobox)
	{
		CStringW strText = m_pMakeupTargetFPSCombobox->m_pEdit->GetWindowTextW();

		bool bExist = false;
		for (int i = 0; i < m_pMakeupTargetFPSCombobox->GetCount(); i++)
		{
			CStringW strTmp = m_pMakeupTargetFPSCombobox->GetLBText(i);
			if (strTmp == strText)
			{
				bExist = true;
				break;
			}
		}
		if (!bExist)
			m_pMakeupTargetFPSCombobox->InsertItem(-1, strText);
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnMakupTargetFPSEditChange(DMEventArgs* pEvt)
{
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	if (EN_CHANGE == pEvent->m_iNotify)
	{
		if (m_pMakeupTargetFPSCombobox)
		{
			CStringW strText = m_pMakeupTargetFPSCombobox->m_pEdit->GetWindowTextW();
			int iTargetFPS = _wtoi((LPCWSTR)strText);
			if (iTargetFPS <= 0 || iTargetFPS > 60)
			{
				ED_MessageBox(L"The effective range is [1, 60]", MB_OK, L"Error", m_hWnd);
				CStringW strTmp;
				strTmp.Format(L"%d", m_iMakeupTargetFPS);
				m_pMakeupTargetFPSCombobox->m_pEdit->SetWindowTextW(strTmp);
			}
			else
			{
				m_iMakeupTargetFPS = iTargetFPS;				
				do
				{
					if (!m_pEffectTreeCtrl)
						break;
					ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
					if (!pData || !pData->m_pJsonParser)
					{
						DMASSERT(FALSE);
						break;
					}

					pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDFaceMorphParserAttr::INT_targetFPS, m_iMakeupTargetFPS);
				} while (false);
			}
		}
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnMakeupAllowDiscardFrameComboChangeEvent(DMEventArgs* pEvent)
{
	DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
	int iSelectItem = pEvt->m_nNewSel;

	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}
		EDFaceMorphParser* pFaceMorphParser = dynamic_cast<EDFaceMorphParser*>(pData->m_pJsonParser); DMASSERT(pFaceMorphParser);
		if (!pFaceMorphParser)
		{
			DMASSERT(FALSE);
			break;
		}
		pFaceMorphParser->SetJSonAttributeBool(EDAttr::EDFaceMorphParserAttr::BOOL_allowDiscardFrame, iSelectItem == 0 ? true : false);

		DMEventLBSelChangedArgs* pEvt = (DMEventLBSelChangedArgs*)pEvent;
		int iSelectItem = pEvt->m_nNewSel;
		if (iSelectItem < 0) iSelectItem = 0;
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

//Background Edge Width选择
DMCode EDMainWnd::OnBackgroundEdgeWidthListComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl || !m_pBackgroundEdgeWidthListCombobox)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}

		EDBackgroundEdgeParser* pBakcgroundEdgeParser = dynamic_cast<EDBackgroundEdgeParser*>(pData->m_pJsonParser); DMASSERT(pBakcgroundEdgeParser);
		if (!pBakcgroundEdgeParser)
			break;

		int iSelect = m_pBackgroundEdgeWidthListCombobox->GetCurSel();
		switch (iSelect)
		{
		case 0:
			pBakcgroundEdgeParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_width, 2);
			break;
		case 1:
			pBakcgroundEdgeParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_width, 3);
			break;
		case 2:
			pBakcgroundEdgeParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_width, 5);
			break;
		case 3:
			pBakcgroundEdgeParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_width, 6);
			break;
		case 4:
			pBakcgroundEdgeParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_width, 8);
			break;
		}
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBackgroundEdgeColorSetCtrlEditChange(DMEventArgs* pEvt)
{
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	if (EN_CHANGE == pEvent->m_iNotify)
	{
		BYTE nField0, nField1, nField2, nField3;
		m_pBackgroundEdgeColorSetCtrl->GetAddress(nField0, nField1, nField2, nField3);
		m_iBackGroundEdgeColor.r = nField0; m_iBackGroundEdgeColor.g = nField1; m_iBackGroundEdgeColor.b = nField2; m_iBackGroundEdgeColor.a = nField3;

		CStringW strClr;
		strClr.Format(L"pbgra(%02x,%02x,%02x,%02x)", m_iBackGroundEdgeColor.b, m_iBackGroundEdgeColor.g, m_iBackGroundEdgeColor.r, m_iBackGroundEdgeColor.a);
		m_pBackgroundEdgeColorBtn->SetAttribute(L"clrbg", strClr);
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (pData && pData->m_pJsonParser)
		{
			DMColor clr;
			clr.a = m_iBackGroundEdgeColor.r;//商汤的保存的rgb有自己的顺序  需要做调整
			clr.r = m_iBackGroundEdgeColor.g;
			clr.g = m_iBackGroundEdgeColor.b;
			clr.b = m_iBackGroundEdgeColor.a;
			pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_color, clr.ToBGRA());
		}
	}
	return DM_ECODE_OK;
}

DMCode EDMainWnd::OnBackgroundEdgeColorBtnCmdEvent(DMEventArgs* pEvent)
{
	CHOOSECOLOR csc;                 // common dialog box structure 
	static COLORREF arrayClr[16];	 // array of custom colors 

	// Initialize CHOOSECOLOR 
	ZeroMemory(&csc, sizeof(csc));
	csc.lStructSize = sizeof(csc);
	csc.hwndOwner = m_hWnd;
	csc.lpCustColors = (LPDWORD)arrayClr;
	csc.rgbResult = m_iBackGroundEdgeColor.ToCOLORREF();
	csc.Flags = CC_ANYCOLOR/*CC_SOLIDCOLOR*/;

	if (ChooseColor(&csc))
	{
		m_iBackGroundEdgeColor.r = GetRValue(csc.rgbResult);
		m_iBackGroundEdgeColor.g = GetGValue(csc.rgbResult);
		m_iBackGroundEdgeColor.b = GetBValue(csc.rgbResult);
		CStringW strClr;
		strClr.Format(L"pbgra(%02x,%02x,%02x,%02x)", m_iBackGroundEdgeColor.b, m_iBackGroundEdgeColor.g, m_iBackGroundEdgeColor.r, m_iBackGroundEdgeColor.a);
		m_pBackgroundEdgeColorBtn->SetAttribute(L"clrbg", strClr);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (pData && pData->m_pJsonParser)
		{
			pData->m_pJsonParser->SetJSonAttributeInt(EDAttr::EDBackgroundEdgeParserAtrr::INT_color, m_iBackGroundEdgeColor.ToBGRA());
			m_pBackgroundEdgeColorSetCtrl->SetAddress(m_iBackGroundEdgeColor.r, m_iBackGroundEdgeColor.g, m_iBackGroundEdgeColor.b, m_iBackGroundEdgeColor.a);
		}		
	}
	return DM_ECODE_OK;
}

//FaceExchange MaxFaceSupported选择
DMCode EDMainWnd::OnMaxFaceSupportedListComboChangeEvent(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pEffectTreeCtrl || !m_pMaxFaceSuppportedListCombobox)
			break;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
		if (!pData || !pData->IsValid())
		{
			DMASSERT(FALSE);
			break;
		}
		EDFaceExchangeParser* pFaceExchangeParser = dynamic_cast<EDFaceExchangeParser*>(pData->m_pJsonParser); DMASSERT(pFaceExchangeParser);
		if (!pFaceExchangeParser)
			break;

		int iSelect = m_pMaxFaceSuppportedListCombobox->GetCurSel();
		pFaceExchangeParser->SetJSonAttributeInt(EDAttr::EDEDFaceExchangeParserAtrr::INT_maxFaceSupported, iSelect + 3);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_EditInputEnter(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;

		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;

		DUIComboBox* pComboBox = dynamic_cast<DUIComboBox*>(pEdit->DM_GetWindow(GDW_OWNER));
		if (!pComboBox)
			break;

		CStringW strText = pComboBox->m_pEdit->GetWindowTextW();
		bool bExist = false;
		for (int i = 0; i < pComboBox->GetCount(); i++)
		{
			CStringW strTmp = pComboBox->GetLBText(i);
			if (strTmp == strText)
			{
				bExist = true;
				break;
			}
		}
		if (!bExist)
			pComboBox->InsertItem(-1, strText);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_EnlargeEye_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;

		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbEnlargeRatio = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_enlargeRatio, m_dbEnlargeRatio);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_EnlargeEye_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fEnlargeEye = _wtof((LPCWSTR)strText);
		if (fEnlargeEye < 0.001 || fEnlargeEye > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbEnlargeRatio));
		}
		else
		{
			m_dbEnlargeRatio = fEnlargeEye;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_enlargeRatio, m_dbEnlargeRatio);
			iRet = DM_ECODE_OK;
		}		
	} while (false);
	return iRet;
}

//Beautify属性  ShrinkFace
DMCode EDMainWnd::OnBeautify_ShrinkFace_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbShrinkRatio = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_shrinkRatio, m_dbShrinkRatio);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_ShrinkFace_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fShrinkFace = _wtof((LPCWSTR)strText);
		if (fShrinkFace < 0.001 || fShrinkFace > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbShrinkRatio));
		}
		else
		{
			m_dbShrinkRatio = fShrinkFace;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_shrinkRatio, m_dbShrinkRatio);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}
//Beautify属性  SmallFace
DMCode EDMainWnd::OnBeautify_SmallFace_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbSmallRatio = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_smallRatio, m_dbSmallRatio);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_SmallFace_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fSmallFace = _wtof((LPCWSTR)strText);
		if (fSmallFace < 0.001 || fSmallFace > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbSmallRatio));
		}
		else
		{
			m_dbSmallRatio = fSmallFace;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_smallRatio, m_dbSmallRatio);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}
//Beautify属性  Redden
DMCode EDMainWnd::OnBeautify_Redden_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbReddenStrength = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_reddenStrength, m_dbReddenStrength);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
DMCode EDMainWnd::OnBeautify_Redden_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fRedden = _wtof((LPCWSTR)strText);
		if (fRedden < 0.001 || fRedden > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbReddenStrength));
		}
		else
		{
			m_dbReddenStrength = fRedden;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_reddenStrength, m_dbReddenStrength);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}

//Beautify属性  Whitten
DMCode EDMainWnd::OnBeautify_Whitten_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbWhitenStrength = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_whitenStrength, m_dbWhitenStrength);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_Whitten_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fWhitten = _wtof((LPCWSTR)strText);
		if (fWhitten < 0.001 || fWhitten > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbWhitenStrength));
		}
		else
		{
			m_dbWhitenStrength = fWhitten;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_whitenStrength, m_dbWhitenStrength);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}
//Beautify属性  Smooth
DMCode EDMainWnd::OnBeautify_Smooth_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbSmoothStrength = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_smoothStrength, m_dbSmoothStrength);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}
DMCode EDMainWnd::OnBeautify_Smooth_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fSmooth = _wtof((LPCWSTR)strText);
		if (fSmooth < 0.001 || fSmooth > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbSmoothStrength));
		}
		else
		{
			m_dbSmoothStrength = fSmooth;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_smoothStrength, m_dbSmoothStrength);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}

//Beautify属性  Contrast
DMCode EDMainWnd::OnBeautify_Contrast_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbContrastStrength = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_contrastStrength, m_dbContrastStrength);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_Contrast_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fConstrast = _wtof((LPCWSTR)strText);
		if (fConstrast < 0.001 || fConstrast > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbContrastStrength));
		}
		else
		{
			m_dbContrastStrength = fConstrast;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_contrastStrength, m_dbContrastStrength);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}

//Beautify属性  Saturation
DMCode EDMainWnd::OnBeautify_Saturation_ComboSelectChanged(DMEventArgs* pEvent)
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!pEvent)
			break;
		DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
		DUIComboBox* pCombobox = dynamic_cast<DUIComboBox*>(pEvt->m_pSender->DM_GetWindow(GDW_OWNER)); DMASSERT(pCombobox);
		if (!pCombobox)
			break;
		CStringW strText = pCombobox->GetLBText(pCombobox->GetCurSel());
		m_dbSaturation = _wtof((LPCWSTR)strText);

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
		if (!pData)
			break;
		EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
		if (!pBeautifyPs)
			break;
		pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_saturation, m_dbSaturation);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EDMainWnd::OnBeautify_Saturation_EditChanged(DMEventArgs* pEvt)
{
	DMCode iRet = DM_ECODE_FAIL;
	DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
	do
	{
		if (EN_CHANGE != pEvent->m_iNotify)
			break;
		if (!pEvent)
			break;
		DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
		if (!pEdit)
			break;
		CStringW strText = pEdit->GetWindowTextW();
		double fSaturation = _wtof((LPCWSTR)strText);
		if (fSaturation < 0.001 || fSaturation > 1.001)
		{
			ED_MessageBox(L"The effective range is [0.0, 1.0]", MB_OK, L"Error", m_hWnd);
			pEdit->SetWindowTextW(DoubleToString(m_dbSmoothStrength));
		}
		else
		{
			m_dbSaturation = fSaturation;
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel); DMASSERT(pData);
			if (!pData)
				break;
			EDBeautifyParser* pBeautifyPs = dynamic_cast<EDBeautifyParser*>(pData->m_pJsonParser);
			if (!pBeautifyPs)
				break;
			pBeautifyPs->SetJSonAttributeDouble(EDAttr::EDBeautifyParserAtrr::DOUBLE_saturation, m_dbSaturation);
			iRet = DM_ECODE_OK;
		}
	} while (false);
	return iRet;
}

DMCode EDMainWnd::ImportImages(bool bSelectFolder, bool bDisableChgTag /*= false*/, EDJSonParserPtr pRefJsonParser /*= NULL*/)
{
	CStringW strImagePath, strResName, strResTag = TAG2DSTICKER;
	BOOL bImportOk = false; bool bMultiFiles = false;

	if (bSelectFolder)
	{
		bImportOk = SelectFolderPathEx(strImagePath);
		int iSlash = strImagePath.ReverseFind(L'\\');
		if (-1 == iSlash)
		{
			iSlash = strImagePath.ReverseFind(L'/');
		}
		strResName = strImagePath.Right(strImagePath.GetLength() - iSlash - 1);
	}
	else
	{
		CStringW filePath;
		CArray<CStringW> FilesArray;
		if (DlgImportImages(filePath, FilesArray))
		{
			bMultiFiles = FilesArray.GetCount() > 1;
			bImportOk = TRUE;
			bool bFindNameOk = false;
			for (size_t i = 0; i < FilesArray.GetCount(); i++)
			{
				int index = -1; WCHAR szResName[50] = { 0 }; WCHAR szSuffix[20] = { 0 };
				if (!bFindNameOk && swscanf_s(FilesArray.GetAt(i), L"%[^_]_%03d.%s", szResName, countof(szResName), &index, szSuffix, countof(szSuffix)) && -1 != index)
				{
					strResName = szResName;
					bFindNameOk = true;
				}

				if (!strImagePath.IsEmpty())
					strImagePath += L";";
				strImagePath += filePath;
				strImagePath += L"\\";
				strImagePath += FilesArray.GetAt(i);

				if (!bFindNameOk && strResName.GetLength() < 20)
				{
					strResName += FilesArray.GetAt(i).Mid(0, FilesArray.GetAt(i).Find(L"."));
				}
			}
		}
	}

	if (bImportOk)
	{
		bool bSelect2DSticker = true; EDJSonParserPtr pJsonParser = NULL;
		if (pRefJsonParser)
		{			
			if (0 == _wcsicmp(pRefJsonParser->V_GetClassName(), EDMakeupsNodeParser::GetClassName()))
			{
				strResTag = TAGMAKEUPNODE;
				bSelect2DSticker = false;
			}
			else if (0 == _wcsicmp(pRefJsonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()))
			{
				strResTag = TAG2DSTICKER;
				bSelect2DSticker = true;
			}
			pJsonParser = pRefJsonParser;
		}
		else if (m_hObjSel)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
			if (pData && pData->m_pJsonParser)
			{
				EDResourceNodeParser* pResNode = pData->m_pJsonParser->GetRelativeResourceNodeParser();
				if (pResNode)
				{
					strResTag = pResNode->m_strResTag;
					bSelect2DSticker = pResNode->m_strResTag == TAG2DSTICKER ? true : false;
					pJsonParser = pData->m_pJsonParser;
				}
			}
		}

		if (IDOK != Show_ResEditDlg(strResTag, bMultiFiles, strResName, m_hWnd, strResTag, strResName, bDisableChgTag))
		{
			return DM_ECODE_FAIL;
		}

		EDResourceParser::GetResourceParserIns(&m_JSonMainParser)->InsertNewResourceNode(strImagePath, strResName, strResTag);
		SucaiListInsertToComboItem(strResTag, pJsonParser, bSelect2DSticker ? m_p2DStickerResListCombobox : m_pMakeupResListCombobox);
	}
	else
		return DM_ECODE_FAIL;
	return DM_ECODE_OK;
}

bool EDMainWnd::ImportImageFile(const std::wstring& strPath, const std::wstring& strItemName, bool isDir, CStringW strResTag)
{
	CStringW strImagePath, strResName;
	BOOL bImportOk = false; bool bMultiFiles = false;
	CStringW filePath = strPath.c_str();
	CArray<CStringW> FilesArray;
	if (!isDir)
	{
		FilesArray.Add(strItemName.c_str());
	}
	else if (ListDirectory(strPath, [&](const std::wstring& strPath, const std::wstring& strItemName, bool isDir)
	{
		if (isDir)
			return false;

		FilesArray.Add(strItemName.c_str());
		return true;
	}) != DM_ECODE_OK)
		return false;

	bMultiFiles = FilesArray.GetCount() > 1;
	bImportOk = TRUE;
	bool bFindNameOk = false;
	for (size_t i = 0; i < FilesArray.GetCount(); i++)
	{
		int index = 0; WCHAR szResName[50] = { 0 }; WCHAR szSuffix[20] = { 0 };
		if (!bFindNameOk && swscanf_s(FilesArray.GetAt(i), L"%[^_]_%03d.%s", szResName, countof(szResName), &index, szSuffix, countof(szSuffix)))
		{
			strResName = szResName;
			bFindNameOk = true;
		}

		if (!strImagePath.IsEmpty())
			strImagePath += L";";

		if (isDir)
		{
			strImagePath += filePath;
			strImagePath += L"\\";
			strImagePath += FilesArray.GetAt(i);
		}
		else
		{
			strImagePath = filePath;
		}

		if (!bFindNameOk && strResName.GetLength() < 20)
		{
			strResName += FilesArray.GetAt(i).Mid(0, FilesArray.GetAt(i).Find(L"."));
		}
	}

	if (bImportOk)
	{
		bool bSelect2DSticker = true;
		EDJSonParserPtr pJsonParser = NULL;
		if (m_hObjSel)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(m_hObjSel);
			if (pData && pData->m_pJsonParser)
			{
				EDResourceNodeParser* pResNode = pData->m_pJsonParser->GetRelativeResourceNodeParser();
				if (pResNode)
				{
					strResTag = pResNode->m_strResTag;
					bSelect2DSticker = pResNode->m_strResTag == TAG2DSTICKER ? true : false;
					pJsonParser = pData->m_pJsonParser;
				}
			}
		}

		EDResourceParser::GetResourceParserIns(&m_JSonMainParser)->InsertNewResourceNode(strImagePath, strResName, strResTag);
		SucaiListInsertToComboItem(strResTag, pJsonParser, bSelect2DSticker ? m_p2DStickerResListCombobox : m_pMakeupResListCombobox);
	}

	return true;
}

DM::DMCode EDMainWnd::ImportImageRootFile()
{
	CStringW strPath;
	SelectFolderPathEx(strPath);
	if (strPath.IsEmpty())
		return DM_ECODE_FAIL;

	std::wstring stdStrPath = strPath;
	return ListDirectory(stdStrPath, [&](const std::wstring& strPath, const std::wstring& strItemName, bool isDir)
	{
		if (isDir == false)
			return true;

		if (strItemName == FACEMORPH)
		{
			auto res = ListDirectory(strPath, [&](const std::wstring& strPath, const std::wstring& strItemName, bool isDir) 
			{
				return ImportImageFile(strPath, strItemName, isDir, TAGMAKEUPNODE);
			});

			return res == DM_ECODE_OK;
		}
		else
		{
			return ImportImageFile(strPath, strItemName, isDir, TAG2DSTICKER);
		}
	});
}

EDResourceNodeParser* EDMainWnd::ImportMakeupArrayImages(const CArray<CStringW>& ResFilesArray, CStringW strMakeUpTag)
{
	EDResourceNodeParser* pResourceNodeParser = NULL;
	CStringW strImagePathComb, strResName; 
	for (size_t i = 0; i < ResFilesArray.GetCount(); i++)
	{
		if (!strImagePathComb.IsEmpty())
			strImagePathComb += L";";
		strImagePathComb += GetProgramDirectory() + CStringW(MAKEUPDIR) + ResFilesArray.GetAt(i);		
		if (!strResName.IsEmpty())
			strResName += L"&";
		strResName += ResFilesArray.GetAt(i).Mid(0, ResFilesArray.GetAt(i).Find(L"."));
	}

	EDResourceParser* pResourceParser = EDResourceParser::GetResourceParserIns(&m_JSonMainParser); DMASSERT(pResourceParser);
	if (pResourceParser)
	{
		pResourceNodeParser = pResourceParser->InsertNewResourceNode(strImagePathComb, strResName, TAGMAKEUPNODE/*strMakeUpTag*/);
	}
	return pResourceNodeParser;
}

void EDMainWnd::HandleProjectNew(CStringW strProjectName)
{
/*	if (m_JSonMainParser.RecursiveCheckIsDataDirty())
	{
		int result = ED_MessageBox(L"工程已被修改，是否保存当前工程?", MB_YESNOCANCEL, L"提示");
		if (result == IDYES)
		{
			if (!HandleProjectSave())
				return;
		}
		else if (result == IDCANCEL)
		{
			return;
		}
	}
	UnInstallProject(); */
	if (DM_ECODE_OK != HandleProjectClose())
		return;
	
	m_strProjectName = strProjectName;
	DUIButton* pImportResButton = FindChildByNameT<DUIButton>(L"ImportResButton");  DMASSERT(pImportResButton);
	if (pImportResButton) pImportResButton->SetAttribute(L"bdisable", L"0");
	HandlAddEffectMenu(EFFECTTEMPBTN_STICKERSMAJOR);
	HandlAddEffectMenu(EFFECTTEMPBTN_TRANSITIONS);
}

void EDMainWnd::SetMainWndTitle(bool bDataDirty)
{
	SetMainWndTitleText(DEFAULTWNDTITLE + (m_strProjectName.IsEmpty() ? CStringW(L"") : CStringW(L" -  ")) + m_strProjectName + (bDataDirty ? CStringW(L"*") : CStringW(L"")));
}

void EDMainWnd::HandleProjectOpen(CStringW strProjectPath /*= L""*/)
{
	if (DM_ECODE_OK != HandleProjectClose())
		return;

	if (strProjectPath.IsEmpty() && !SelectFolderPathEx(strProjectPath))
	{
		return;
	}

	if (strProjectPath.Right(1) == L'\\' || strProjectPath.Right(1) == L'/')
	{
		strProjectPath = strProjectPath.Left(strProjectPath.GetLength() - 1);
	}
	int iSlash = strProjectPath.ReverseFind(L'\\');
	if (-1 == iSlash)
	{
		iSlash = strProjectPath.ReverseFind(L'/');
	}
	DMASSERT(iSlash > 0);
	CStringW strFolderName = strProjectPath.Right(strProjectPath.GetLength() - iSlash - 1);
	CStringW strProjectFullPath = strProjectPath + CStringW(L"\\") + strFolderName + CStringW(L".json");
	if (!PathFileExistsW((LPCWSTR)strProjectFullPath))
	{
		ED_MessageBox(L"无效路径，没有发现与文件夹同名的json文件。", MB_OK, L"打开工程失败", m_hWnd);
		return;
	}

	m_strProjectName = m_strJSonFilePath = strProjectPath;
	m_strJSonFileJSonPath = strProjectFullPath;
	DMCode hRet = m_JSonMainParser.LoadJSonFile(m_strJSonFileJSonPath); DMASSERT(DM_ECODE_OK == hRet);

	DUIButton* pImportResButton = FindChildByNameT<DUIButton>(L"ImportResButton");  DMASSERT(pImportResButton);
	if (pImportResButton) pImportResButton->SetAttribute(L"bdisable", L"0");
	m_JSonMainParser.MarkDataDirtyRecursive(false);
	m_bJsonDataDirtyMark = false;
	SetMainWndTitle(m_bJsonDataDirtyMark);

	if (m_pSDKPreviewWnd)
	{
		m_pSDKPreviewWnd->setResourcePath((LPCWSTR)strProjectPath);
	}
	g_pCtrlXml->AddRecentResDir(m_strProjectName);
	return;
}

DMCode EDMainWnd::HandleProjectClose()
{
	if (m_JSonMainParser.RecursiveCheckIsDataDirty())
	{
		int result = ED_MessageBox(L"工程已被修改，是否保存当前工程?", MB_YESNOCANCEL, L"提示");
		if (result == IDYES)
		{
			if (!HandleProjectSave())
				return DM_ECODE_FAIL;
		}
		else if (result == IDCANCEL)
		{
			return DM_ECODE_FAIL;
		}
	}
	UnInstallProject();
	return DM_ECODE_OK;
}

bool EDMainWnd::HandleProjectSave()
{
	if (m_strJSonFilePath.IsEmpty()) //没有保存过
	{
		return HandleFileSaveAs();
	}
	else
	{
		CopyFile(m_strJSonFileJSonPath, CStringW(m_strJSonFileJSonPath.Left(m_strJSonFileJSonPath.GetLength() - 5) + CStringW(L"JSON.jsbak")), TRUE);
		return HandleFileSaveAs(false);
	}
	return false;
}

DMCode EDMainWnd::ReOrderParserZPositonByTreeOrder(int& iZPostion, HDMTREEITEM hItem /*= DMTVI_ROOT*/)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (!m_pEffectTreeCtrl)
			break;

		HDMTREEITEM hSiblingItem = m_pEffectTreeCtrl->GetChildItem(hItem, false);
		while (hSiblingItem)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hSiblingItem); DMASSERT(pData && pData->m_pJsonParser);
			if (pData && pData->m_pJsonParser)
			{
				if (pData->m_pJsonParser->SetZPositionOrder(iZPostion))
					iZPostion++;
			}
			ReOrderParserZPositonByTreeOrder(iZPostion, hSiblingItem);//递归
			hSiblingItem = m_pEffectTreeCtrl->GetPrevSiblingItem(hSiblingItem);
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

bool EDMainWnd::HandleFileSaveAs(bool bPopSelectFolder /*= true*/, bool bPopTransitionsReminder /*= true*/)
{
	if (!m_JSonMainParser.FindChildParserByClassName(EDTransitionsNodeParser::GetClassNameW()) && bPopTransitionsReminder)
	{
		if (IDYES!=ED_MessageBox(L"请设置transition，如果不设置，贴纸将无法正常显示，是否继续？", MB_YESNOCANCEL, L"提示", m_hWnd))
			return false;
	}

	if (bPopSelectFolder)
	{
		CStringW strProjectPath;
		if (!SelectFolderPathEx(strProjectPath))
		{
			return false;
		}
		int iSlash = strProjectPath.ReverseFind(L'\\');
		if (-1 == iSlash)
		{
			iSlash = strProjectPath.ReverseFind(L'/');
		}
		DMASSERT(iSlash > 0);
		CStringW strFolderName = strProjectPath.Right(strProjectPath.GetLength() - iSlash - 1);
		m_strJSonFileJSonPath = strProjectPath + CStringW(L"\\") + strFolderName + CStringW(L".json");
		m_strProjectName = m_strJSonFilePath = strProjectPath;
	}

	int iZPositionStart = 0;
	ReOrderParserZPositonByTreeOrder(iZPositionStart);
	m_JSonMainParser.BuildJSonFile(m_strJSonFileJSonPath);
	m_JSonMainParser.MarkDataDirtyRecursive(false);
	m_bJsonDataDirtyMark = false;
	SetMainWndTitle(m_bJsonDataDirtyMark);
	if (m_pSDKPreviewWnd)
	{
		m_pSDKPreviewWnd->setResourcePath((LPCWSTR)m_strJSonFileJSonPath);
	}

	return true;
}

DMCode EDMainWnd::UnInstallProject()
{
	m_strProjectName.Empty();
	m_strJSonFileJSonPath.Empty();
	m_strJSonFilePath.Empty();
	m_bJsonDataDirtyMark = false;
	m_iTransitionIndex = 1;
	
	m_pObjEditor->UnInitObjEditor();
	g_pMainWnd->m_hObjSel = NULL;
	m_actionSlotMgr.FreeAllActionSlot();
	DMASSERT(m_pEffectTreeCtrl);
	if (m_pEffectTreeCtrl){
		m_pEffectTreeCtrl->RemoveAllItems();
	}		
	if (m_pTransitionTreeCtrl){
		m_pTransitionTreeCtrl->RemoveAllItems();
	}
	if (m_pZiyuankuListboxEx) m_pZiyuankuListboxEx->DeleteAllItems();

	DUIButton* pImportResButton = FindChildByNameT<DUIButton>(L"ImportResButton");  DMASSERT(pImportResButton);
	if (pImportResButton) pImportResButton->SetAttribute(L"bdisable", L"1");

	m_JSonMainParser.DestoryParser(); //必须放在m_pEffectTreeCtrl->RemoveAllItems()后面  树控件卸载的时候会delete jsonPharser   不能提前delete掉 不然会重复delete
	memset(m_bProjectAddedMakeupRes, 0, sizeof(m_bProjectAddedMakeupRes));
	SetParameInfoTabCurSel(PARAMEINFOTAB_NULL);
	m_JSonMainParser.MarkDataDirtyRecursive(false);
	m_bJsonDataDirtyMark = false;
	SetMainWndTitle(false);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::BindRootWndToObjEditor(HDMTREEITEM hItem, EffectTempletType id, EDJSonParserPtr pJSonParser)
{
	if (!hItem)
		return DM_ECODE_FAIL;
	DUIRoot* pRootWnd = m_pObjEditor->InitDesignChild(hItem); DMASSERT(pRootWnd);
	if (pRootWnd)
	{
		m_pObjEditor->ShowDesignChild(pRootWnd);
	}

	BindObjTreeData(pRootWnd, pRootWnd, hItem, id, pJSonParser);
	return DM_ECODE_OK;
}

DMCode EDMainWnd::BindObjTreeData(DUIRoot* pEditor, DUIWindowPtr pDUI, HDMTREEITEM hTreeItem, EffectTempletType iEfType, EDJSonParserPtr pJSonParser /*= NULL*/)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (NULL == hTreeItem || DMTVI_ROOT == hTreeItem || NULL == m_pEffectTreeCtrl)
		{
			break;
		}
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hTreeItem);
		if (pData)// 更新数据
		{
			pData->SetData(pEditor, pDUI, iEfType, hTreeItem, pJSonParser);
		}
		else// 新增绑定
		{
			pData = new ObjTreeData(pEditor, pDUI, iEfType, hTreeItem, pJSonParser);
			m_pEffectTreeCtrl->SetItemData(hTreeItem, (LPARAM)pData);
		}
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

HDMTREEITEM EDMainWnd::AddEffectNodeItem(HDMTREEITEM hParent, LPCWSTR lpName, bool bEyeCheck, bool bDisableEyeCheck, bool bSelectItem, EffectTempletType type, EDJSonParserPtr pJSonParser)
{
	DMASSERT(hParent);
	if (!hParent)
		return NULL;
	
	if (!bEyeCheck && pJSonParser)
		pJSonParser->SetJSonAttributeBool(EDAttr::EDPartsNodeParserAttr::BOOL_enable, false);

	ObjTreeDataPtr pParentData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hParent); DMASSERT(pParentData);
	if (!pParentData)
		return NULL;

	DUIWindow* pAddChildWnd = pParentData->m_pRootWnd->GetAddChild(hParent, pJSonParser); DMASSERT(pAddChildWnd);
	pParentData->m_pRootWnd->DV_UpdateChildLayout(); //必须重新布局
	HDMTREEITEM hChildItem = m_pEffectTreeCtrl->InsertChildItem(lpName, hParent, DMTVI_FIRST, false, false);
	BindObjTreeData(pParentData->m_pRootWnd, pAddChildWnd, hChildItem, type, pJSonParser);
	
	if (bSelectItem)
	{
		m_pEffectTreeCtrl->SelectItem(hChildItem);
	}
	else
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hChildItem);
		CRect rcFrame;
		m_pObjEditor->m_pDragFrame->InitDragFrame(pData, rcFrame);
		m_pObjEditor->m_pDuiPos->InitLayout(pData->m_pDUIWnd->m_pLayout, (type == EFFECTTEMPBTN_PARTNODE) ? (DM::PosEdit**)&m_pStickerPosEditItem : (DM::PosEdit**)&m_pMakeUpPosEditItem);
	}
	
	m_pEffectTreeCtrl->SetTreeItemEyeBtnChk(hChildItem, bEyeCheck, bDisableEyeCheck);  //必须先绑定数据之后才调用 否则新出现窗口不能正常跟着父节点隐藏
	if (pJSonParser && pJSonParser->GetRelativeResourceNodeParser())
	{
		pJSonParser->RelativeResourceNodeParser((EDResourceNodeParser*)pJSonParser->GetRelativeResourceNodeParser());
	}
	return hChildItem;
}

VOID EDMainWnd::AdjustTreeItemPosByZPositon(HDMTREEITEM hItem)
{
	do 
	{
		if (!hItem)
			break;

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (!pData)
			break;

		if (!pData->m_pJsonParser)
			break;

		int iZPosition = pData->m_pJsonParser->GetZPositionOrder();
		if (iZPosition == -1)
		{
			break;
		}

		HDMTREEITEM hNextSiblingItem = m_pEffectTreeCtrl->GetNextSiblingItem(hItem);
		while (hNextSiblingItem)
		{
			ObjTreeDataPtr pNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextSiblingItem); DMASSERT(pNextSiblingData && pNextSiblingData->m_pJsonParser);
			if (pNextSiblingData && pNextSiblingData->m_pJsonParser)
			{
				HDMTREEITEM hNextSiblingItemTemp = m_pEffectTreeCtrl->GetNextSiblingItem(hNextSiblingItem);
				//int iZ = pNextSiblingData->m_pJsonParser->GetZPositionOrder();
				if (iZPosition < pNextSiblingData->m_pJsonParser->GetZPositionOrder()) //小于Zorder 下移
				{
					//调整窗口位置
					do 
					{
						ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hItem); DMASSERT(pData);
						if (!pData)
							break;

						ObjTreeDataPtr pNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextSiblingItem); DMASSERT(pNextSiblingData);
						if (!pNextSiblingData)
							break;

						DUIWindowPtr pNextNextInsertWnd = NULL;
						HDMTREEITEM hNextNextSiblingItem = m_pEffectTreeCtrl->GetNextSiblingItem(hNextSiblingItem);
						if (hNextNextSiblingItem)
						{
							ObjTreeDataPtr pNextNextSiblingData = (ObjTreeDataPtr)m_pEffectTreeCtrl->GetItemData(hNextNextSiblingItem); DMASSERT(pNextNextSiblingData);
							if (pNextNextSiblingData)
							{
								pNextNextInsertWnd = pNextNextSiblingData->m_pDUIWnd;
								if (0 != _wcsicmp(pNextNextSiblingData->m_pJsonParser->V_GetClassName(), pData->m_pJsonParser->V_GetClassName())) //同类型的Item才发生调整 如makeup跟partnode 否则跳出
									break;
							}
						}						

						DUIWindow* pParent = pData->m_pDUIWnd ? pData->m_pDUIWnd->DM_GetWindow(GDW_PARENT) : NULL;
						if (pParent)//调整窗口顺序
						{
							pParent->DM_RemoveChildWnd(pData->m_pDUIWnd);
							pParent->DM_InsertChild(pData->m_pDUIWnd, pNextNextInsertWnd ? pNextNextInsertWnd : DUIWND_FIRST);
							pParent->DM_Invalidate();
						}
					} while (false);				
					//移动树Item的位置
					m_pEffectTreeCtrl->MoveItemToNewPos(hItem, m_pEffectTreeCtrl->GetParentItem(hItem) ? m_pEffectTreeCtrl->GetParentItem(hItem) : DMTVI_ROOT, hNextSiblingItem);
				}
//				else
//				{
// 					//调整parser的顺序  
// 					if (0 == _wcsicmp(pNextSiblingData->m_pJsonParser->V_GetClassName(), pData->m_pJsonParser->V_GetClassName())) //同类型的Item发生移动
// 					{
// 					}
//				}
				hNextSiblingItem = hNextSiblingItemTemp;
			}			
		}
	} while (false);
}

void EDMainWnd::On2DEffectItemAttrUpdate(EDPartsNodeParser* pPartsNodeParser)
{	
	DUIWndAutoMuteGuard guard(m_pTraceposListCombobox->GetListBox());
	DUIWndAutoMuteGuard guard2(m_pStickerBlendModeCombobox->GetListBox());
	DUIWndAutoMuteGuard guard3(m_pStickerTargetFPSCombobox->GetListBox());
	DUIWndAutoMuteGuard guard4(m_pStickerTargetFPSCombobox->m_pEdit);
	DUIWndAutoMuteGuard guard5(m_pStickerGroupSizeCombobox->GetListBox());
	DUIWndAutoMuteGuard guard6(m_pStickerGroupIndexCombobox->GetListBox());
	DUIWndAutoMuteGuard guard7(m_pStickerAllowDiscardFrameCombobox->GetListBox());
	DUIWndAutoMuteGuard guard8(m_pStickerPositionTypeCombobox->GetListBox());
	DUIWndAutoMuteGuard guard9(m_pStickerPositionRelationTypeCombobox->GetListBox());
	DUIWndAutoMuteGuard guard10(m_pStickerHotLinkCombobox->GetListBox());
	do
	{
		DMASSERT(pPartsNodeParser);
		if (!pPartsNodeParser)
			break;

		int iIndex1 = -1, iIndex2 = -1;
		pPartsNodeParser->GetReferencePointIndex(iIndex1, iIndex2);
		int iSelect = -1;
		for (int i = 0; i < countof(g_tracePoints); i++)
		{
			if (g_tracePoints[i].x == iIndex1 && g_tracePoints[i].y == iIndex2)
			{
				iSelect = i;
			}
		}
		m_pTraceposListCombobox->SetCurSel(iSelect);
		m_pStickerBlendModeCombobox->SetCurSel(pPartsNodeParser->m_iBlendMode);
		m_pStickerTargetFPSCombobox->m_pEdit->SetWindowText(std::to_wstring(pPartsNodeParser->m_iTargetFPS).c_str());
		m_pStickerGroupSizeCombobox->SetCurSel(pPartsNodeParser->m_stickerGroup.m_nGroupSize - 1);

		m_pStickerGroupIndexCombobox->ResetContent();
		for (int i = 0; i < pPartsNodeParser->m_stickerGroup.m_nGroupSize; i++)
		{
			m_pStickerGroupIndexCombobox->InsertItem(-1, std::to_wstring(i).c_str());
		}
		m_pStickerGroupIndexCombobox->SetCurSel(pPartsNodeParser->m_stickerGroup.m_nIndex);
		m_pStickerAllowDiscardFrameCombobox->SetCurSel(pPartsNodeParser->m_bAllowDiscardFrame ? 0 : 1);
		m_pStickerPositionRelationTypeCombobox->SetCurSel(pPartsNodeParser->m_iPositionRelationType == 1 ? 0 : 1);
		m_pStickerHotLinkCombobox->SetCurSel(pPartsNodeParser->m_bHotlinkEnable ? 0 : 1);

		int iCombTypeSele = 0;
		for (int i = 0; i < countof(g_PositionTypes); i++)
		{
			if (g_PositionTypes[i].typeId == pPartsNodeParser->m_iPositionType && g_PositionTypes[i].type2Id == pPartsNodeParser->m_iPositionType2)
			{
				iCombTypeSele = i;
				break;
			}
		}
		m_pStickerPositionTypeCombobox->SetCurSel(iCombTypeSele);
	} while (false);
	return;
}

/*
void EDMainWnd::On2DeffectItemTiggerUpdate()
{
	auto muteTriggerComboboxs = [this](bool isMute)
	{
		m_pTriggerAriseListCombobox->GetListBox()->m_EventMgr.SetMuted(isMute);
		m_pTriggerActListCombobox->GetListBox()->m_EventMgr.SetMuted(isMute);
		m_pTriggercicleListCombobox->GetListBox()->m_EventMgr.SetMuted(isMute);
	};

	muteTriggerComboboxs(true);

	auto partItem = getPartItem();
	if (partItem)
	{
// todo: remove
// 		int triggerType = 0;
// 		if (partItem->hasTriggerType())
// 			triggerType = partItem->triggerType();
// 
// 		if (triggerType == NoneTrigger)//无触发
// 		{
// 			m_pTriggerAriseListCombobox->SetCurSel(NoneTrigger);
// 			m_pTriggerActListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(false);
// 			m_pTriggercicleListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
// 		}
// 		else
// 		{
// 			auto trigTypeList = g_pCoreApp->triggerTypeList();
// 			auto typeIter = std::find(trigTypeList.begin(), trigTypeList.end(), triggerType);
// 			if (typeIter == trigTypeList.end())
// 			{
// 				m_pTriggerAriseListCombobox->SetCurSel(TriggerAppear);
// 				m_pTriggerActListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(false);
// 				m_pTriggercicleListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
// 			}
// 			else
// 			{
// 				m_pTriggerAriseListCombobox->SetCurSel(1);
// 				m_pTriggerActListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
// 				m_pTriggercicleListCombobox->DM_GetWindow(GDW_PARENT)->DM_SetVisible(true);
// 				m_pTriggerActListCombobox->SetCurSel(std::distance(trigTypeList.begin(), typeIter));
// 			}
// 		}
// 
// 		int loopValue = 0;
// 		if (partItem->hasTriggerLoop())
// 			loopValue = partItem->triggerLoop();
// 		m_pTriggercicleListCombobox->SetCurSel(loopValue);
	}

	m_pTriggerActListCombobox->DM_GetWindow(GDW_PARENT)->DM_GetWindow(GDW_PARENT)->DM_Invalidate();
	muteTriggerComboboxs(false);
}  */
