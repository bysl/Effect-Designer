//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDPartsParser.h 
// File Des: Parts字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-26	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDPartsNodeParserAttr
	{
	public:
		static char* BOOL_allowDiscardFrame;								///< 示例:"allowDiscardFrame" : false
		static char* INT_blendMode;											///< 示例:"blendMode" : 0
		static char* BOOL_enable;											///< 示例:"enable" : true
		static char* INT_frameCount;										///< 示例:"frameCount" : 2
		static char* INT_height;											///< 示例:"height" : 218
		static char* BOOL_hotlinkEnable;									///< 示例:"hotlinkEnable" : false
		static char* COMPOSITE_position;									///< 示例:"position": {
		class EDPartsNodePosition
		{
		public:
			static char* COMPOSITE_positionX;								///< 示例:	"positionX": [{
			static char* COMPOSITE_positionY;								///< 示例:	"positionY": [{
		};
		static char* INT_positionRelationType;								///< 示例:"positionRelationType" : 1
		static char* INT_positionType;										///< 示例:"positionType": 1,
		static char* INT_positionType2;										///< 示例:"positionType": 2,
		static char* COMPOSITE_rotateCenter;								///< 示例:"rotateCenter": [{"index": 46,"x" : 175.43,"y" : 591.68}],
		static char* COMPOSITE_scale;										///< 示例:"scale": {"scaleX": {	"pointA": [{ };
		class EDPartsNodeScale
		{
		public:
			static char* COMPOSITE_scaleX;									///< 示例:	"scaleX": {
			static char* COMPOSITE_scaleY;									///< 示例:	"scaleY": {
			static char* COMPOSITE_pointA;									///< 示例:	"pointA": [{
			static char* COMPOSITE_pointB;									///< 示例:	"pointB": [{
		};
		static char* COMPOSITE_stickerGroup;								///< 示例:"stickerGroup": {	"groupSize": 1,	"index" : 0	},
		class EDPartsNodeStickerGroup
		{
		public:
			static char* INT_groupSize;										///< 示例:	"groupSize": 1,
			static char* INT_index;											///< 示例:	"index": 0
		};
		static char* INT_targetFPS;											///< 示例:"targetFPS": 25
		static char* INT_width;												///< 示例:"width" : 510
		static char* INT_zPosition;											///< 示例:"zPosition" : 16
	};

	EDAttrValueInit(EDPartsNodeParserAttr, BOOL_allowDiscardFrame)EDAttrValueInit(EDPartsNodeParserAttr, INT_blendMode)EDAttrValueInit(EDPartsNodeParserAttr, BOOL_enable)EDAttrValueInit(EDPartsNodeParserAttr, INT_frameCount)
	EDAttrValueInit(EDPartsNodeParserAttr, INT_height)EDAttrValueInit(EDPartsNodeParserAttr, BOOL_hotlinkEnable)EDAttrValueInit(EDPartsNodeParserAttr, COMPOSITE_position)EDAttrValueInit(EDPartsNodeParserAttr, INT_positionRelationType)
	EDAttrValueInit(EDPartsNodeParserAttr, INT_positionType)EDAttrValueInit(EDPartsNodeParserAttr, INT_positionType2)EDAttrValueInit(EDPartsNodeParserAttr, COMPOSITE_rotateCenter)EDAttrValueInit(EDPartsNodeParserAttr, COMPOSITE_scale)
	EDAttrValueInit(EDPartsNodeParserAttr, COMPOSITE_stickerGroup)EDAttrValueInit(EDPartsNodeParserAttr, INT_targetFPS)EDAttrValueInit(EDPartsNodeParserAttr, INT_width)EDAttrValueInit(EDPartsNodeParserAttr, INT_zPosition)
	EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodePosition, COMPOSITE_positionX)EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodePosition, COMPOSITE_positionY)
	EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeScale, COMPOSITE_scaleX)EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeScale, COMPOSITE_scaleY)
	EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeScale, COMPOSITE_pointA)EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeScale, COMPOSITE_pointB)
	EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeStickerGroup, INT_groupSize)EDAttrValueInit(EDPartsNodeParserAttr::EDPartsNodeStickerGroup, INT_index)
}


/// <summary>
///		"parts"节点json字段解析类
/// </summary>
enum BlendModeEm
{
	BlendMode_Normal,
	BlendMode_Screen,
	BlendMode_Linear_Dodge,
};
class EDPartsParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDPartsParser, L"parts", DMREG_Attribute);
public:
	EDPartsParser();
	~EDPartsParser();

	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon) override;
};

/// <summary>
///		"parts"子节点json字段解析类
/// </summary>
class EDPartsNodeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDPartsNodeParser, L"partsNode", DMREG_Attribute);
public:
	EDPartsNodeParser();
	~EDPartsNodeParser();

	/// Point节点类
	class CPSPointNode
	{
	public:
		CPSPointNode() : m_index(0), m_x(0), m_y(0){};
		CPSPointNode(int index, double x, double y) :m_index(index), m_x(x), m_y(y){};
		~CPSPointNode(){};
	public:
		int		m_index;
		double	m_x;
		double	m_y;
	};

	/// Position节点类
	class CPosition
	{
	public:
		CPosition(){};
		~CPosition(){};
		DMCode SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon);
		DMCode BuildMemberJsonData(JSHandle &JSonHandler);
	public:
		DM::CArray<CPSPointNode> m_positionX;
		DM::CArray<CPSPointNode> m_positionY;
	};

	/// RotateCenter节点类
	class CRotateCenter
	{
	public:
		CRotateCenter(){};
		~CRotateCenter(){};
		DMCode SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon);
		DMCode BuildMemberJsonData(JSHandle &JSonHandler);
	public:
		DM::CArray<CPSPointNode> m_position;
	};
		
	/// scale节点类
	class CScale
	{
	public:
		CScale(){ 
		};
		~CScale(){};

		/// scaleX  scaleY 节点类
		class CScaleXY
		{
		public:
			CScaleXY(){};
			~CScaleXY(){};
			/// pointA  pointB 节点类
			class CPointAB
			{
			public:
				CPointAB(){};
				~CPointAB(){};
				DMCode SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon);
				DMCode BuildMemberJsonData(JSHandle &JSonHandler);
			public:
 				DM::CArray<CPSPointNode> m_position;
			};
		public:
			VOID AddPointAVal(CPSPointNode& point){ m_pointA.m_position.Add(point); }
			VOID AddPointBVal(CPSPointNode& point){ m_pointB.m_position.Add(point); }
			DMCode SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon);
			DMCode BuildMemberJsonData(JSHandle &JSonHandler);
		public:
			CPointAB	m_pointA;
			CPointAB	m_pointB;  
		};

		DMCode SetJSonAttribute(JSHandle& JsHandleValue, bool bLoadJSon);
		DMCode BuildMemberJsonData(JSHandle &JSonHandler);
	public:
		CScaleXY	m_scaleX;
		CScaleXY	m_scaleY;
	};

	/// stickerGroup 节点类
	class CstickerGroup
	{
	public:
		CstickerGroup()
			: m_nGroupSize(1)
			, m_nIndex(0)
		{}
		CstickerGroup(int nGroupSize, int nIndex) : m_nGroupSize(nGroupSize), m_nIndex(nIndex){}
		~CstickerGroup(){}
	public:
		int	m_nGroupSize;
		int	m_nIndex;
	};

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

	DMCode AddParserRelativeResource(LPCSTR lpKeyVal);										///< 表示member解析完成
	DMCode RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon = false) override;	///<关联Resource指针
	EDResourceNodeParser* GetRelativeResourceNodeParser() override;
	void SetParserItemRect(const CRect& rect) override;
	void GetReferencePoint(CPoint& pt1, CPoint& pt2);										///< 获取参考点
	void GetReferencePointIndex(int& index1, int& index2);									///< 获取参考点index值
	void SetReferencePoint(int iIndex1, int iIndex2);										///< 设置参考点
	void SetScalePoint(int iIndex1, int iIndex2);											///< 设置Scale
	void SetRotateCenterPoint(int iIndex);													///< 设置Rotate
	CPoint GetParserItemTopLeftPt() override;												///< 获取topleft坐标
	double GetParserImageScale() override;													///< 获取ImageScale
	void ResetParserImageScale();															///< 设置ImageScale
	int GetZPositionOrder() override;														///< 获取zposition
	bool SetZPositionOrder(int iZPostion) override;											///< 设置zposition
	bool IsParserEnable() override;															///< 获取m_bEnable

	DMCode GetRelativeResourceImgSize(INT& uiWidth, INT& uiHeight) override;
public:
	bool			m_bAllowDiscardFrame;
	int				m_iBlendMode;
	bool			m_bEnable;
	int				m_iFrameCount;
	int				m_iHeight;
	bool			m_bHotlinkEnable;
	CPosition		m_psPosition;									///< static char* COMPOSITE_position;///< 示例:"position": {		
	int				m_iPositionRelationType;
	int				m_iPositionType;
	int				m_iPositionType2;
	CRotateCenter	m_psRotateCenter;									///< static char* COMPOSITE_rotateCenter; ///< 示例:"rotateCenter": [{"index": 46,"x" : 175.43,"y" : 591.68}],
	CScale			m_scScale;											///< static char* COMPOSITE_scale;		///< 示例:"scale": {"scaleX": {	"pointA": [{ };
	CstickerGroup   m_stickerGroup;										///< COMPOSITE_stickerGroup
	int				m_iTargetFPS;
	int				m_iWidth;
	int				m_iZPosition;
	double			m_dbImageScale;

	EDResourceNodeParser* m_pRelativeResourceNodeParser;

	///自己定义的变量  非需要写入json的变量
private:
};
