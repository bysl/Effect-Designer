//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDBase.h 
// File Des: 所有注册类的基类  Json解析基类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-22	1.0		
//-------------------------------------------------------
#pragma once

/// <summary>
///		json解析类的基类，用于所有数据类使用json解析宏
/// </summary>
class EDDataBase
{
public:
	virtual DMCode InitJSonData(JSHandle &JSonHandler);															///<加载、解析JSon数据
	virtual DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& pszValue, bool bLoadJSon);					///<解析JSon的属性数据
	virtual DMCode DefAttributeProc(LPCSTR pszAttribute, JSHandle& pszValue, bool bLoadJSon);					///<默认解析JSon的属性数据
	virtual DMCode ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon);				///<解析member成员
	virtual DMCode ParseJSNodeObjFinished();																	///<完成整个node的解析
	virtual DMCode SetJSonMemberKey(LPCSTR lpKeyVal);															///<设置JSon成员的key值

	virtual DMCode BuildJSonData(JSHandle JSonHandler);															///<生成JSon成员
	virtual DMCode BuildMemberJsonData(JSHandle &JSonHandler);													///<生成当前成员变量的JSon节点
};

/// <summary>
///		所有注册类的基类
/// </summary>
class EDBase : public EDDataBase
{
public:
	static LPCWSTR GetClassName();			///<取得注册类名
	virtual LPCWSTR V_GetClassName();       ///<取得注册类名, 这是虚函数
	static LPCWSTR GetBaseClassName();		///<取得注册类父类名
	virtual bool IsClass(LPCWSTR lpszName);	///<是否属于此窗口类向上链
	static int GetClassType();              ///<取得窗口类型
	virtual int V_GetClassType();			///<取得窗口类型，和GetClassType一样，但这是个虚函数，用于基指针指向子类时动态调用
};

/// <summary>
///		属性字段字符串分割 INT_blendMode -> blendMode
/// </summary>
namespace EDAttr
{
	class EDInitAttrDispatch
	{
	public:
		static char* GetAttrValue(char* cls, char *pBuf);
	};
}

/// 所有基于EDBase的子类必须定义此宏,前两个static,后两个由子类重载了classtype只允许使用EDREGTYPE中的类型
#define EDDECLARE_CLASS_NAME(theclass, classname, classtype) \
public: \
static LPCWSTR GetClassName() \
{ \
	return classname; \
} \
virtual LPCWSTR V_GetClassName() \
{ \
	return classname; \
} \
static LPCWSTR GetBaseClassName() \
{ \
	return __super::GetClassName(); \
} \
virtual bool IsClass(LPCWSTR lpszName) \
{ \
	if (0 == _wcsicmp(GetClassName(), lpszName)) \
		return true; \
	return __super::IsClass(lpszName);\
} \
static int GetClassType() \
{ \
	return classtype; \
}\
virtual int V_GetClassType()\
{\
	return classtype;\
}

namespace EDAttr
{
#define  EDAttrValueInit(cls,x)						__declspec(selectany)   char*  cls::x = EDInitAttrDispatch::GetAttrValue(#cls,#x);
}

// -----------------------------------------------
// 结构定义
typedef enum _GPS_CODE
{
	GPS_FIRSTCHILD = 0,
	GPS_LASTCHILD,
	GPS_PREVSIBLING,
	GPS_NEXTSIBLING,
	GPS_PARENT,
}GPS_CODE;

class EDJSonParser;
class IEDJSonParserOwner;
typedef EDJSonParser* EDJSonParserPtr;

#define JSPARSER_FIRST		((EDJSonParser*)-1)
#define JSPARSER_LAST		NULL
#define INVALIDEPARSERNAME	L"Untitled"
#define TAG2DSTICKER		L"TAG2DSticker"
#define TAGMAKEUPNODE		L"TAGMakeupNode"
#define FACEMORPH			L"__facemorph"

#define MAKEUPTAG_EYESHADOW L"EYE_SHADOW"
#define MAKEUPTAG_BLUSH		L"BLUSH"
#define MAKEUPTAG_EYE		L"EYE"
#define MAKEUPTAG_LIP		L"LIP"
#define MAKEUPTAG_NOSE		L"NOSE"
#define MAKEUPTAG_NONE		L"NONE"

#define DEFRESPNGWIDTH			168
#define DEFRESPNGHEIGHT			168

/// <summary>
///		jsnode的树形节点管理类
/// </summary>
class EDJSonParserNode
{
public:
	EDJSonParserNode()
	{
		m_pOwner = NULL;
		m_pParent = m_pFirstChild = m_pLastChild = m_pNextSibling = m_pPrevSibling = NULL;
		m_nChildrenCount = 0;
	}
public:
	IEDJSonParserOwner*		m_pOwner;								///< 容器Owner
	EDJSonParser*			m_pParent;								///< 父节点
	EDJSonParser*			m_pFirstChild;							///< 第一子节点
	EDJSonParser*			m_pLastChild;							///< 最后节点
	EDJSonParser*			m_pNextSibling;							///< 前一兄弟节点
	EDJSonParser*			m_pPrevSibling;							///< 后一兄弟节点
	int						m_nChildrenCount;						///< 子节点数量
};

