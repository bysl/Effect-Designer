// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDUIPlugin.h
// File mark:   ����UI���  
// File summary:
// Edition:     1.0
// Create date: 2019-1-29
// ----------------------------------------------------------------
#pragma once

class EDUIPlugin:public DM::IDMPlugin
{
public:
	const wchar_t* GetName() const;
	void Install();
	void Uninstall();

	void Initialise();
	void Shutdown();
};
