﻿#pragma once

namespace ED
{
	class EDListBoxEx : public DUIListBoxEx
	{
		DMDECLARE_CLASS_NAME(EDListBoxEx, L"EDListBoxEx", DMREG_Window)
	public:
		EDListBoxEx();
		~EDListBoxEx();

		DMCode InsertNewItem(const CStringW& strResName, int index = -1, bool b2DStickerPic = true, bool bInUse = true, LPARAM lpData = NULL);
		DMCode UpdateItemInfo(const CStringW& strResName, int index = -1, bool b2DStickerPic = true, bool bInUse = true, LPARAM lpData = NULL);
		DMCode HandleResLibMenu(int nID);
		int GetText(int nIndex, CStringW& strText);
		void DM_OnKillFocus();

	public:
		DM_BEGIN_MSG_MAP()
			MSG_WM_LBUTTONDBLCLK(OnLButtonDbClick)
			MESSAGE_RANGE_HANDLER_EX(WM_MOUSEFIRST, WM_MOUSELAST, OnMouseEvent)
			MSG_WM_KEYDOWN(OnKeyDown)
			MSG_WM_RBUTTONDOWN(OnRButtonDown)
			DM_MSG_WM_KILLFOCUS(DM_OnKillFocus)
		DM_END_MSG_MAP()

	private:
		LRESULT OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
		void OnRButtonDown(UINT nFlags, CPoint pt);
		void OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags);
		void CreateDragCanvas(UINT iItem);												  ///< 创建画布
		void DrawDraggingState(DWORD dwDragTo);

		DMCode KillAllVisibleEditFocus();
		DMCode OnSelectChangedEvent(DMEventArgs *pEvt);
		DMCode OnReslibEditKillFocus(DMEventArgs* pEvent);
		DMCode OnReslibEditInputReturn(DMEventArgs* pEvent);
		void   OnLButtonDbClick(UINT nFlags, CPoint pt);

	private:
		// 拖动行
		bool                                   m_bSwapLine;
		bool								   m_bDragging;						///< 正在拖动标志
		DMSmartPtrT<IDMCanvas>				   m_pDragCanvas;					///< 显示拖动窗口的临时画布
		DWORD								   m_dwHitTest;						///< 拖动位置所在项
		DWORD								   m_dwDragTo;						///< 拖放目标所在项    
		CPoint								   m_ptClick;						///< 当前点击坐标
	};
}

