#include "EDBaseAfx.h"
#include "EventMgr.h"

int s_nextSlotId = 0;
EventSlot::EventSlot(const GPSlot& slot)
: m_slotImpl(slot)
, m_slotId(++s_nextSlotId)
, m_bMuted(false)
{
}

bool EventSlot::operator==(const EventSlot& src) const
{
	return m_slotImpl == src.m_slotImpl;
}

EventSlot::~EventSlot()
{
}

bool EventSlot::Connected() const
{
	return !m_slotImpl.empty();
}

void EventSlot::Disconnect()
{
	m_slotImpl.clear();
}

bool EventSlot::IsMuted(void) const
{
	return m_bMuted;
}

void EventSlot::SetMuted(bool bMuted)
{
	m_bMuted = bMuted;
}

bool EventSlot::CompareIfThis(void* pThis) const
{
	class SlotThis : public DM::DelegateMemento
	{
	public:
		SlotThis(const DM::DelegateMemento& self) : DM::DelegateMemento(self) {}
		void* This() const {return this->m_pthis;}
	};
	SlotThis implThis(const_cast<GPSlot&>(m_slotImpl).GetMemento());
	if (implThis.This() == pThis) 
	{
		return true;
	}
	return false;
}

const GPSlot EventSlot::GetSlot() const
{
	return m_slotImpl;
}

bool EventSlot::Invoke(EventSender& sender, Bundle& args)
{
	if (m_slotImpl)
	{
		return m_slotImpl(sender, args);
	}
	return false;
}

EventSlot* EventSlot::Clone() const
{
	return new EventSlot(m_slotImpl);
}


///Event---------------------------------------------------------------------------------------------
Event::Event(const std::string& eventName) : m_name(eventName)
{
}

void Event::operator()(EventSender& sender, Bundle& args)
{
	SlotContainer::iterator iter(m_slotContainer.begin());
	const SlotContainer::const_iterator iterEnd(m_slotContainer.end());
	for ( ; iter != iterEnd; ++iter) 
	{
		SlotContainer::mapped_type copySlot = iter->second;
		if (copySlot && false == copySlot->IsMuted()) 
			copySlot->Invoke(sender, args);
	}
}

Event::~Event()
{
	SlotContainer::iterator iter = m_slotContainer.begin();
	SlotContainer::iterator iterEnd = m_slotContainer.end();
	for (; iterEnd != iter; ++iter) 
	{
		SlotContainer::mapped_type copySlot = iter->second;
		if (copySlot) copySlot->Disconnect();
	}
	m_slotContainer.clear();
}

const std::string& Event::GetName(void) const
{ 
	return m_name; 
}

bool Event::EmptySlot(void) const
{
	return m_slotContainer.empty();
}

Event::Connection Event::ConnectSlot(const EventSlot& slot, Group group/* = -1*/)
{
	Event::Connection _conn; _conn.Attach(slot.Clone());
	m_slotContainer.insert(std::pair<Group, Connection>(group, _conn));
	return _conn;
}

bool Event::DisconnectIfThis(void* pThis)
{
	bool bSuccess = false;
	SlotContainer::iterator iter(m_slotContainer.begin());
	const SlotContainer::const_iterator iterEnd(m_slotContainer.end());
	for ( ; iter != iterEnd; ) 
	{
		SlotContainer::mapped_type copySlot = iter->second;
		if (copySlot && copySlot->CompareIfThis(pThis))
		{
			copySlot->Disconnect();	// 先断开事件，再移除
			m_slotContainer.erase(iter++);
			bSuccess = true;
		} 
		else 
		{
			++iter;
		}
	} // for
	return bSuccess;
}

///EventSet ---------------------------------------------------------------------------
EventSet::EventSet()
{
}

EventSet::~EventSet(void)
{
	RemoveAllEvents();
}

void EventSet::AddEvent(const std::string& eventName)
{
	if (!ExistEvent(eventName)) 
	{
		RefEvent pEvent; pEvent.Attach(new Event(eventName));
		m_mapEvents[eventName] = pEvent;
	}
}

void EventSet::RemoveEvent(const std::string& eventName)
{
	t_mapEvents::iterator iter = m_mapEvents.find(eventName);
	if (m_mapEvents.end() != iter) 
	{
		m_mapEvents.erase(iter);
	}
}

void EventSet::RemoveEventIfThis(void* pThis)
{
	t_mapEvents::iterator iter(m_mapEvents.begin());
	t_mapEvents::iterator iterEnd(m_mapEvents.end());
	for ( ; iter != iterEnd; ) 
	{
		t_mapEvents::mapped_type copyEvent = iter->second;
		if (copyEvent) 
		{
			copyEvent->DisconnectIfThis(pThis);
			if (copyEvent->EmptySlot()) // 若是空的事件槽则清空事件对象
			{
				m_mapEvents.erase(iter++);
			}
			else 
			{
				++iter;
			}
		} 
		else 
		{
			m_mapEvents.erase(iter++);
		}
	}
}

void EventSet::RemoveAllEvents(void)
{
	t_mapEvents clearAllEventList;
	clearAllEventList.swap(m_mapEvents);

	t_mapEvents::iterator iter = clearAllEventList.begin();
	t_mapEvents::iterator iterEnd = clearAllEventList.end();
	for (; iter != iterEnd; )
	{
		clearAllEventList.erase(iter++);
	}
}

bool EventSet::ExistEvent(const std::string& eventName)
{
	return (m_mapEvents.find(eventName) != m_mapEvents.end());
}

void EventSet::FireEvent(const std::string& eventName, Bundle& args, LPCSTR pszReserve /*= NULL*/)
{
	EventSender sender;
	FireEventImpl(eventName, sender, args);
}

void EventSet::FireEvent(const std::string& eventName, EventSender& sender, Bundle& args, LPCSTR pszReserve /*= NULL*/)
{
	FireEventImpl(eventName, sender, args);
}

Event::Connection EventSet::ConnectEvent(const std::string& eventName, const EventSlot& slot, Event::Group group /*= -1*/)
{
	return GetEventObject(eventName, true)->ConnectSlot(slot, group);
}

RefEvent EventSet::GetEventObject(const std::string& eventName, bool autoAdd)
{
	t_mapEvents::iterator iter = m_mapEvents.find(eventName);
	if (m_mapEvents.end() == iter)
	{
		if (autoAdd)
		{
			AddEvent(eventName);
			return m_mapEvents.find(eventName)->second;
		}
		else 
		{
			return RefEvent();
		}
	}
	return iter->second;
}

void EventSet::FireEventImpl(const std::string& eventName, EventSender& sender, Bundle& args)
{
	RefEvent refEvt = GetEventObject(eventName);
	if (refEvt.isValid()) 
	{
		refEvt->operator()(sender, args);
	}
}

