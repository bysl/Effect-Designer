// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DES3Coder.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-1
// ----------------------------------------------------------------
#pragma once

#define EN0	0	/* MODE == encrypt */
#define DE1	1	/* MODE == decrypt */

void deskey(unsigned char *, short);
void des(unsigned char *, unsigned char *);
void Ddes(unsigned char *, unsigned char *);
void D2des(unsigned char *, unsigned char *);
void usekey(unsigned long *);
void cpkey(unsigned long *);
void des2key(unsigned char *, short);
void makekey(char *, unsigned char *);

#define makeDkey(a,b)	make2key((a),(b))
void make2key(char *, unsigned char *);
#define useDkey(a)		use3key((a))
#define cpDkey(a)		cp3key((a))

void des3key(unsigned char *, short);
void use3key(unsigned long *);
void cp3key(unsigned long *);
void make3key(char *, unsigned char *);