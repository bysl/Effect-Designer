// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EventEx.h 
// File mark:   扩展
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-6
// ----------------------------------------------------------------
#pragma once
#include "EventMgr.h"
#include "TaskSvc.h"

/// <summary>
///		将事件槽转换成task
/// </summary>
class SlotDispatchTask : public ITask
{
public:
	SlotDispatchTask(const GPSlot& slot,const EventSender& sender,const Bundle& bundle,IEventSlot* pSlot);
	~SlotDispatchTask();

	void Run();
private:
	DMSmartPtrT<IEventSlot>					m_pEventSlot;
	GPSlot									m_slot;
	EventSender								m_sender;
	Bundle									m_bundle;
};

class EventSetEx;
/// <summary>
///		UI事件槽
/// </summary>
class EventHandlerUISlot : public EventSlot
{
public:
	EventHandlerUISlot(const GPSlot& slot,EventSetEx *pOwner);
public:
	virtual bool Invoke(EventSender& sender, Bundle& args);
	virtual EventSlot* Clone() const;
public:
	EventSetEx*                     m_pOwner;
};

/// <summary>
///		消息窗口
/// </summary>
class EventHandlerUIWnd : public DMCWnd
{
public:
	EventHandlerUIWnd(DWORD dwThreadId);

	void DispatchUiEvent(SlotDispatchTask* _pSlotDispatchTask);

public:
	static EventHandlerUIWnd* New(DWORD dwThreadId);
	static void Del(EventHandlerUIWnd*& refObj);

public:
	BEGIN_MSG_MAPT(EventHandlerUIWnd)
		MESSAGE_HANDLER_EX(m_MsgId, OnHandlerEvent)
	END_MSG_MAP()
public:
	LRESULT OnHandlerEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);

public:
	const UINT							m_MsgId;
	DWORD								m_dwThreadId;
};

/// <summary>
///		工作事件槽
/// </summary>
class EventHandlerWorkerSlot : public EventSlot
{
public:
	EventHandlerWorkerSlot(const GPSlot& slot,EventSetEx *pOwner);
public:
	virtual bool Invoke(EventSender& sender, Bundle& args);
	virtual EventSlot* Clone() const;
public:
	EventSetEx*                     m_pOwner;
};


/// <summary>
///		扩展EventSet支持UI线程和工作线程
/// </summary>
class  EventSetEx : public EventSet
{
public:
	EventSetEx();
	~EventSetEx();

public:
	bool InitEvent(DWORD dwThreadId);
	Event::Connection ConnectUiEvent(const std::string& eventName, const GPSlot& slot, Event::Group group = -1);
    Event::Connection ConnectAsyncEvent(const std::string& eventName, const GPSlot& slot, Event::Group group = -1);
	bool DispatchUiEvent(SlotDispatchTask* _pSlotDispatchTask);
	bool DispatchAsyncEvent(const GPSlot& slot, const EventSender& sender, const Bundle& evt);

protected:
	 EventHandlerUIWnd* GetUiThreadHandlerImpl(UINT uiThreadHandler);
	 static bool DispatchEvent(const GPSlot& slot, const EventSender& sender, const Bundle& evt);
protected:
	EventHandlerUIWnd*					m_uiThreadHandler;
	bool								m_bRunUiHandler;
	DWORD								m_dwUiThreadId;

	// 任务队列
	bool								m_bRunTaskThread;
	TaskSvc								m_TaskSvc;
};