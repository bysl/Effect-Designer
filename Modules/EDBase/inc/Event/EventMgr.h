// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EventMgr.h 
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-6
// ----------------------------------------------------------------
#pragma once
#include "Bundle.h"
#include <functional>

interface IEventSlot : public DMRefNum
{
	virtual bool Connected() const = 0;
};

/// <summary>
///		事件槽
/// </summary>
class EventSlot : public IEventSlot
{
public:
	EventSlot(const GPSlot& slot);
	bool operator==(const EventSlot& src) const;
	virtual ~EventSlot();

public:
	bool Connected() const;
	void Disconnect();
	bool IsMuted(void) const;
	void SetMuted(bool bMuted);
	bool CompareIfThis(void* pThis) const;	
	const GPSlot GetSlot() const;

public:
	virtual bool Invoke(EventSender& sender, Bundle& args);
	virtual EventSlot* Clone() const;

private:
	bool									m_bMuted;		///< 为true时不调用事件
	int										m_slotId;		///< 事件槽Id号
	GPSlot									m_slotImpl;		///< 事件槽实现体
};


/// <summary>
///		事件槽容器
/// </summary>
class Event : public DMRefNum
{
public:
	typedef DMSmartPtrT<EventSlot>			Connection;
	typedef	int								Group;
public:
	Event(const std::string& eventName);
	void operator()(EventSender& sender, Bundle& args);
	~Event();

public:
	const std::string& GetName(void) const;
	bool EmptySlot(void) const;
	Connection ConnectSlot(const EventSlot& slot, Group group = -1);
	bool DisconnectIfThis(void* pThis);

public:
	const std::string						m_name;			///< 事件名
	typedef std::multimap<Group, Connection, std::greater<Group>> SlotContainer;
	SlotContainer							m_slotContainer;///< 事件槽容器，组别越大越优先调用 
};
typedef DMSmartPtrT<Event>					RefEvent;

/// <summary>
///		事件集
/// </summary>
class EventSet
{
public:
	EventSet();
	virtual ~EventSet(void);

public:
	void AddEvent(const std::string& eventName);
	void RemoveEvent(const std::string& eventName);
	void RemoveEventIfThis(void* pThis);
	void RemoveAllEvents(void);
	bool ExistEvent(const std::string& eventName);

	void FireEvent(const std::string& eventName, Bundle& args, LPCSTR pszReserve = NULL);
	void FireEvent(const std::string& eventName, EventSender& sender, Bundle& args, LPCSTR pszReserve = NULL);

public:
	 virtual Event::Connection ConnectEvent(const std::string& eventName, const EventSlot& slot, Event::Group group = -1);
	
protected:
	RefEvent GetEventObject(const std::string& eventName, bool autoAdd = false);
	void FireEventImpl(const std::string& eventName, EventSender& sender, Bundle& args);

public:
	typedef std::map<std::string, RefEvent>	t_mapEvents;
	t_mapEvents								m_mapEvents;
};
